﻿using System;
using CoreGraphics;
using UIKit;
using WhirlyGlobeMaply;

namespace WGTest
{
    public partial class ViewController : UIViewController
    {
        WhirlyGlobeViewController theViewC;

        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            // Create an empty globe and add it to the view
            theViewC = new WhirlyGlobeViewController();

            View.AddSubview(theViewC.View);
            theViewC.View.Frame = View.Bounds;
            AddChildViewController(theViewC);

            //// this logic makes it work for either globe or map
            //WhirlyGlobeViewController globeViewC = null;
            //MaplyViewController mapViewC = null;
            //if ([theViewC isKindOfClass:[WhirlyGlobeViewController class]])
            //    globeViewC = (WhirlyGlobeViewController*) theViewC;
            //else
            //    mapViewC = (MaplyViewController*) theViewC;

            // we want a black background for a globe, a white background for a map.
            theViewC.ClearColor = UIColor.White;

            // and thirty fps if we can get it ­ change this to 3 if you find your app is struggling
            theViewC.FrameInterval = 2;

            // set up the data source
            var tileSource = new MaplyMBTileSource("geography-class_medres");

            //// set up the layer
            var layer = new MaplyQuadImageTilesLayer(tileSource.CoordSys, tileSource);
            layer.HandleEdges = false;
            layer.CoverPoles = false;
            layer.RequireElev = false;
            layer.WaitLoad = false;
            layer.DrawPriority = 0;
            layer.SingleLevelLoading = false;

            theViewC.AddLayer(layer);

            //// start up over San Francisco, center of the universe
            //if (globeViewC != nil) {
            theViewC.Height = 1f;
            var coords = new MaplyCoordinate();
            coords.x = -122.4192f;
            coords.y = 37.7793f;

            theViewC.AnimateToPosition(coords, 1.0);
            //} else {
            //    mapViewC.height = 1.0;
            //    [mapViewC animateToPosition:MaplyCoordinateMakeWithDegrees(-122.4192, 37.7793)
            //                           time:1.0];
            //}
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

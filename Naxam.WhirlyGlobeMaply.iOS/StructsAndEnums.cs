﻿using System;
using System.Runtime.InteropServices;
using CoreAnimation;
using CoreFoundation;
using CoreGraphics;
using CoreLocation;
using CoreText;
using CoreVideo;
using Foundation;
using IOSurface;
using ImageIO;
using Metal;
using ObjCRuntime;
using OpenGLES;
using Security;
using UIKit;

namespace WhirlyGlobeMaply
{

	[StructLayout (LayoutKind.Sequential)]
	public struct MaplyCoordinate
	{
		public float x;

		public float y;
	}

	[StructLayout (LayoutKind.Sequential)]
	public struct MaplyCoordinateD
	{
		public double x;

		public double y;
	}

	[StructLayout (LayoutKind.Sequential)]
	public struct MaplyCoordinate3d
	{
		public float x;

		public float y;

		public float z;
	}

	[StructLayout (LayoutKind.Sequential)]
	public struct MaplyCoordinate3dD
	{
		public double x;

		public double y;

		public double z;
	}

	[StructLayout (LayoutKind.Sequential)]
	public struct MaplyBoundingBox
	{
		public MaplyCoordinate ll;

		public MaplyCoordinate ur;
	}

	[StructLayout (LayoutKind.Sequential)]
	public struct MaplyBoundingBoxD
	{
		public MaplyCoordinateD ll;

		public MaplyCoordinateD ur;
	}

	[Native]
	public enum MaplyLabelJustify : long
	{
		Left,
		Middle,
		Right
	}

	[StructLayout (LayoutKind.Sequential)]
	public struct MaplyTileID
	{
		public int x;

		public int y;

		public int level;
	}

	[Native]
	public enum MaplyQuadImageFormat : long
	{
		IntRGBA,
		UShort565,
		UShort4444,
		UShort5551,
		UByteRed,
		UByteGreen,
		UByteBlue,
		UByteAlpha,
		UByteRGB,
		Etc2rgb8,
		Etc2rgba8,
		Etc2rgbpa8,
		Eacr11,
		Eacr11s,
		Eacrg11,
		Eacrg11s,
		MaplyImage4Layer8Bit
	}

	[Native]
	public enum MaplyVectorObjectType : long
	{
		NoneType,
		PointType,
		LinearType,
		Linear3dType,
		ArealType,
		MultiType
	}

	[Native]
	public enum MaplyShaderAttrType : long
	{
		Int,
		Float,
		Float2,
		Float3,
		Float4
	}

	[Native]
	public enum MaplyParticleSystemType : long
	{
		Point,
		Rectangle
	}


	[StructLayout (LayoutKind.Sequential)]
	public struct MaplyLocationTrackerSimulationPoint
	{
		public float lonDeg;

		public float latDeg;

		public float headingDeg;
	}

	public enum MaplyLocationLockType : uint
	{
		None,
		NorthUp,
		HeadingUp,
		HeadingUpOffset
	}

	[Native]
	public enum MaplyThreadMode : long
	{
		Current,
		Any
	}

	[Native]
	public enum MaplyMapType : long
	{
		MaplyMapType3D,
		Flat
	}

	[Native]
	public enum MaplyQuadPagingDataStyle : long
	{
		Add,
		Replace
	}

	[StructLayout (LayoutKind.Sequential)]
	public struct MaplyQuadTrackerPointReturn
	{
		public double screenU;

		public double screenV;

		public MaplyTileID tileID;

		public int padding;

		public double locX;

		public double locY;

		public double tileU;

		public double tileV;
	}

}

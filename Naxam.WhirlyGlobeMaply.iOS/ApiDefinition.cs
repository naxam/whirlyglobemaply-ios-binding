using System;
using AVFoundation;
using CloudKit;
using Contacts;
using CoreAnimation;
using CoreData;
using CoreFoundation;
using CoreGraphics;
using CoreImage;
using CoreLocation;
using CoreVideo;
using FileProvider;
using Foundation;
using IOSurface;
using ImageIO;
using Intents;
using Metal;
using ObjCRuntime;
using OpenGLES;
using Security;
using UIKit;

namespace WhirlyGlobeMaply
{
    // @interface MaplyVertexAttribute : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyVertexAttribute
    {
        // -(instancetype _Nonnull)initWithName:(NSString * _Nonnull)name float:(float)val;
        [Export("initWithName:float:")]
        IntPtr Constructor(string name, float val);

        // -(instancetype _Nonnull)initWithName:(NSString * _Nonnull)name floatX:(float)x y:(float)y;
        [Export("initWithName:floatX:y:")]
        IntPtr Constructor(string name, float x, float y);

        // -(instancetype _Nonnull)initWithName:(NSString * _Nonnull)name floatX:(float)x y:(float)y z:(float)z;
        [Export("initWithName:floatX:y:z:")]
        IntPtr Constructor(string name, float x, float y, float z);

        // -(instancetype _Nonnull)initWithName:(NSString * _Nonnull)name color:(UIColor * _Nonnull)color;
        [Export("initWithName:color:")]
        IntPtr Constructor(string name, UIColor color);

        // -(instancetype _Nonnull)initWithName:(NSString * _Nonnull)name int:(int)val;
        [Export("initWithName:int:")]
        IntPtr Constructor(string name, int val);
    }

    // @interface MaplyTexture : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyTexture
    {
    }

    // @interface MaplyLabel : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyLabel
    {
        // @property (assign, nonatomic) MaplyCoordinate loc;
        [Export("loc", ArgumentSemantic.Assign)]
        MaplyCoordinate Loc { get; set; }

        // @property (assign, nonatomic) CGSize size;
        [Export("size", ArgumentSemantic.Assign)]
        CGSize Size { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable text;
        [NullAllowed, Export("text", ArgumentSemantic.Strong)]
        string Text { get; set; }

        // @property (nonatomic, strong) id _Nullable iconImage2;
        [NullAllowed, Export("iconImage2", ArgumentSemantic.Strong)]
        NSObject IconImage2 { get; set; }

        // @property (nonatomic, strong) UIColor * _Nullable color;
        [NullAllowed, Export("color", ArgumentSemantic.Strong)]
        UIColor Color { get; set; }

        // @property (assign, nonatomic) _Bool selectable;
        [Export("selectable")]
        bool Selectable { get; set; }

        // @property (assign, nonatomic) MaplyLabelJustify justify;
        [Export("justify", ArgumentSemantic.Assign)]
        MaplyLabelJustify Justify { get; set; }

        // @property (nonatomic, strong) id _Nullable userObject;
        [NullAllowed, Export("userObject", ArgumentSemantic.Strong)]
        NSObject UserObject { get; set; }
    }

    // @interface MaplyScreenLabel : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyScreenLabel
    {
        // @property (assign, nonatomic) MaplyCoordinate loc;
        [Export("loc", ArgumentSemantic.Assign)]
        MaplyCoordinate Loc { get; set; }

        // @property (assign, nonatomic) float rotation;
        [Export("rotation")]
        float Rotation { get; set; }

        // @property (assign, nonatomic) _Bool keepUpright;
        [Export("keepUpright")]
        bool KeepUpright { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable text;
        [NullAllowed, Export("text", ArgumentSemantic.Strong)]
        string Text { get; set; }

        // @property (nonatomic, strong) UIImage * _Nullable iconImage2;
        [NullAllowed, Export("iconImage2", ArgumentSemantic.Strong)]
        UIImage IconImage2 { get; set; }

        // @property (assign, nonatomic) CGSize iconSize;
        [Export("iconSize", ArgumentSemantic.Assign)]
        CGSize IconSize { get; set; }

        // @property (assign, nonatomic) CGPoint offset;
        [Export("offset", ArgumentSemantic.Assign)]
        CGPoint Offset { get; set; }

        // @property (nonatomic, strong) UIColor * _Nullable color;
        [NullAllowed, Export("color", ArgumentSemantic.Strong)]
        UIColor Color { get; set; }

        // @property (assign, nonatomic) _Bool selectable;
        [Export("selectable")]
        bool Selectable { get; set; }

        // @property (assign, nonatomic) float layoutImportance;
        [Export("layoutImportance")]
        float LayoutImportance { get; set; }

        // @property (assign, nonatomic) int layoutPlacement;
        [Export("layoutPlacement")]
        int LayoutPlacement { get; set; }

        // @property (nonatomic, strong) id _Nullable userObject;
        [NullAllowed, Export("userObject", ArgumentSemantic.Strong)]
        NSObject UserObject { get; set; }
    }

    // @interface MaplyMovingScreenLabel : MaplyScreenLabel
    [BaseType(typeof(MaplyScreenLabel))]
    interface MaplyMovingScreenLabel
    {
        // @property (assign, nonatomic) MaplyCoordinate endLoc;
        [Export("endLoc", ArgumentSemantic.Assign)]
        MaplyCoordinate EndLoc { get; set; }

        // @property (assign, nonatomic) NSTimeInterval duration;
        [Export("duration")]
        double Duration { get; set; }
    }

    // @interface MaplyMarker : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyMarker
    {
        // @property (assign, nonatomic) MaplyCoordinate loc;
        [Export("loc", ArgumentSemantic.Assign)]
        MaplyCoordinate Loc { get; set; }

        // @property (assign, nonatomic) CGSize size;
        [Export("size", ArgumentSemantic.Assign)]
        CGSize Size { get; set; }

        // @property (nonatomic, strong) id _Nullable image;
        [NullAllowed, Export("image", ArgumentSemantic.Strong)]
        UIImage Image { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable images;
        [NullAllowed, Export("images", ArgumentSemantic.Strong)]
        UIImage[] Images { get; set; }

        // @property (nonatomic) double period;
        [Export("period")]
        double Period { get; set; }

        // @property (assign, nonatomic) _Bool selectable;
        [Export("selectable")]
        bool Selectable { get; set; }

        // @property (nonatomic, strong) id _Nullable userObject;
        [NullAllowed, Export("userObject", ArgumentSemantic.Strong)]
        NSObject UserObject { get; set; }
    }

    // @interface MaplyScreenMarker : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyScreenMarker
    {
        // @property (assign, nonatomic) MaplyCoordinate loc;
        [Export("loc", ArgumentSemantic.Assign)]
        MaplyCoordinate Loc { get; set; }

        // @property (assign, nonatomic) CGSize size;
        [Export("size", ArgumentSemantic.Assign)]
        CGSize Size { get; set; }

        // @property (assign, nonatomic) double rotation;
        [Export("rotation")]
        double Rotation { get; set; }

        // @property (nonatomic, strong) id _Nullable image;
        [NullAllowed, Export("image", ArgumentSemantic.Strong)]
        UIImage Image { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable images;
        [NullAllowed, Export("images", ArgumentSemantic.Strong)]
        UIImage[] Images { get; set; }

        // @property (nonatomic) double period;
        [Export("period")]
        double Period { get; set; }

        // @property (nonatomic, strong) UIColor * _Nullable color;
        [NullAllowed, Export("color", ArgumentSemantic.Strong)]
        UIColor Color { get; set; }

        // @property (assign, nonatomic) float layoutImportance;
        [Export("layoutImportance")]
        float LayoutImportance { get; set; }

        // @property (assign, nonatomic) CGSize layoutSize;
        [Export("layoutSize", ArgumentSemantic.Assign)]
        CGSize LayoutSize { get; set; }

        // @property (assign, nonatomic) CGPoint offset;
        [Export("offset", ArgumentSemantic.Assign)]
        CGPoint Offset { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable vertexAttributes;
        [NullAllowed, Export("vertexAttributes", ArgumentSemantic.Strong)]
        NSObject[] VertexAttributes { get; set; }

        // @property (assign, nonatomic) _Bool selectable;
        [Export("selectable")]
        bool Selectable { get; set; }

        // @property (nonatomic, strong) id _Nullable userObject;
        [NullAllowed, Export("userObject", ArgumentSemantic.Strong)]
        NSObject UserObject { get; set; }
    }

    // @interface MaplyMovingScreenMarker : MaplyScreenMarker
    [BaseType(typeof(MaplyScreenMarker))]
    interface MaplyMovingScreenMarker
    {
        // @property (assign, nonatomic) MaplyCoordinate endLoc;
        [Export("endLoc", ArgumentSemantic.Assign)]
        MaplyCoordinate EndLoc { get; set; }

        // @property (assign, nonatomic) NSTimeInterval duration;
        [Export("duration")]
        double Duration { get; set; }
    }

    // @interface MaplyShape : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyShape
    {
        // @property (nonatomic, strong) UIColor * _Nullable color;
        [NullAllowed, Export("color", ArgumentSemantic.Strong)]
        UIColor Color { get; set; }

        // @property (assign, nonatomic) _Bool selectable;
        [Export("selectable")]
        bool Selectable { get; set; }

        // @property (nonatomic, strong) id _Nullable userObject;
        [NullAllowed, Export("userObject", ArgumentSemantic.Strong)]
        NSObject UserObject { get; set; }
    }

    // @interface MaplyShapeCircle : MaplyShape
    [BaseType(typeof(MaplyShape))]
    interface MaplyShapeCircle
    {
        // @property (assign, nonatomic) MaplyCoordinate center;
        [Export("center", ArgumentSemantic.Assign)]
        MaplyCoordinate Center { get; set; }

        // @property (assign, nonatomic) float radius;
        [Export("radius")]
        float Radius { get; set; }

        // @property (assign, nonatomic) float height;
        [Export("height")]
        float Height { get; set; }
    }

    // @interface MaplyShapeSphere : MaplyShape
    [BaseType(typeof(MaplyShape))]
    interface MaplyShapeSphere
    {
        // @property (assign, nonatomic) MaplyCoordinate center;
        [Export("center", ArgumentSemantic.Assign)]
        MaplyCoordinate Center { get; set; }

        // @property (assign, nonatomic) float radius;
        [Export("radius")]
        float Radius { get; set; }

        // @property (assign, nonatomic) float height;
        [Export("height")]
        float Height { get; set; }
    }

    // @interface MaplyShapeCylinder : MaplyShape
    [BaseType(typeof(MaplyShape))]
    interface MaplyShapeCylinder
    {
        // @property (assign, nonatomic) MaplyCoordinate baseCenter;
        [Export("baseCenter", ArgumentSemantic.Assign)]
        MaplyCoordinate BaseCenter { get; set; }

        // @property (assign, nonatomic) float baseHeight;
        [Export("baseHeight")]
        float BaseHeight { get; set; }

        // @property (assign, nonatomic) float radius;
        [Export("radius")]
        float Radius { get; set; }

        // @property (assign, nonatomic) float height;
        [Export("height")]
        float Height { get; set; }
    }

    // @interface MaplyShapeGreatCircle : MaplyShape
    [BaseType(typeof(MaplyShape))]
    interface MaplyShapeGreatCircle
    {
        // @property (assign, nonatomic) MaplyCoordinate startPt;
        [Export("startPt", ArgumentSemantic.Assign)]
        MaplyCoordinate StartPt { get; set; }

        // @property (assign, nonatomic) MaplyCoordinate endPt;
        [Export("endPt", ArgumentSemantic.Assign)]
        MaplyCoordinate EndPt { get; set; }

        // @property (assign, nonatomic) float height;
        [Export("height")]
        float Height { get; set; }

        // @property (assign, nonatomic) float lineWidth;
        [Export("lineWidth")]
        float LineWidth { get; set; }

        // -(float)calcAngleBetween;
        [Export("calcAngleBetween")]
        float CalcAngleBetween { get; }
    }

    // @interface MaplyShapeLinear : MaplyShape
    [BaseType(typeof(MaplyShape))]
    interface MaplyShapeLinear
    {
        // @property (assign, nonatomic) float lineWidth;
        [Export("lineWidth")]
        float LineWidth { get; set; }

        // -(instancetype _Nullable)initWithCoords:(MaplyCoordinate3d * _Nonnull)coords numCoords:(int)numCoords;
        [Export("initWithCoords:numCoords:")]
        unsafe IntPtr Constructor(NSNumber[] coords, int numCoords);

        // -(int)getCoords:(MaplyCoordinate3d * _Nullable * _Nonnull)retCoords;
        [Export("getCoords:")]
        unsafe int GetCoords([NullAllowed] NSNumber[] retCoords);
    }

    // @interface MaplyShapeExtruded : MaplyShape
    [BaseType(typeof(MaplyShape))]
    interface MaplyShapeExtruded
    {
        // -(instancetype _Nonnull)initWithOutline:(NSArray * _Nonnull)coords;
        [Export("initWithOutline:")]
        IntPtr Constructor(NSObject[] coords);

        // -(instancetype _Nonnull)initWithOutline:(double * _Nonnull)coords numCoordPairs:(int)numCoordPairs;
        [Export("initWithOutline:numCoordPairs:")]
        unsafe IntPtr Constructor(ref double coords, int numCoordPairs);

        // @property (readonly, nonatomic) int numCoordPairs;
        [Export("numCoordPairs")]
        int NumCoordPairs { get; }

        // @property (readonly, nonatomic) double * _Nullable coordData;
        [NullAllowed, Export("coordData")]
        unsafe NSNumber[] CoordData { get; }

        // @property (nonatomic) MaplyCoordinate center;
        [Export("center", ArgumentSemantic.Assign)]
        MaplyCoordinate Center { get; set; }

        // @property (assign, nonatomic) double scale;
        [Export("scale")]
        double Scale { get; set; }

        // @property (assign, nonatomic) double thickness;
        [Export("thickness")]
        double Thickness { get; set; }

        // @property (assign, nonatomic) double height;
        [Export("height")]
        double Height { get; set; }

        // @property (nonatomic, strong) MaplyMatrix * _Nullable transform;
        [NullAllowed, Export("transform", ArgumentSemantic.Strong)]
        MaplyMatrix Transform { get; set; }
    }

    // @interface MaplyViewControllerLayer : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyViewControllerLayer
    {
        // @property (assign, nonatomic) int drawPriority;
        [Export("drawPriority")]
        int DrawPriority { get; set; }
    }

    // @interface MaplyImageTile : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyImageTile
    {
        // -(instancetype)initWithRawImage:(NSData *)data width:(int)width height:(int)height;
        [Export("initWithRawImage:width:height:")]
        IntPtr Constructor(NSData data, int width, int height);

        // -(instancetype)initWithRawImageArray:(NSArray *)data width:(int)width height:(int)height;
        [Export("initWithRawImageArray:width:height:")]
        IntPtr Constructor(NSData[] data, int width, int height);

        // -(instancetype)initWithImage:(UIImage *)image;
        [Export("initWithImage:")]
        IntPtr Constructor(UIImage image);

        // -(instancetype)initWithImageArray:(NSArray *)images;
        [Export("initWithImageArray:")]
        IntPtr Constructor(UIImage[] images);

        // -(instancetype)initWithPNGorJPEGData:(NSData *)data;
        [Export("initWithPNGorJPEGData:")]
        IntPtr Constructor(NSData data);

        // -(instancetype)initWithPNGorJPEGDataArray:(NSArray *)data;
        [Export("initWithPNGorJPEGDataArray:")]
        IntPtr Constructor(NSData[] data);

        // -(instancetype)initWithRandomData:(id)theObj;
        [Export("initWithRandomData:")]
        IntPtr Constructor(NSObject theObj);

        // @property (nonatomic, strong) MaplyElevationChunk * elevChunk;
        [Export("elevChunk", ArgumentSemantic.Strong)]
        MaplyElevationChunk ElevChunk { get; set; }

        // @property (nonatomic) CGSize targetSize;
        [Export("targetSize", ArgumentSemantic.Assign)]
        CGSize TargetSize { get; set; }
    }

    // @interface MaplyFrameStatus : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyFrameStatus
    {
        // @property (nonatomic) int numTilesLoaded;
        [Export("numTilesLoaded")]
        int NumTilesLoaded { get; set; }

        // @property (nonatomic) _Bool fullyLoaded;
        [Export("fullyLoaded")]
        bool FullyLoaded { get; set; }

        // @property (nonatomic) int currentFrame;
        [Export("currentFrame")]
        int CurrentFrame { get; set; }
    }

    // @protocol MaplyTileSource
    [Protocol, Model]
    interface MaplyTileSource
    {
        // @required -(int)minZoom;
        [Abstract]
        [Export("minZoom")]
        int MinZoom { get; }

        // @required -(int)maxZoom;
        [Abstract]
        [Export("maxZoom")]
        int MaxZoom { get; }

        // @required -(int)tileSize;
        [Abstract]
        [Export("tileSize")]
        int TileSize { get; }

        // @required -(_Bool)tileIsLocal:(MaplyTileID)tileID frame:(int)frame;
        [Abstract]
        [Export("tileIsLocal:frame:")]
        bool TileIsLocal(MaplyTileID tileID, int frame);

        // @required -(MaplyCoordinateSystem * _Nonnull)coordSys;
        [Abstract]
        [Export("coordSys")]
        MaplyCoordinateSystem CoordSys { get; }

        // @optional -(_Bool)validTile:(MaplyTileID)tileID bbox:(MaplyBoundingBox)bbox;
        [Export("validTile:bbox:")]
        bool ValidTile(MaplyTileID tileID, MaplyBoundingBox bbox);

        // @optional -(int)tileSizeForTile:(MaplyTileID)tileID;
        [Export("tileSizeForTile:")]
        int TileSizeForTile(MaplyTileID tileID);

        // @optional -(id _Nullable)imageForTile:(MaplyTileID)tileID;
        [Export("imageForTile:")]
        [return: NullAllowed]
        NSObject ImageForTile(MaplyTileID tileID);

        // @optional -(id _Nullable)imageForTile:(MaplyTileID)tileID frame:(int)frame;
        [Export("imageForTile:frame:")]
        [return: NullAllowed]
        NSObject ImageForTile(MaplyTileID tileID, int frame);

        // @optional -(void)startFetchLayer:(id _Nonnull)layer tile:(MaplyTileID)tileID;
        [Export("startFetchLayer:tile:")]
        void StartFetchLayer(NSObject layer, MaplyTileID tileID);

        // @optional -(void)startFetchLayer:(id _Nonnull)layer tile:(MaplyTileID)tileID frame:(int)frame;
        [Export("startFetchLayer:tile:frame:")]
        void StartFetchLayer(NSObject layer, MaplyTileID tileID, int frame);

        // @optional -(void)tileWasDisabled:(MaplyTileID)tileID;
        [Export("tileWasDisabled:")]
        void TileWasDisabled(MaplyTileID tileID);

        // @optional -(void)tileWasEnabled:(MaplyTileID)tileID;
        [Export("tileWasEnabled:")]
        void TileWasEnabled(MaplyTileID tileID);

        // @optional -(void)tileUnloaded:(MaplyTileID)tileID;
        [Export("tileUnloaded:")]
        void TileUnloaded(MaplyTileID tileID);
    }

    // @interface MaplyQuadImageTilesLayer : MaplyViewControllerLayer
    [BaseType(typeof(MaplyViewControllerLayer))]
    interface MaplyQuadImageTilesLayer
    {
        // -(instancetype _Nullable)initWithTileSource:(NSObject<MaplyTileSource> * _Nonnull)tileSource;
        [Export("initWithTileSource:")]
        IntPtr Constructor(IMaplyTileSource tileSource);

        // -(instancetype _Nullable)initWithCoordSystem:(MaplyCoordinateSystem * _Nonnull)coordSys tileSource:(NSObject<MaplyTileSource> * _Nonnull)tileSource;
        [Export("initWithCoordSystem:tileSource:")]
        IntPtr Constructor(MaplyCoordinateSystem coordSys, IMaplyTileSource tileSource);

        // @property (nonatomic, strong) NSObject<MaplyTileSource> * _Nonnull tileSource;
        [Export("tileSource", ArgumentSemantic.Strong)]
        IMaplyTileSource TileSource { get; set; }

        // @property (assign, nonatomic) _Bool enable;
        [Export("enable")]
        bool Enable { get; set; }

        // @property (assign, nonatomic) float fade;
        [Export("fade")]
        float Fade { get; set; }

        // @property (assign, nonatomic) int numSimultaneousFetches;
        [Export("numSimultaneousFetches")]
        int NumSimultaneousFetches { get; set; }

        // @property (assign, nonatomic) _Bool handleEdges;
        [Export("handleEdges")]
        bool HandleEdges { get; set; }

        // @property (assign, nonatomic) _Bool coverPoles;
        [Export("coverPoles")]
        bool CoverPoles { get; set; }

        // @property (nonatomic) UIColor * _Nullable northPoleColor;
        [NullAllowed, Export("northPoleColor", ArgumentSemantic.Assign)]
        UIColor NorthPoleColor { get; set; }

        // @property (nonatomic) UIColor * _Nullable southPoleColor;
        [NullAllowed, Export("southPoleColor", ArgumentSemantic.Assign)]
        UIColor SouthPoleColor { get; set; }

        // @property (assign, nonatomic) float minVis;
        [Export("minVis")]
        float MinVis { get; set; }

        // @property (assign, nonatomic) float maxVis;
        [Export("maxVis")]
        float MaxVis { get; set; }

        // @property (assign, nonatomic) _Bool asyncFetching;
        [Export("asyncFetching")]
        bool AsyncFetching { get; set; }

        // @property (assign, nonatomic) NSTimeInterval viewUpdatePeriod;
        [Export("viewUpdatePeriod")]
        double ViewUpdatePeriod { get; set; }

        // @property (assign, nonatomic) float minUpdateDist;
        [Export("minUpdateDist")]
        float MinUpdateDist { get; set; }

        // @property (assign, nonatomic) _Bool waitLoad;
        [Export("waitLoad")]
        bool WaitLoad { get; set; }

        // @property (assign, nonatomic) NSTimeInterval waitLoadTimeout;
        [Export("waitLoadTimeout")]
        double WaitLoadTimeout { get; set; }

        // @property (assign, nonatomic) unsigned int imageDepth;
        [Export("imageDepth")]
        uint ImageDepth { get; set; }

        // @property (assign, nonatomic) float currentImage;
        [Export("currentImage")]
        float CurrentImage { get; set; }

        // @property (assign, nonatomic) float maxCurrentImage;
        [Export("maxCurrentImage")]
        float MaxCurrentImage { get; set; }

        // @property (assign, nonatomic) float animationPeriod;
        [Export("animationPeriod")]
        float AnimationPeriod { get; set; }

        // @property (assign, nonatomic) _Bool animationWrap;
        [Export("animationWrap")]
        bool AnimationWrap { get; set; }

        // @property (assign, nonatomic) _Bool allowFrameLoading;
        [Export("allowFrameLoading")]
        bool AllowFrameLoading { get; set; }

        // -(void)setFrameLoadingPriority:(NSArray * _Nullable)priorities;
        [Export("setFrameLoadingPriority:")]
        void SetFrameLoadingPriority([NullAllowed] NSObject[] priorities);

        // @property (assign, nonatomic) _Bool includeElevAttrForShader;
        [Export("includeElevAttrForShader")]
        bool IncludeElevAttrForShader { get; set; }

        // @property (assign, nonatomic) _Bool useElevAsZ;
        [Export("useElevAsZ")]
        bool UseElevAsZ { get; set; }

        // @property (assign, nonatomic) _Bool requireElev;
        [Export("requireElev")]
        bool RequireElev { get; set; }

        // @property (nonatomic, strong) UIColor * _Nullable color;
        [NullAllowed, Export("color", ArgumentSemantic.Strong)]
        UIColor Color { get; set; }

        // @property (nonatomic) int maxTiles;
        [Export("maxTiles")]
        int MaxTiles { get; set; }

        // @property (nonatomic) float importanceScale;
        [Export("importanceScale")]
        float ImportanceScale { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable shaderProgramName;
        [NullAllowed, Export("shaderProgramName", ArgumentSemantic.Strong)]
        string ShaderProgramName { get; set; }

        // @property (nonatomic) unsigned int texturAtlasSize;
        [Export("texturAtlasSize")]
        uint TexturAtlasSize { get; set; }

        // @property (nonatomic) MaplyQuadImageFormat imageFormat;
        [Export("imageFormat", ArgumentSemantic.Assign)]
        MaplyQuadImageFormat ImageFormat { get; set; }

        // @property (nonatomic) int borderTexel;
        [Export("borderTexel")]
        int BorderTexel { get; set; }

        // @property (nonatomic) _Bool flipY;
        [Export("flipY")]
        bool FlipY { get; set; }

        // @property (nonatomic) _Bool useTargetZoomLevel;
        [Export("useTargetZoomLevel")]
        bool UseTargetZoomLevel { get; set; }

        // @property (nonatomic) _Bool singleLevelLoading;
        [Export("singleLevelLoading")]
        bool SingleLevelLoading { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable multiLevelLoads;
        [NullAllowed, Export("multiLevelLoads", ArgumentSemantic.Strong)]
        NSObject[] MultiLevelLoads { get; set; }

        // -(int)targetZoomLevel;
        [Export("targetZoomLevel")]
        int TargetZoomLevel { get; }

        // -(void)setTesselationValues:(NSDictionary * _Nonnull)tessDict;
        [Export("setTesselationValues:")]
        void SetTesselationValues(NSDictionary tessDict);

        // -(void)reload;
        [Export("reload")]
        void Reload();

        // -(_Bool)loadedImages:(id _Nonnull)images forTile:(MaplyTileID)tileID;
        [Export("loadedImages:forTile:")]
        bool LoadedImages(NSObject images, MaplyTileID tileID);

        // -(_Bool)loadedImages:(id _Nonnull)images forTile:(MaplyTileID)tileID frame:(int)frame;
        [Export("loadedImages:forTile:frame:")]
        bool LoadedImages(NSObject images, MaplyTileID tileID, int frame);

        // -(void)loadedElevation:(MaplyElevationChunk * _Nonnull)elevChunk forTile:(MaplyTileID)tileID;
        [Export("loadedElevation:forTile:")]
        void LoadedElevation(MaplyElevationChunk elevChunk, MaplyTileID tileID);

        // -(void)loadedElevation:(MaplyElevationChunk * _Nonnull)elevChunk forTile:(MaplyTileID)tileID frame:(int)frame;
        [Export("loadedElevation:forTile:frame:")]
        void LoadedElevation(MaplyElevationChunk elevChunk, MaplyTileID tileID, int frame);

        // -(void)loadError:(NSError * _Nullable)error forTile:(MaplyTileID)tileID;
        [Export("loadError:forTile:")]
        void LoadError([NullAllowed] NSError error, MaplyTileID tileID);

        // -(void)loadError:(NSError * _Nullable)error forTile:(MaplyTileID)tileID frame:(int)frame;
        [Export("loadError:forTile:frame:")]
        void LoadError([NullAllowed] NSError error, MaplyTileID tileID, int frame);

        // -(NSArray * _Nullable)loadedFrames;
        [NullAllowed, Export("loadedFrames")]
        NSObject[] LoadedFrames { get; }

        // -(MaplyBoundingBox)geoBoundsForTile:(MaplyTileID)tileID;
        [Export("geoBoundsForTile:")]
        MaplyBoundingBox GeoBoundsForTile(MaplyTileID tileID);

        // -(void)geoBoundsForTile:(MaplyTileID)tileID bbox:(MaplyBoundingBox * _Nonnull)bbox;
        [Export("geoBoundsForTile:bbox:")]
        unsafe void GeoBoundsForTile(MaplyTileID tileID, ref MaplyBoundingBox bbox);

        // -(MaplyBoundingBox)boundsForTile:(MaplyTileID)tileID;
        [Export("boundsForTile:")]
        MaplyBoundingBox BoundsForTile(MaplyTileID tileID);

        // -(void)boundsForTile:(MaplyTileID)tileID bbox:(MaplyBoundingBox * _Nonnull)bbox;
        [Export("boundsForTile:bbox:")]
        unsafe void BoundsForTile(MaplyTileID tileID, ref MaplyBoundingBox bbox);

        // -(void)reset;
        [Export("reset")]
        void Reset();
    }

    // @interface MaplySticker : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplySticker
    {
        // @property (assign, nonatomic) MaplyCoordinate ll;
        [Export("ll", ArgumentSemantic.Assign)]
        MaplyCoordinate Ll { get; set; }

        // @property (assign, nonatomic) MaplyCoordinate ur;
        [Export("ur", ArgumentSemantic.Assign)]
        MaplyCoordinate Ur { get; set; }

        // @property (assign, nonatomic) float rotation;
        [Export("rotation")]
        float Rotation { get; set; }

        // @property (nonatomic, strong) MaplyCoordinateSystem * _Nullable coordSys;
        [NullAllowed, Export("coordSys", ArgumentSemantic.Strong)]
        MaplyCoordinateSystem CoordSys { get; set; }

        // @property (nonatomic, strong) id _Nullable image;
        [NullAllowed, Export("image", ArgumentSemantic.Strong)]
        UIImage Image { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable images;
        [NullAllowed, Export("images", ArgumentSemantic.Strong)]

        UIImage[] Images { get; set; }

        // @property (nonatomic) MaplyQuadImageFormat imageFormat;
        [Export("imageFormat", ArgumentSemantic.Assign)]
        MaplyQuadImageFormat ImageFormat { get; set; }
    }

    // @interface MaplyVectorObject : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyVectorObject
    {
        // @property (nonatomic, strong) id _Nullable userObject;
        [NullAllowed, Export("userObject", ArgumentSemantic.Strong)]
        NSObject UserObject { get; set; }

        // @property (assign, nonatomic) _Bool selectable;
        [Export("selectable")]
        bool Selectable { get; set; }

        // @property (readonly, nonatomic) NSMutableDictionary * _Nonnull attributes;
        [Export("attributes")]
        NSMutableDictionary Attributes { get; }

        // +(MaplyVectorObject * _Nullable)VectorObjectFromGeoJSON:(NSData * _Nonnull)geoJSON;
        [Static]
        [Export("VectorObjectFromGeoJSON:")]
        [return: NullAllowed]
        MaplyVectorObject VectorObjectFromGeoJSON(NSData geoJSON);

        // +(MaplyVectorObject * _Nullable)VectorObjectFromGeoJSONApple:(NSData * _Nonnull)geoJSON;
        [Static]
        [Export("VectorObjectFromGeoJSONApple:")]
        [return: NullAllowed]
        MaplyVectorObject VectorObjectFromGeoJSONApple(NSData geoJSON);

        // +(MaplyVectorObject * _Nullable)VectorObjectFromGeoJSONDictionary:(NSDictionary * _Nonnull)geoJSON;
        [Static]
        [Export("VectorObjectFromGeoJSONDictionary:")]
        [return: NullAllowed]
        MaplyVectorObject VectorObjectFromGeoJSONDictionary(NSDictionary geoJSON);

        // +(MaplyVectorObject * _Nullable)VectorObjectFromFile:(NSString * _Nonnull)fileName;
        [Static]
        [Export("VectorObjectFromFile:")]
        [return: NullAllowed]
        MaplyVectorObject VectorObjectFromFile(string fileName);

        // +(MaplyVectorObject * _Nullable)VectorObjectFromShapeFile:(NSString * _Nonnull)fileName;
        [Static]
        [Export("VectorObjectFromShapeFile:")]
        [return: NullAllowed]
        MaplyVectorObject VectorObjectFromShapeFile(string fileName);

        // +(NSDictionary * _Nullable)VectorObjectsFromGeoJSONAssembly:(NSData * _Nonnull)geoJSON;
        [Static]
        [Export("VectorObjectsFromGeoJSONAssembly:")]
        [return: NullAllowed]
        NSDictionary VectorObjectsFromGeoJSONAssembly(NSData geoJSON);

        // -(instancetype _Nonnull)initWithPoint:(MaplyCoordinate)coord attributes:(NSDictionary * _Nullable)attr;
        [Export("initWithPoint:attributes:")]
        IntPtr Constructor(MaplyCoordinate coord, [NullAllowed] NSDictionary attr);

        // -(instancetype _Nonnull)initWithPointRef:(MaplyCoordinate * _Nonnull)coord attributes:(NSDictionary * _Nullable)attr;
        [Export("initWithPointRef:attributes:")]
        unsafe IntPtr Constructor(NSNumber[] coord, [NullAllowed] NSDictionary attr);

        // -(instancetype _Nonnull)initWithLineString:(NSArray * _Nonnull)coords attributes:(NSDictionary * _Nullable)attr;
        [Export("initWithLineString:attributes:")]

        IntPtr Constructor(NSObject[] coords, [NullAllowed] NSDictionary attr);

        // -(instancetype _Nonnull)initWithLineString:(MaplyCoordinate * _Nonnull)coords numCoords:(int)numCoords attributes:(NSDictionary * _Nullable)attr;
        [Export("initWithLineString:numCoords:attributes:")]
        unsafe IntPtr Constructor(NSNumber[] coords, int numCoords, [NullAllowed] NSDictionary attr);

        //// -(instancetype _Nonnull)initWithAreal:(MaplyCoordinate * _Nonnull)coords numCoords:(int)numCoords attributes:(NSDictionary * _Nullable)attr;
        //[Export ("initWithAreal:numCoords:attributes:")] TODO
        //unsafe IntPtr Constructor (NSNumber[] coords, int numCoords, [NullAllowed] NSDictionary attr);

        // -(instancetype _Nullable)initWithGeoJSON:(NSData * _Nonnull)geoJSON;
        [Export("initWithGeoJSON:")]
        IntPtr Constructor(NSData geoJSON);

        //// -(instancetype _Nullable)initWithGeoJSONApple:(NSData * _Nonnull)geoJSON;
        //[Export ("initWithGeoJSONApple:")] TODO
        //IntPtr Constructor (NSData geoJSON);

        // -(instancetype _Nullable)initWithGeoJSONDictionary:(NSDictionary * _Nonnull)geoJSON;
        [Export("initWithGeoJSONDictionary:")]
        IntPtr Constructor(NSDictionary geoJSON);

        // -(instancetype _Nullable)initWithFile:(NSString * _Nonnull)fileName;
        [Export("initWithFile:")]
        IntPtr Constructor(string fileName);

        //// -(instancetype _Nullable)initWithShapeFile:(NSString * _Nonnull)fileName;
        //[Export ("initWithShapeFile:")] TODO
        //IntPtr Constructor (string fileName);

        // -(_Bool)writeToFile:(NSString * _Nonnull)fileName;
        [Export("writeToFile:")]
        bool WriteToFile(string fileName);

        // -(MaplyVectorObject * _Nonnull)deepCopy2;
        [Export("deepCopy2")]

        MaplyVectorObject DeepCopy2 { get; }

        // -(void)reprojectFrom:(MaplyCoordinateSystem * _Nonnull)srcSystem to:(MaplyCoordinateSystem * _Nonnull)destSystem;
        [Export("reprojectFrom:to:")]
        void ReprojectFrom(MaplyCoordinateSystem srcSystem, MaplyCoordinateSystem destSystem);

        // -(NSString * _Nonnull)log;
        [Export("log")]

        string Log { get; }

        // -(void)addHole:(MaplyCoordinate * _Nonnull)coords numCoords:(int)numCoords;
        [Export("addHole:numCoords:")]
        unsafe void AddHole(NSNumber[] coords, int numCoords);

        // -(MaplyVectorObjectType)vectorType;
        [Export("vectorType")]

        MaplyVectorObjectType VectorType { get; }

        // -(_Bool)pointInAreal:(MaplyCoordinate)coord;
        [Export("pointInAreal:")]
        bool PointInAreal(MaplyCoordinate coord);

        // -(_Bool)pointNearLinear:(MaplyCoordinate)coord distance:(float)maxDistance inViewController:(MaplyBaseViewController * _Nonnull)vc;
        [Export("pointNearLinear:distance:inViewController:")]
        bool PointNearLinear(MaplyCoordinate coord, float maxDistance, MaplyBaseViewController vc);

        // -(MaplyCoordinate)center;
        [Export("center")]

        MaplyCoordinate Center { get; }

        // -(void)mergeVectorsFrom:(MaplyVectorObject * _Nonnull)otherVec;
        [Export("mergeVectorsFrom:")]
        void MergeVectorsFrom(MaplyVectorObject otherVec);

        // -(_Bool)linearMiddle:(MaplyCoordinate * _Nonnull)middle rot:(double * _Nonnull)rot;
        [Export("linearMiddle:rot:")]
        unsafe bool LinearMiddle(ref MaplyCoordinate middle, ref double rot);

        // -(_Bool)linearMiddle:(MaplyCoordinate * _Nullable)middle rot:(double * _Nullable)rot displayCoordSys:(MaplyCoordinateSystem * _Nonnull)coordSys;
        [Export("linearMiddle:rot:displayCoordSys:")]
        unsafe bool LinearMiddle([NullAllowed] ref MaplyCoordinate middle, [NullAllowed] ref double rot, MaplyCoordinateSystem coordSys);

        // -(MaplyCoordinate)linearMiddle:(MaplyCoordinateSystem * _Nonnull)coordSys;
        [Export("linearMiddle:")]
        MaplyCoordinate LinearMiddle(MaplyCoordinateSystem coordSys);

        // -(double)linearMiddleRotation:(MaplyCoordinateSystem * _Nonnull)coordSys;
        [Export("linearMiddleRotation:")]
        double LinearMiddleRotation(MaplyCoordinateSystem coordSys);

        // -(MaplyCoordinate)middleCoordinate;
        [Export("middleCoordinate")]

        MaplyCoordinate MiddleCoordinate { get; }

        // -(_Bool)middleCoordinate:(MaplyCoordinate * _Nonnull)middle;
        [Export("middleCoordinate:")]
        unsafe bool SetMiddleCoordinate(NSNumber[] middle);

        // -(_Bool)largestLoopCenter:(MaplyCoordinate * _Nullable)center mbrLL:(MaplyCoordinate * _Nullable)ll mbrUR:(MaplyCoordinate * _Nullable)ur;
        [Export("largestLoopCenter:mbrLL:mbrUR:")]
        unsafe bool LargestLoopCenter([NullAllowed] NSNumber[] center, [NullAllowed] NSNumber[] ll, [NullAllowed] NSNumber[] ur);

        // -(MaplyCoordinate)centroid;
        [Export("centroid")]

        MaplyCoordinate Centroid { get; }

        // -(_Bool)centroid:(MaplyCoordinate * _Nonnull)centroid;
        [Export("centroid:")]
        unsafe bool SetCentroid(NSNumber[] centroid);

        // -(MaplyBoundingBox)boundingBox;
        [Export("boundingBox")]

        MaplyBoundingBox BoundingBox { get; }

        // -(_Bool)boundingBoxLL:(MaplyCoordinate * _Nonnull)ll ur:(MaplyCoordinate * _Nonnull)ur;
        [Export("boundingBoxLL:ur:")]
        unsafe bool BoundingBoxLL(NSNumber[] ll, NSNumber[] ur);

        // -(double)areaOfOuterLoops;
        [Export("areaOfOuterLoops")]

        double AreaOfOuterLoops { get; }

        // -(NSArray * _Nullable)asCLLocationArrays;
        [NullAllowed, Export("asCLLocationArrays")]

        NSObject[] AsCLLocationArrays { get; }

        // -(NSArray * _Nullable)asNumbers;
        [NullAllowed, Export("asNumbers")]

        NSObject[] AsNumbers { get; }

        // -(NSArray * _Nonnull)splitVectors;
        [Export("splitVectors")]

        NSObject[] SplitVectors { get; }

        // -(void)subdivideToGlobe:(float)epsilon;
        [Export("subdivideToGlobe:")]
        void SubdivideToGlobe(float epsilon);

        // -(void)subdivideToGlobeGreatCircle:(float)epsilon;
        [Export("subdivideToGlobeGreatCircle:")]
        void SubdivideToGlobeGreatCircle(float epsilon);

        // -(void)subdivideToFlatGreatCircle:(float)epsilon;
        [Export("subdivideToFlatGreatCircle:")]
        void SubdivideToFlatGreatCircle(float epsilon);

        // -(MaplyVectorObject * _Nonnull)tesselate;
        [Export("tesselate")]

        MaplyVectorObject Tesselate { get; }

        // -(MaplyVectorObject * _Nullable)clipToGrid:(CGSize)gridSize;
        [Export("clipToGrid:")]
        [return: NullAllowed]
        MaplyVectorObject ClipToGrid(CGSize gridSize);

        // -(MaplyVectorObject * _Nullable)clipToMbr:(MaplyCoordinate)ll upperRight:(MaplyCoordinate)ur;
        [Export("clipToMbr:upperRight:")]
        [return: NullAllowed]
        MaplyVectorObject ClipToMbr(MaplyCoordinate ll, MaplyCoordinate ur);
    }

    // @interface MaplyVectorDatabase : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyVectorDatabase
    {
        // +(MaplyVectorDatabase * _Nonnull)vectorDatabaseWithShape:(NSString * _Nonnull)shapeName __attribute__((deprecated("")));
        [Static]
        [Export("vectorDatabaseWithShape:")]
        MaplyVectorDatabase VectorDatabaseWithShape(string shapeName);

        // -(instancetype _Nonnull)initWithShape:(NSString * _Nonnull)shapeName;
        [Export("initWithShape:")]
        IntPtr Constructor(string shapeName);

        // -(MaplyVectorObject * _Nullable)fetchMatchingVectors:(NSString * _Nonnull)sqlQuery;
        [Export("fetchMatchingVectors:")]
        [return: NullAllowed]
        MaplyVectorObject FetchMatchingVectors(string sqlQuery);

        // -(MaplyVectorObject * _Nullable)fetchArealsForPoint:(MaplyCoordinate)coord;
        [Export("fetchArealsForPoint:")]
        [return: NullAllowed]
        MaplyVectorObject FetchArealsForPoint(MaplyCoordinate coord);

        // -(MaplyVectorObject * _Nullable)fetchAllVectors;
        [NullAllowed, Export("fetchAllVectors")]

        MaplyVectorObject FetchAllVectors { get; }
    }

    // @interface MaplyViewTracker : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyViewTracker
    {
        // @property (nonatomic, strong) UIView * _Nullable view;
        [NullAllowed, Export("view", ArgumentSemantic.Strong)]
        UIView View { get; set; }

        // @property (assign, nonatomic) MaplyCoordinate loc;
        [Export("loc", ArgumentSemantic.Assign)]
        MaplyCoordinate Loc { get; set; }

        // @property (assign, nonatomic) CGPoint offset;
        [Export("offset", ArgumentSemantic.Assign)]
        CGPoint Offset { get; set; }

        // @property (assign, nonatomic) float minVis;
        [Export("minVis")]
        float MinVis { get; set; }

        // @property (assign, nonatomic) float maxVis;
        [Export("maxVis")]
        float MaxVis { get; set; }
    }

    // @interface MaplyComponentObject : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyComponentObject
    {
        // -(instancetype _Nonnull)initWithDesc:(NSDictionary * _Nonnull)desc;
        [Export("initWithDesc:")]
        IntPtr Constructor(NSDictionary desc);
    }

    partial interface Constants
    {
        // extern NSString *const kMaplyRenderHintZBuffer;
        [Field("kMaplyRenderHintZBuffer", "__Internal")]
        NSString kMaplyRenderHintZBuffer { get; }

        // extern NSString *const kMaplyRenderHintCulling;
        [Field("kMaplyRenderHintCulling", "__Internal")]
        NSString kMaplyRenderHintCulling { get; }

        // extern NSString *const kMaplyRendererLightingMode;
        [Field("kMaplyRendererLightingMode", "__Internal")]
        NSString kMaplyRendererLightingMode { get; }

        // extern NSString *const kMaplyDrawOffset;
        [Field("kMaplyDrawOffset", "__Internal")]
        NSString kMaplyDrawOffset { get; }

        // extern NSString *const kMaplyDrawPriority;
        [Field("kMaplyDrawPriority", "__Internal")]
        NSString kMaplyDrawPriority { get; }

        // extern NSString *const kMaplyMinVis;
        [Field("kMaplyMinVis", "__Internal")]
        NSString kMaplyMinVis { get; }

        // extern NSString *const kMaplyMaxVis;
        [Field("kMaplyMaxVis", "__Internal")]
        NSString kMaplyMaxVis { get; }

        // extern NSString *const kMaplyViewerMinDist;
        [Field("kMaplyViewerMinDist", "__Internal")]
        NSString kMaplyViewerMinDist { get; }

        // extern NSString *const kMaplyViewerMaxDist;
        [Field("kMaplyViewerMaxDist", "__Internal")]
        NSString kMaplyViewerMaxDist { get; }

        // extern NSString *const kMaplyViewableCenterX;
        [Field("kMaplyViewableCenterX", "__Internal")]
        NSString kMaplyViewableCenterX { get; }

        // extern NSString *const kMaplyViewableCenterY;
        [Field("kMaplyViewableCenterY", "__Internal")]
        NSString kMaplyViewableCenterY { get; }

        // extern NSString *const kMaplyViewableCenterZ;
        [Field("kMaplyViewableCenterZ", "__Internal")]
        NSString kMaplyViewableCenterZ { get; }

        // extern NSString *const kMaplyFade;
        [Field("kMaplyFade", "__Internal")]
        NSString kMaplyFade { get; }

        // extern NSString *const kMaplyFadeIn;
        [Field("kMaplyFadeIn", "__Internal")]
        NSString kMaplyFadeIn { get; }

        // extern NSString *const kMaplyFadeOut;
        [Field("kMaplyFadeOut", "__Internal")]
        NSString kMaplyFadeOut { get; }

        // extern NSString *const kMaplyFadeOutTime;
        [Field("kMaplyFadeOutTime", "__Internal")]
        NSString kMaplyFadeOutTime { get; }

        // extern NSString *const kMaplyEnable;
        [Field("kMaplyEnable", "__Internal")]
        NSString kMaplyEnable { get; }

        // extern NSString *const kMaplyEnableStart;
        [Field("kMaplyEnableStart", "__Internal")]
        NSString kMaplyEnableStart { get; }

        // extern NSString *const kMaplyEnableEnd;
        [Field("kMaplyEnableEnd", "__Internal")]
        NSString kMaplyEnableEnd { get; }

        // extern NSString *const kMaplyZBufferRead;
        [Field("kMaplyZBufferRead", "__Internal")]
        NSString kMaplyZBufferRead { get; }

        // extern NSString *const kMaplyZBufferWrite;
        [Field("kMaplyZBufferWrite", "__Internal")]
        NSString kMaplyZBufferWrite { get; }

        // extern NSString *const kMaplyShader;
        [Field("kMaplyShader", "__Internal")]
        NSString kMaplyShader { get; }

        // extern NSString *const kMaplyShaderUniforms;
        [Field("kMaplyShaderUniforms", "__Internal")]
        NSString kMaplyShaderUniforms { get; }

        // extern const int kMaplyStarsDrawPriorityDefault;
        [Field("kMaplyStarsDrawPriorityDefault", "__Internal")]
        int kMaplyStarsDrawPriorityDefault { get; }

        // extern const int kMaplySunDrawPriorityDefault;
        [Field("kMaplySunDrawPriorityDefault", "__Internal")]
        int kMaplySunDrawPriorityDefault { get; }

        // extern const int kMaplyMoonDrawPriorityDefault;
        [Field("kMaplyMoonDrawPriorityDefault", "__Internal")]
        int kMaplyMoonDrawPriorityDefault { get; }

        // extern const int kMaplyAtmosphereDrawPriorityDefault;
        [Field("kMaplyAtmosphereDrawPriorityDefault", "__Internal")]
        int kMaplyAtmosphereDrawPriorityDefault { get; }

        // extern const int kMaplyImageLayerDrawPriorityDefault;
        [Field("kMaplyImageLayerDrawPriorityDefault", "__Internal")]
        int kMaplyImageLayerDrawPriorityDefault { get; }

        // extern const int kMaplyFeatureDrawPriorityBase;
        [Field("kMaplyFeatureDrawPriorityBase", "__Internal")]
        int kMaplyFeatureDrawPriorityBase { get; }

        // extern const int kMaplyStickerDrawPriorityDefault;
        [Field("kMaplyStickerDrawPriorityDefault", "__Internal")]
        int kMaplyStickerDrawPriorityDefault { get; }

        // extern const int kMaplyMarkerDrawPriorityDefault;
        [Field("kMaplyMarkerDrawPriorityDefault", "__Internal")]
        int kMaplyMarkerDrawPriorityDefault { get; }

        // extern const int kMaplyVectorDrawPriorityDefault;
        [Field("kMaplyVectorDrawPriorityDefault", "__Internal")]
        int kMaplyVectorDrawPriorityDefault { get; }

        // extern const int kMaplyParticleSystemDrawPriorityDefault;
        [Field("kMaplyParticleSystemDrawPriorityDefault", "__Internal")]
        int kMaplyParticleSystemDrawPriorityDefault { get; }

        // extern const int kMaplyLabelDrawPriorityDefault;
        [Field("kMaplyLabelDrawPriorityDefault", "__Internal")]
        int kMaplyLabelDrawPriorityDefault { get; }

        // extern const int kMaplyLoftedPolysDrawPriorityDefault;
        [Field("kMaplyLoftedPolysDrawPriorityDefault", "__Internal")]
        int kMaplyLoftedPolysDrawPriorityDefault { get; }

        // extern const int kMaplyShapeDrawPriorityDefault;
        [Field("kMaplyShapeDrawPriorityDefault", "__Internal")]
        int kMaplyShapeDrawPriorityDefault { get; }

        // extern const int kMaplyBillboardDrawPriorityDefault;
        [Field("kMaplyBillboardDrawPriorityDefault", "__Internal")]
        int kMaplyBillboardDrawPriorityDefault { get; }

        // extern const int kMaplyModelDrawPriorityDefault;
        [Field("kMaplyModelDrawPriorityDefault", "__Internal")]
        int kMaplyModelDrawPriorityDefault { get; }

        // extern NSString *const kMaplyTextColor;
        [Field("kMaplyTextColor", "__Internal")]
        NSString kMaplyTextColor { get; }

        // extern NSString *const kMaplyBackgroundColor;
        [Field("kMaplyBackgroundColor", "__Internal")]
        NSString kMaplyBackgroundColor { get; }

        // extern NSString *const kMaplyFont;
        [Field("kMaplyFont", "__Internal")]
        NSString kMaplyFont { get; }

        // extern NSString *const kMaplyLabelHeight;
        [Field("kMaplyLabelHeight", "__Internal")]
        NSString kMaplyLabelHeight { get; }

        // extern NSString *const kMaplyLabelWidth;
        [Field("kMaplyLabelWidth", "__Internal")]
        NSString kMaplyLabelWidth { get; }

        // extern NSString *const kMaplyJustify;
        [Field("kMaplyJustify", "__Internal")]
        NSString kMaplyJustify { get; }

        // extern NSString *const kMaplyShadowSize;
        [Field("kMaplyShadowSize", "__Internal")]
        NSString kMaplyShadowSize { get; }

        // extern NSString *const kMaplyShadowColor;
        [Field("kMaplyShadowColor", "__Internal")]
        NSString kMaplyShadowColor { get; }

        // extern NSString *const kMaplyTextOutlineSize;
        [Field("kMaplyTextOutlineSize", "__Internal")]
        NSString kMaplyTextOutlineSize { get; }

        // extern NSString *const kMaplyTextOutlineColor;
        [Field("kMaplyTextOutlineColor", "__Internal")]
        NSString kMaplyTextOutlineColor { get; }

        // extern NSString *const kMaplyClusterGroup;
        [Field("kMaplyClusterGroup", "__Internal")]
        NSString kMaplyClusterGroup { get; }

        // extern NSString *const kMaplyColor;
        [Field("kMaplyColor", "__Internal")]
        NSString kMaplyColor { get; }

        // extern NSString *const kMaplyVecWidth;
        [Field("kMaplyVecWidth", "__Internal")]
        NSString kMaplyVecWidth { get; }

        // extern NSString *const kMaplyFilled;
        [Field("kMaplyFilled", "__Internal")]
        NSString kMaplyFilled { get; }

        // extern NSString *const kMaplyVecTexture;
        [Field("kMaplyVecTexture", "__Internal")]
        NSString kMaplyVecTexture { get; }

        // extern NSString *const kMaplyVecTexScaleX;
        [Field("kMaplyVecTexScaleX", "__Internal")]
        NSString kMaplyVecTexScaleX { get; }

        // extern NSString *const kMaplyVecTexScaleY;
        [Field("kMaplyVecTexScaleY", "__Internal")]
        NSString kMaplyVecTexScaleY { get; }

        // extern NSString *const kMaplyVecTextureProjection;
        [Field("kMaplyVecTextureProjection", "__Internal")]
        NSString kMaplyVecTextureProjection { get; }

        // extern NSString *const kMaplyProjectionTangentPlane;
        [Field("kMaplyProjectionTangentPlane", "__Internal")]
        NSString kMaplyProjectionTangentPlane { get; }

        // extern NSString *const kMaplyProjectionScreen;
        [Field("kMaplyProjectionScreen", "__Internal")]
        NSString kMaplyProjectionScreen { get; }

        // extern NSString *const kMaplyVecCentered;
        [Field("kMaplyVecCentered", "__Internal")]
        NSString kMaplyVecCentered { get; }

        // extern NSString *const kMaplyVecCenterX;
        [Field("kMaplyVecCenterX", "__Internal")]
        NSString kMaplyVecCenterX { get; }

        // extern NSString *const kMaplyVecCenterY;
        [Field("kMaplyVecCenterY", "__Internal")]
        NSString kMaplyVecCenterY { get; }

        // extern NSString *const kMaplyWideVecCoordType;
        [Field("kMaplyWideVecCoordType", "__Internal")]
        NSString kMaplyWideVecCoordType { get; }

        // extern NSString *const kMaplyWideVecCoordTypeReal;
        [Field("kMaplyWideVecCoordTypeReal", "__Internal")]
        NSString kMaplyWideVecCoordTypeReal { get; }

        // extern NSString *const kMaplyWideVecCoordTypeScreen;
        [Field("kMaplyWideVecCoordTypeScreen", "__Internal")]
        NSString kMaplyWideVecCoordTypeScreen { get; }

        // extern NSString *const kMaplyWideVecJoinType;
        [Field("kMaplyWideVecJoinType", "__Internal")]
        NSString kMaplyWideVecJoinType { get; }

        // extern NSString *const kMaplyWideVecMiterJoin;
        [Field("kMaplyWideVecMiterJoin", "__Internal")]
        NSString kMaplyWideVecMiterJoin { get; }

        // extern NSString *const kMaplyWideVecBevelJoin;
        [Field("kMaplyWideVecBevelJoin", "__Internal")]
        NSString kMaplyWideVecBevelJoin { get; }

        // extern NSString *const kMaplyWideVecEdgeFalloff;
        [Field("kMaplyWideVecEdgeFalloff", "__Internal")]
        NSString kMaplyWideVecEdgeFalloff { get; }

        // extern NSString *const kMaplyWideVecMiterLimit;
        [Field("kMaplyWideVecMiterLimit", "__Internal")]
        NSString kMaplyWideVecMiterLimit { get; }

        // extern NSString *const kMaplyWideVecTexRepeatLen;
        [Field("kMaplyWideVecTexRepeatLen", "__Internal")]
        NSString kMaplyWideVecTexRepeatLen { get; }

        // extern NSString *const kMaplySubdivEpsilon;
        [Field("kMaplySubdivEpsilon", "__Internal")]
        NSString kMaplySubdivEpsilon { get; }

        // extern NSString *const kMaplySubdivType;
        [Field("kMaplySubdivType", "__Internal")]
        NSString kMaplySubdivType { get; }

        // extern NSString *const kMaplySubdivGreatCircle;
        [Field("kMaplySubdivGreatCircle", "__Internal")]
        NSString kMaplySubdivGreatCircle { get; }

        // extern NSString *const kMaplySubdivStatic;
        [Field("kMaplySubdivStatic", "__Internal")]
        NSString kMaplySubdivStatic { get; }

        // extern NSString *const kMaplySubdivSimple;
        [Field("kMaplySubdivSimple", "__Internal")]
        NSString kMaplySubdivSimple { get; }

        // extern NSString *const kMaplySubdivGrid;
        [Field("kMaplySubdivGrid", "__Internal")]
        NSString kMaplySubdivGrid { get; }

        // extern NSString *const kMaplySelectable;
        [Field("kMaplySelectable", "__Internal")]
        NSString kMaplySelectable { get; }

        // extern NSString *const kMaplySampleX;
        [Field("kMaplySampleX", "__Internal")]
        NSString kMaplySampleX { get; }

        // extern NSString *const kMaplySampleY;
        [Field("kMaplySampleY", "__Internal")]
        NSString kMaplySampleY { get; }

        // extern NSString *const kMaplyStickerImages;
        [Field("kMaplyStickerImages", "__Internal")]
        NSString kMaplyStickerImages { get; }

        // extern NSString *const kMaplyStickerImageFormat;
        [Field("kMaplyStickerImageFormat", "__Internal")]
        NSString kMaplyStickerImageFormat { get; }

        // extern NSString *const kMaplyBillboardOrient;
        [Field("kMaplyBillboardOrient", "__Internal")]
        NSString kMaplyBillboardOrient { get; }

        // extern NSString *const kMaplyBillboardOrientGround;
        [Field("kMaplyBillboardOrientGround", "__Internal")]
        NSString kMaplyBillboardOrientGround { get; }

        // extern NSString *const kMaplyBillboardOrientEye;
        [Field("kMaplyBillboardOrientEye", "__Internal")]
        NSString kMaplyBillboardOrientEye { get; }

        // extern NSString *const kMaplyLoftedPolyHeight;
        [Field("kMaplyLoftedPolyHeight", "__Internal")]
        NSString kMaplyLoftedPolyHeight { get; }

        // extern NSString *const kMaplyLoftedPolyTop;
        [Field("kMaplyLoftedPolyTop", "__Internal")]
        NSString kMaplyLoftedPolyTop { get; }

        // extern NSString *const kMaplyLoftedPolySide;
        [Field("kMaplyLoftedPolySide", "__Internal")]
        NSString kMaplyLoftedPolySide { get; }

        // extern NSString *const kMaplyLoftedPolyBase;
        [Field("kMaplyLoftedPolyBase", "__Internal")]
        NSString kMaplyLoftedPolyBase { get; }

        // extern NSString *const kMaplyLoftedPolyGridSize;
        [Field("kMaplyLoftedPolyGridSize", "__Internal")]
        NSString kMaplyLoftedPolyGridSize { get; }

        // extern NSString *const kMaplyLoftedPolyOutline;
        [Field("kMaplyLoftedPolyOutline", "__Internal")]
        NSString kMaplyLoftedPolyOutline { get; }

        // extern NSString *const kMaplyLoftedPolyOutlineBottom;
        [Field("kMaplyLoftedPolyOutlineBottom", "__Internal")]
        NSString kMaplyLoftedPolyOutlineBottom { get; }

        // extern NSString *const kMaplyLoftedPolyOutlineColor;
        [Field("kMaplyLoftedPolyOutlineColor", "__Internal")]
        NSString kMaplyLoftedPolyOutlineColor { get; }

        // extern NSString *const kMaplyLoftedPolyOutlineWidth;
        [Field("kMaplyLoftedPolyOutlineWidth", "__Internal")]
        NSString kMaplyLoftedPolyOutlineWidth { get; }

        // extern NSString *const kMaplyLoftedPolyOutlineDrawPriority;
        [Field("kMaplyLoftedPolyOutlineDrawPriority", "__Internal")]
        NSString kMaplyLoftedPolyOutlineDrawPriority { get; }

        // extern NSString *const kMaplyLoftedPolyOutlineSide;
        [Field("kMaplyLoftedPolyOutlineSide", "__Internal")]
        NSString kMaplyLoftedPolyOutlineSide { get; }

        // extern NSString *const kMaplyShapeSampleX;
        [Field("kMaplyShapeSampleX", "__Internal")]
        NSString kMaplyShapeSampleX { get; }

        // extern NSString *const kMaplyShapeSampleY;
        [Field("kMaplyShapeSampleY", "__Internal")]
        NSString kMaplyShapeSampleY { get; }

        // extern NSString *const kMaplyShapeInsideOut;
        [Field("kMaplyShapeInsideOut", "__Internal")]
        NSString kMaplyShapeInsideOut { get; }

        // extern NSString *const kMaplyShapeCenterX;
        [Field("kMaplyShapeCenterX", "__Internal")]
        NSString kMaplyShapeCenterX { get; }

        // extern NSString *const kMaplyShapeCenterY;
        [Field("kMaplyShapeCenterY", "__Internal")]
        NSString kMaplyShapeCenterY { get; }

        // extern NSString *const kMaplyShapeCenterZ;
        [Field("kMaplyShapeCenterZ", "__Internal")]
        NSString kMaplyShapeCenterZ { get; }

        // extern NSString *const kMaplyVecHeight;
        [Field("kMaplyVecHeight", "__Internal")]
        NSString kMaplyVecHeight { get; }

        // extern NSString *const kMaplyVecMinSample;
        [Field("kMaplyVecMinSample", "__Internal")]
        NSString kMaplyVecMinSample { get; }

        // extern NSString *const kMaplyPointSize;
        [Field("kMaplyPointSize", "__Internal")]
        NSString kMaplyPointSize { get; }

        // extern const float kMaplyPointSizeDefault;
        [Field("kMaplyPointSizeDefault", "__Internal")]
        float kMaplyPointSizeDefault { get; }

        // extern NSString *const kMaplyTexFormat;
        [Field("kMaplyTexFormat", "__Internal")]
        NSString kMaplyTexFormat { get; }

        // extern NSString *const kMaplyTexMinFilter;
        [Field("kMaplyTexMinFilter", "__Internal")]
        NSString kMaplyTexMinFilter { get; }

        // extern NSString *const kMaplyTexMagFilter;
        [Field("kMaplyTexMagFilter", "__Internal")]
        NSString kMaplyTexMagFilter { get; }

        // extern NSString *const kMaplyMinFilterNearest;
        [Field("kMaplyMinFilterNearest", "__Internal")]
        NSString kMaplyMinFilterNearest { get; }

        // extern NSString *const kMaplyMinFilterLinear;
        [Field("kMaplyMinFilterLinear", "__Internal")]
        NSString kMaplyMinFilterLinear { get; }

        // extern NSString *const kMaplyTexAtlas;
        [Field("kMaplyTexAtlas", "__Internal")]
        NSString kMaplyTexAtlas { get; }

        // extern NSString *const kMaplyTexWrapX;
        [Field("kMaplyTexWrapX", "__Internal")]
        NSString kMaplyTexWrapX { get; }

        // extern NSString *const kMaplyTexWrapY;
        [Field("kMaplyTexWrapY", "__Internal")]
        NSString kMaplyTexWrapY { get; }

        // extern NSString *const kMaplyShaderDefaultTri;
        [Field("kMaplyShaderDefaultTri", "__Internal")]
        NSString kMaplyShaderDefaultTri { get; }

        // extern NSString *const kMaplyDefaultTriangleShader;
        [Field("kMaplyDefaultTriangleShader", "__Internal")]
        NSString kMaplyDefaultTriangleShader { get; }

        // extern NSString *const kMaplyShaderDefaultModelTri;
        [Field("kMaplyShaderDefaultModelTri", "__Internal")]
        NSString kMaplyShaderDefaultModelTri { get; }

        // extern NSString *const kMaplyShaderDefaultTriNoLighting;
        [Field("kMaplyShaderDefaultTriNoLighting", "__Internal")]
        NSString kMaplyShaderDefaultTriNoLighting { get; }

        // extern NSString *const kMaplyNoLightTriangleShader;
        [Field("kMaplyNoLightTriangleShader", "__Internal")]
        NSString kMaplyNoLightTriangleShader { get; }

        // extern NSString *const kMaplyShaderDefaultTriScreenTex;
        [Field("kMaplyShaderDefaultTriScreenTex", "__Internal")]
        NSString kMaplyShaderDefaultTriScreenTex { get; }

        // extern NSString *const kMaplyShaderDefaultTriMultiTex;
        [Field("kMaplyShaderDefaultTriMultiTex", "__Internal")]
        NSString kMaplyShaderDefaultTriMultiTex { get; }

        // extern NSString *const kMaplyShaderDefaultTriMultiTexRamp;
        [Field("kMaplyShaderDefaultTriMultiTexRamp", "__Internal")]
        NSString kMaplyShaderDefaultTriMultiTexRamp { get; }

        // extern NSString *const kMaplyShaderDefaultTriNightDay;
        [Field("kMaplyShaderDefaultTriNightDay", "__Internal")]
        NSString kMaplyShaderDefaultTriNightDay { get; }

        // extern NSString *const kMaplyShaderDefaultLine;
        [Field("kMaplyShaderDefaultLine", "__Internal")]
        NSString kMaplyShaderDefaultLine { get; }

        // extern NSString *const kMaplyDefaultLineShader;
        [Field("kMaplyDefaultLineShader", "__Internal")]
        NSString kMaplyDefaultLineShader { get; }

        // extern NSString *const kMaplyShaderDefaultLineNoBackface;
        [Field("kMaplyShaderDefaultLineNoBackface", "__Internal")]
        NSString kMaplyShaderDefaultLineNoBackface { get; }

        // extern NSString *const kMaplyNoBackfaceLineShader;
        [Field("kMaplyNoBackfaceLineShader", "__Internal")]
        NSString kMaplyNoBackfaceLineShader { get; }

        // extern NSString *const kMaplyShaderBillboardGround;
        [Field("kMaplyShaderBillboardGround", "__Internal")]
        NSString kMaplyShaderBillboardGround { get; }

        // extern NSString *const kMaplyShaderBillboardEye;
        [Field("kMaplyShaderBillboardEye", "__Internal")]
        NSString kMaplyShaderBillboardEye { get; }

        // extern NSString *const kMaplyShaderParticleSystemPointDefault;
        [Field("kMaplyShaderParticleSystemPointDefault", "__Internal")]
        NSString kMaplyShaderParticleSystemPointDefault { get; }
    }

    // @interface MaplyLight : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyLight
    {
        // @property (assign, nonatomic) MaplyCoordinate3d pos;
        [Export("pos", ArgumentSemantic.Assign)]
        MaplyCoordinate3d Pos { get; set; }

        // @property (assign, nonatomic) _Bool viewDependent;
        [Export("viewDependent")]
        bool ViewDependent { get; set; }

        // @property (nonatomic, strong) UIColor * _Nullable ambient;
        [NullAllowed, Export("ambient", ArgumentSemantic.Strong)]
        UIColor Ambient { get; set; }

        // @property (nonatomic, strong) UIColor * _Nullable diffuse;
        [NullAllowed, Export("diffuse", ArgumentSemantic.Strong)]
        UIColor Diffuse { get; set; }
    }

    // @interface MaplyShader : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyShader
    {
        // -(instancetype _Nullable)initWithName:(NSString * _Nonnull)name vertexFile:(NSString * _Nonnull)vertexFileName fragmentFile:(NSString * _Nonnull)fragFileName viewC:(MaplyBaseViewController * _Nonnull)baseViewC;
        [Export("initWithName:vertexFile:fragmentFile:viewC:")]
        IntPtr Constructor(string name, string vertexFileName, string fragFileName, MaplyBaseViewController baseViewC);

        //// -(instancetype _Nullable)initWithName:(NSString * _Nonnull)name vertex:(NSString * _Nonnull)vertexProg fragment:(NSString * _Nonnull)fragProg viewC:(MaplyBaseViewController * _Nonnull)baseViewC;
        //[Export ("initWithName:vertex:fragment:viewC:")] TODO
        //IntPtr Constructor (string name, string vertexProg, string fragProg, MaplyBaseViewController baseViewC);

        // @property (nonatomic, strong) NSString * _Nullable name;
        [NullAllowed, Export("name", ArgumentSemantic.Strong)]
        string Name { get; set; }

        // -(void)addTextureNamed:(NSString * _Nonnull)shaderAttrName image:(UIImage * _Nonnull)image;
        [Export("addTextureNamed:image:")]
        void AddTextureNamed(string shaderAttrName, UIImage image);

        // -(_Bool)setUniformFloatNamed:(NSString * _Nonnull)uniName val:(float)val;
        [Export("setUniformFloatNamed:val:")]
        bool SetUniformFloatNamed(string uniName, float val);

        // -(_Bool)setUniformIntNamed:(NSString * _Nonnull)uniName val:(int)val;
        [Export("setUniformIntNamed:val:")]
        bool SetUniformIntNamed(string uniName, int val);

        // -(_Bool)setUniformVector2Named:(NSString * _Nonnull)uniName x:(float)x y:(float)y;
        [Export("setUniformVector2Named:x:y:")]
        bool SetUniformVector2Named(string uniName, float x, float y);

        // -(_Bool)setUniformVector3Named:(NSString * _Nonnull)uniName x:(float)x y:(float)y z:(float)z;
        [Export("setUniformVector3Named:x:y:z:")]
        bool SetUniformVector3Named(string uniName, float x, float y, float z);

        // -(_Bool)setUniformVector4Named:(NSString * _Nonnull)uniName x:(float)x y:(float)y z:(float)z w:(float)w;
        [Export("setUniformVector4Named:x:y:z:w:")]
        bool SetUniformVector4Named(string uniName, float x, float y, float z, float w);

        // -(_Bool)valid;
        [Export("valid")]

        bool Valid { get; }

        // -(NSString * _Nullable)getError;
        [NullAllowed, Export("getError")]

        string Error { get; }
    }

    // @interface MaplyActiveObject : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyActiveObject
    {
        // -(instancetype _Nonnull)initWithViewController:(MaplyBaseViewController * _Nonnull)viewC;
        [Export("initWithViewController:")]
        IntPtr Constructor(MaplyBaseViewController viewC);

        // @property (readonly, nonatomic, weak) MaplyBaseViewController * _Nullable viewC;
        [NullAllowed, Export("viewC", ArgumentSemantic.Weak)]
        MaplyBaseViewController ViewC { get; }
    }

    // @interface MaplyElevationChunk : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyElevationChunk
    {
    }

    // @interface MaplyElevationGridChunk : MaplyElevationChunk
    [BaseType(typeof(MaplyElevationChunk))]
    interface MaplyElevationGridChunk
    {
        // -(instancetype _Nonnull)initWithGridData:(NSData * _Nonnull)data sizeX:(unsigned int)sizeX sizeY:(unsigned int)sizeY;
        [Export("initWithGridData:sizeX:sizeY:")]
        IntPtr Constructor(NSData data, uint sizeX, uint sizeY);
    }

    // @interface MaplyElevationCesiumChunk : MaplyElevationChunk
    [BaseType(typeof(MaplyElevationChunk))]
    interface MaplyElevationCesiumChunk
    {
        // -(instancetype _Nonnull)initWithCesiumData:(NSData * _Nonnull)data sizeX:(unsigned int)sizeX sizeY:(unsigned int)sizeY;
        [Export("initWithCesiumData:sizeX:sizeY:")]
        IntPtr Constructor(NSData data, uint sizeX, uint sizeY);

        // @property (nonatomic) float scale;
        [Export("scale")]
        float Scale { get; set; }
    }

    // @protocol MaplyElevationSourceDelegate
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface MaplyElevationSourceDelegate
    {
        // @required -(MaplyCoordinateSystem * _Nullable)getCoordSystem;
        [Abstract]
        [NullAllowed, Export("getCoordSystem")]

        MaplyCoordinateSystem CoordSystem { get; }

        // @required -(int)minZoom;
        [Abstract]
        [Export("minZoom")]

        int MinZoom { get; }

        // @required -(int)maxZoom;
        [Abstract]
        [Export("maxZoom")]

        int MaxZoom { get; }

        // @required -(MaplyElevationChunk * _Nullable)elevForTile:(MaplyTileID)tileID;
        [Abstract]
        [Export("elevForTile:")]
        [return: NullAllowed]
        MaplyElevationChunk ElevForTile(MaplyTileID tileID);

        // @required -(_Bool)tileIsLocal:(MaplyTileID)tileID frame:(int)frame;
        [Abstract]
        [Export("tileIsLocal:frame:")]
        bool TileIsLocal(MaplyTileID tileID, int frame);
    }

    // @interface MaplyElevationSourceTester : NSObject <MaplyElevationSourceDelegate>
    [BaseType(typeof(NSObject))]
    interface MaplyElevationSourceTester : MaplyElevationSourceDelegate
    {
    }

    // @interface MaplyAnnotation : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyAnnotation
    {
        // @property (assign, nonatomic) float minVis;
        [Export("minVis")]
        float MinVis { get; set; }

        // @property (assign, nonatomic) float maxVis;
        [Export("maxVis")]
        float MaxVis { get; set; }

        // @property (nonatomic, strong) NSString * title;
        [Export("title", ArgumentSemantic.Strong)]
        string Title { get; set; }

        // @property (nonatomic, strong) NSString * subTitle;
        [Export("subTitle", ArgumentSemantic.Strong)]
        string SubTitle { get; set; }

        // @property (nonatomic, strong) UIView * leftAccessoryView;
        [Export("leftAccessoryView", ArgumentSemantic.Strong)]
        UIView LeftAccessoryView { get; set; }

        // @property (nonatomic, strong) UIView * rightAccessoryView;
        [Export("rightAccessoryView", ArgumentSemantic.Strong)]
        UIView RightAccessoryView { get; set; }

        // @property (nonatomic, strong) UIView * titleView;
        [Export("titleView", ArgumentSemantic.Strong)]
        UIView TitleView { get; set; }

        // @property (nonatomic, strong) UIView * subtitleView;
        [Export("subtitleView", ArgumentSemantic.Strong)]
        UIView SubtitleView { get; set; }

        // @property (nonatomic, strong) UIView * contentView;
        [Export("contentView", ArgumentSemantic.Strong)]
        UIView ContentView { get; set; }

        // @property (readonly, nonatomic) MaplyCoordinate loc;
        [Export("loc")]
        MaplyCoordinate Loc { get; }

        // @property (nonatomic) _Bool repositionForVisibility;
        [Export("repositionForVisibility")]
        bool RepositionForVisibility { get; set; }
    }

    // @interface MaplyParticleSystem : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyParticleSystem
    {
        // @property (nonatomic, strong) NSString * _Nullable name;
        [NullAllowed, Export("name", ArgumentSemantic.Strong)]
        string Name { get; set; }

        // @property (assign, nonatomic) MaplyParticleSystemType type;
        [Export("type", ArgumentSemantic.Assign)]
        MaplyParticleSystemType Type { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable shader;
        [NullAllowed, Export("shader", ArgumentSemantic.Strong)]
        string Shader { get; set; }

        // @property (assign, nonatomic) NSTimeInterval lifetime;
        [Export("lifetime")]
        double Lifetime { get; set; }

        // @property (readonly, nonatomic) NSTimeInterval baseTime;
        [Export("baseTime")]
        double BaseTime { get; }

        // @property (assign, nonatomic) int totalParticles;
        [Export("totalParticles")]
        int TotalParticles { get; set; }

        // @property (assign, nonatomic) int batchSize;
        [Export("batchSize")]
        int BatchSize { get; set; }

        // @property (assign, nonatomic) _Bool continuousUpdate;
        [Export("continuousUpdate")]
        bool ContinuousUpdate { get; set; }

        // -(instancetype _Nonnull)initWithName:(NSString * _Nonnull)name;
        [Export("initWithName:")]
        IntPtr Constructor(string name);

        // -(void)addAttribute:(NSString * _Nonnull)attrName type:(MaplyShaderAttrType)type;
        [Export("addAttribute:type:")]
        void AddAttribute(string attrName, MaplyShaderAttrType type);

        // -(void)addTexture:(id _Nonnull)image;
        [Export("addTexture:")]
        void AddTexture(NSObject image);
    }

    // @interface MaplyParticleBatch : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyParticleBatch
    {
        // @property (nonatomic, weak) MaplyParticleSystem * _Nullable partSys;
        [NullAllowed, Export("partSys", ArgumentSemantic.Weak)]
        MaplyParticleSystem PartSys { get; set; }

        // @property (assign, nonatomic) NSTimeInterval time;
        [Export("time")]
        double Time { get; set; }

        // -(instancetype _Nonnull)initWithParticleSystem:(MaplyParticleSystem * _Nonnull)partSys;
        [Export("initWithParticleSystem:")]
        IntPtr Constructor(MaplyParticleSystem partSys);

        // -(_Bool)addAttribute:(NSString * _Nonnull)attrName values:(NSData * _Nonnull)data;
        [Export("addAttribute:values:")]
        bool AddAttribute(string attrName, NSData data);

        // -(_Bool)isValid;
        [Export("isValid")]

        bool IsValid { get; }
    }

    // @interface MaplyPoints : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyPoints
    {
        // -(id _Nonnull)initWithNumPoints:(int)numPoints;
        [Export("initWithNumPoints:")]
        IntPtr Constructor(int numPoints);

        // @property (nonatomic) MaplyMatrix * _Nullable transform;
        [NullAllowed, Export("transform", ArgumentSemantic.Assign)]
        MaplyMatrix Transform { get; set; }

        // -(void)addGeoCoordLon:(float)x lat:(float)y z:(float)z;
        [Export("addGeoCoordLon:lat:z:")]
        void AddGeoCoordLon(float x, float y, float z);

        // -(void)addDispCoordX:(float)x y:(float)y z:(float)z;
        [Export("addDispCoordX:y:z:")]
        void AddDispCoordX(float x, float y, float z);

        // -(void)addDispCoordDoubleX:(double)x y:(double)y z:(double)z;
        [Export("addDispCoordDoubleX:y:z:")]
        void AddDispCoordDoubleX(double x, double y, double z);

        // -(void)addColorR:(float)r g:(float)g b:(float)b a:(float)a;
        [Export("addColorR:g:b:a:")]
        void AddColorR(float r, float g, float b, float a);

        // -(int)addAttributeType:(NSString * _Nonnull)attrName type:(MaplyShaderAttrType)type;
        [Export("addAttributeType:type:")]
        int AddAttributeType(string attrName, MaplyShaderAttrType type);

        // -(void)addAttribute:(int)whichAttr iVal:(int)val;
        [Export("addAttribute:iVal:")]
        void AddAttribute(int whichAttr, int val);

        // -(void)addAttribute:(int)whichAttr fVal:(float)val;
        [Export("addAttribute:fVal:")]
        void AddAttribute(int whichAttr, float val);

        // -(void)addAttribute:(int)whichAttr fValX:(float)valX fValY:(float)valY;
        [Export("addAttribute:fValX:fValY:")]
        void AddAttribute(int whichAttr, float valX, float valY);

        // -(void)addAttribute:(int)whichAttr fValX:(float)fValX fValY:(float)valY fValZ:(float)valZ;
        [Export("addAttribute:fValX:fValY:fValZ:")]
        void AddAttribute(int whichAttr, float fValX, float valY, float valZ);

        // -(void)addAttribute:(int)whichAttr valX:(double)valX valY:(double)valY valZ:(double)valZ;
        [Export("addAttribute:valX:valY:valZ:")]
        void AddAttribute(int whichAttr, double valX, double valY, double valZ);

        // -(void)addAttribute:(int)whichAttr fValX:(float)valX fValY:(float)valY fValZ:(float)valZ fValW:(float)valW;
        [Export("addAttribute:fValX:fValY:fValZ:fValW:")]
        void AddAttribute(int whichAttr, float valX, float valY, float valZ, float valW);
    }

    // @interface MaplyClusterInfo : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyClusterInfo
    {
        // @property (assign, nonatomic) int numObjects;
        [Export("numObjects")]
        int NumObjects { get; set; }
    }

    // @interface MaplyClusterGroup : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyClusterGroup
    {
        // @property (nonatomic) id _Nonnull image;
        [Export("image", ArgumentSemantic.Assign)]
        NSObject Image { get; set; }

        // @property (assign, nonatomic) CGSize size;
        [Export("size", ArgumentSemantic.Assign)]
        CGSize Size { get; set; }
    }

    // @protocol MaplyClusterGenerator <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface MaplyClusterGenerator
    {
        // @required -(void)startClusterGroup;
        [Abstract]
        [Export("startClusterGroup")]
        void StartClusterGroup();

        // @required -(MaplyClusterGroup * _Nonnull)makeClusterGroup:(MaplyClusterInfo * _Nonnull)clusterInfo;
        [Abstract]
        [Export("makeClusterGroup:")]
        MaplyClusterGroup MakeClusterGroup(MaplyClusterInfo clusterInfo);

        // @required -(void)endClusterGroup;
        [Abstract]
        [Export("endClusterGroup")]
        void EndClusterGroup();

        // @required -(int)clusterNumber;
        [Abstract]
        [Export("clusterNumber")]

        int ClusterNumber { get; }

        // @required -(CGSize)clusterLayoutSize;
        [Abstract]
        [Export("clusterLayoutSize")]

        CGSize ClusterLayoutSize { get; }

        // @required -(_Bool)selectable;
        [Abstract]
        [Export("selectable")]

        bool Selectable { get; }

        // @required -(double)markerAnimationTime;
        [Abstract]
        [Export("markerAnimationTime")]

        double MarkerAnimationTime { get; }

        // @required -(MaplyShader * _Nullable)motionShader;
        [Abstract]
        [NullAllowed, Export("motionShader")]

        MaplyShader MotionShader { get; }
    }

    // @interface MaplyBasicClusterGenerator : NSObject <MaplyClusterGenerator>
    [BaseType(typeof(NSObject))]
    interface MaplyBasicClusterGenerator : MaplyClusterGenerator
    {
        // -(instancetype _Nonnull)initWithColors:(NSArray * _Nonnull)colors clusterNumber:(int)clusterNumber size:(CGSize)markerSize viewC:(MaplyBaseViewController * _Nonnull)viewC;
        [Export("initWithColors:clusterNumber:size:viewC:")]

        IntPtr Constructor(NSObject[] colors, int clusterNumber, CGSize markerSize, MaplyBaseViewController viewC);

        // @property (assign, nonatomic) int clusterNumber;
        [Export("clusterNumber")]
        int ClusterNumber { get; set; }

        // @property (nonatomic) CGSize clusterLayoutSize;
        [Export("clusterLayoutSize", ArgumentSemantic.Assign)]
        CGSize ClusterLayoutSize { get; set; }

        // @property (nonatomic) _Bool selectable;
        [Export("selectable")]
        bool Selectable { get; set; }

        // @property (nonatomic) double markerAnimationTime;
        [Export("markerAnimationTime")]
        double MarkerAnimationTime { get; set; }

        // @property (nonatomic) MaplyShader * _Nullable motionShader;
        [NullAllowed, Export("motionShader", ArgumentSemantic.Assign)]
        MaplyShader MotionShader { get; set; }
    }

    // @protocol Maply3dTouchPreviewDatasource <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface Maply3dTouchPreviewDatasource
    {
        // @required -(UIViewController * _Nullable)maplyViewController:(MaplyBaseViewController * _Nonnull)viewC previewViewControllerForSelection:(NSObject * _Nonnull)selectedObj;
        [Abstract]
        [Export("maplyViewController:previewViewControllerForSelection:")]
        [return: NullAllowed]
        UIViewController MaplyViewController(MaplyBaseViewController viewC, NSObject selectedObj);

        // @required -(void)maplyViewController:(MaplyBaseViewController * _Nonnull)viewC showPreviewViewController:(UIViewController * _Nonnull)previewViewC;
        [Abstract]
        [Export("maplyViewController:showPreviewViewController:")]
        void MaplyViewController(MaplyBaseViewController viewC, UIViewController previewViewC);
    }

    // @interface MaplyCoordinate3dWrapper : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyCoordinate3dWrapper
    {
        // -(instancetype)initWithCoord:(MaplyCoordinate3d)coord;
        [Export("initWithCoord:")]
        IntPtr Constructor(MaplyCoordinate3d coord);

        // @property (readonly, nonatomic) MaplyCoordinate3d coord;
        [Export("coord")]
        MaplyCoordinate3d Coord { get; }
    }

    // @interface MaplyCoordinate3dDWrapper : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyCoordinate3dDWrapper
    {
        // -(instancetype)initWithCoord:(MaplyCoordinate3dD)coord;
        [Export("initWithCoord:")]
        IntPtr Constructor(MaplyCoordinate3dD coord);

        // @property (readonly, nonatomic) MaplyCoordinate3dD coord;
        [Export("coord")]
        MaplyCoordinate3dD Coord { get; }
    }

    // @interface MaplyCoordinateSystem : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyCoordinateSystem
    {
        // -(void)setBounds:(MaplyBoundingBox)bounds;
        [Export("setBounds:")]
        void SetBounds(MaplyBoundingBox bounds);

        // -(void)setBoundsLL:(MaplyCoordinate * _Nonnull)ll ur:(MaplyCoordinate * _Nonnull)ll;
        [Export("setBoundsLL:ur:")]
        unsafe void SetBoundsLL(NSNumber[] ll, NSNumber[] ur);

        // -(MaplyBoundingBox)getBounds;
        [Export("getBounds")]

        MaplyBoundingBox Bounds { get; }

        // -(void)getBoundsLL:(MaplyCoordinate * _Nonnull)ret_ll ur:(MaplyCoordinate * _Nonnull)ret_ur;
        [Export("getBoundsLL:ur:")]
        unsafe void GetBoundsLL(NSNumber[] ret_ll, NSNumber[] ret_ur);

        // -(MaplyCoordinate)geoToLocal:(MaplyCoordinate)coord;
        [Export("geoToLocal:")]
        MaplyCoordinate GeoToLocal(MaplyCoordinate coord);

        // -(MaplyCoordinate)localToGeo:(MaplyCoordinate)coord;
        [Export("localToGeo:")]
        MaplyCoordinate LocalToGeo(MaplyCoordinate coord);

        // -(MaplyCoordinate3dD)localToGeocentric:(MaplyCoordinate3dD)coord;
        [Export("localToGeocentric:")]
        MaplyCoordinate3dD LocalToGeocentric(MaplyCoordinate3dD coord);

        // -(MaplyCoordinate3dD)geocentricToLocal:(MaplyCoordinate3dD)coord;
        [Export("geocentricToLocal:")]
        MaplyCoordinate3dD GeocentricToLocal(MaplyCoordinate3dD coord);

        // -(NSString * _Nonnull)getSRS;
        [Export("getSRS")]

        string SRS { get; }

        // -(_Bool)canBeDegrees;
        [Export("canBeDegrees")]

        bool CanBeDegrees { get; }
    }

    // @interface MaplyPlateCarree : MaplyCoordinateSystem
    [BaseType(typeof(MaplyCoordinateSystem))]
    interface MaplyPlateCarree
    {
        // -(instancetype _Nullable)initWithBoundingBox:(MaplyBoundingBox)bbox;
        [Export("initWithBoundingBox:")]
        IntPtr Constructor(MaplyBoundingBox bbox);
    }

    // @interface MaplySphericalMercator : MaplyCoordinateSystem
    [BaseType(typeof(MaplyCoordinateSystem))]
    interface MaplySphericalMercator
    {
    }

    // @interface MaplyProj4CoordSystem : MaplyCoordinateSystem
    [BaseType(typeof(MaplyCoordinateSystem))]
    interface MaplyProj4CoordSystem
    {
        // -(instancetype _Nonnull)initWithString:(NSString * _Nonnull)proj4Str;
        [Export("initWithString:")]
        IntPtr Constructor(string proj4Str);

        // -(_Bool)valid;
        [Export("valid")]

        bool Valid { get; }
    }

    // @interface MaplyMatrix : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyMatrix
    {
        // -(instancetype _Nonnull)initWithYaw:(double)yaw pitch:(double)pitch roll:(double)roll;
        [Export("initWithYaw:pitch:roll:")]
        IntPtr Constructor(double yaw, double pitch, double roll);

        // -(instancetype _Nonnull)initWithScale:(double)scale;
        [Export("initWithScale:")]
        IntPtr Constructor(double scale);

        //// -(instancetype _Nonnull)initWithTranslateX:(double)x y:(double)y z:(double)z;
        //[Export ("initWithTranslateX:y:z:")] TODO
        //IntPtr Constructor (double x, double y, double z);

        // -(instancetype _Nonnull)initWithAngle:(double)ang axisX:(double)x axisY:(double)y axisZ:(double)z;
        [Export("initWithAngle:axisX:axisY:axisZ:")]
        IntPtr Constructor(double ang, double x, double y, double z);

        // -(instancetype _Nonnull)multiplyWith:(MaplyMatrix * _Nonnull)other;
        [Export("multiplyWith:")]
        MaplyMatrix MultiplyWith(MaplyMatrix other);
    }
    // @protocol MaplyLocationTrackerDelegate
    [Protocol, Model]
    interface MaplyLocationTrackerDelegate
    {
        // @required -(void)locationManager:(CLLocationManager * _Nonnull)manager didFailWithError:(NSError * _Nonnull)error;
        [Abstract]
        [Export("locationManager:didFailWithError:")]
        void DidFailWithError(CLLocationManager manager, NSError error);

        // @required -(void)locationManager:(CLLocationManager * _Nonnull)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;
        [Abstract]
        [Export("locationManager:didChangeAuthorizationStatus:")]
        void DidChangeAuthorizationStatus(CLLocationManager manager, CLAuthorizationStatus status);

        // @optional -(MaplyLocationTrackerSimulationPoint)getSimulationPoint;
        [Export("getSimulationPoint")]

        MaplyLocationTrackerSimulationPoint SimulationPoint { get; }
    }

    partial interface IMaplyLocationTrackerDelegate { }
    // @interface MaplyLocationTracker : NSObject <CLLocationManagerDelegate>
    [BaseType(typeof(NSObject))]
    interface MaplyLocationTracker : ICLLocationManagerDelegate
    {
        // -(instancetype _Nonnull)initWithViewC:(MaplyBaseViewController * _Nullable)viewC delegate:(NSObject<MaplyLocationTrackerDelegate> * _Nullable)delegate useHeading:(_Bool)useHeading useCourse:(_Bool)useCourse simulate:(_Bool)simulate;
        [Export("initWithViewC:delegate:useHeading:useCourse:simulate:")]
        IntPtr Constructor([NullAllowed] MaplyBaseViewController viewC, [NullAllowed] IMaplyLocationTrackerDelegate @delegate, bool useHeading, bool useCourse, bool simulate);

        // -(void)changeLockType:(MaplyLocationLockType)lockType forwardTrackOffset:(int)forwardTrackOffset;
        [Export("changeLockType:forwardTrackOffset:")]
        void ChangeLockType(MaplyLocationLockType lockType, int forwardTrackOffset);

        // -(void)teardown;
        [Export("teardown")]
        void Teardown();
    }

    // @interface MaplySelectedObject : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplySelectedObject
    {
        // @property (nonatomic, weak) id _Nullable selectedObj;
        [NullAllowed, Export("selectedObj", ArgumentSemantic.Weak)]
        NSObject SelectedObj { get; set; }

        // @property double screenDist;
        [Export("screenDist")]
        double ScreenDist { get; set; }

        // @property double zDist;
        [Export("zDist")]
        double ZDist { get; set; }

        // @property _Bool cluster;
        [Export("cluster")]
        bool Cluster { get; set; }
    }

    // @interface MaplyBaseViewController : UIViewController
    [BaseType(typeof(UIViewController))]
    interface MaplyBaseViewController
    {
        // @property (assign, nonatomic) _Bool selection;
        [Export("selection")]
        bool Selection { get; set; }

        // @property (nonatomic, strong) UIColor * _Nullable clearColor;
        [NullAllowed, Export("clearColor", ArgumentSemantic.Strong)]
        UIColor ClearColor { get; set; }

        // @property (assign, nonatomic) int frameInterval;
        [Export("frameInterval")]
        int FrameInterval { get; set; }

        [Wrap("WeakElevDelegate")]
        [NullAllowed]
        MaplyElevationSourceDelegate ElevDelegate { get; set; }

        // @property (nonatomic, weak) NSObject<MaplyElevationSourceDelegate> * _Nullable elevDelegate;
        [NullAllowed, Export("elevDelegate", ArgumentSemantic.Weak)]
        NSObject WeakElevDelegate { get; set; }

        // @property (assign, nonatomic) _Bool threadPerLayer;
        [Export("threadPerLayer")]
        bool ThreadPerLayer { get; set; }

        // @property (assign, nonatomic) int screenObjectDrawPriorityOffset;
        [Export("screenObjectDrawPriorityOffset")]
        int ScreenObjectDrawPriorityOffset { get; set; }

        // -(void)clearLights;
        [Export("clearLights")]
        void ClearLights();

        // -(void)resetLights;
        [Export("resetLights")]
        void ResetLights();

        // -(void)addLight:(MaplyLight * _Nonnull)light;
        [Export("addLight:")]
        void AddLight(MaplyLight light);

        // -(void)removeLight:(MaplyLight * _Nonnull)light;
        [Export("removeLight:")]
        void RemoveLight(MaplyLight light);

        // -(void)setHints:(NSDictionary * _Nonnull)hintsDict;
        [Export("setHints:")]
        void SetHints(NSDictionary hintsDict);

        // -(MaplyComponentObject * _Nullable)addScreenMarkers:(NSArray * _Nonnull)markers desc:(NSDictionary * _Nullable)desc;
        [Export("addScreenMarkers:desc:")]

        [return: NullAllowed]
        MaplyComponentObject AddScreenMarkers(NSObject[] markers, [NullAllowed] NSDictionary desc);

        // -(MaplyComponentObject * _Nullable)addScreenMarkers:(NSArray * _Nonnull)markers desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addScreenMarkers:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddScreenMarkers(NSObject[] markers, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(void)addClusterGenerator:(NSObject<MaplyClusterGenerator> * _Nonnull)clusterGen;
        [Export("addClusterGenerator:")]
        void AddClusterGenerator(MaplyClusterGenerator clusterGen);

        // -(MaplyComponentObject * _Nullable)addMarkers:(NSArray * _Nonnull)markers desc:(NSDictionary * _Nullable)desc;
        [Export("addMarkers:desc:")]

        [return: NullAllowed]
        MaplyComponentObject AddMarkers(NSObject[] markers, [NullAllowed] NSDictionary desc);

        // -(MaplyComponentObject * _Nullable)addMarkers:(NSArray * _Nonnull)markers desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addMarkers:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddMarkers(NSObject[] markers, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addScreenLabels:(NSArray * _Nonnull)labels desc:(NSDictionary * _Nullable)desc;
        [Export("addScreenLabels:desc:")]

        [return: NullAllowed]
        MaplyComponentObject AddScreenLabels(NSObject[] labels, [NullAllowed] NSDictionary desc);

        // -(MaplyComponentObject * _Nullable)addScreenLabels:(NSArray * _Nonnull)labels desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addScreenLabels:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddScreenLabels(NSObject[] labels, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addLabels:(NSArray * _Nonnull)labels desc:(NSDictionary * _Nullable)desc;
        [Export("addLabels:desc:")]

        [return: NullAllowed]
        MaplyComponentObject AddLabels(NSObject[] labels, [NullAllowed] NSDictionary desc);

        // -(MaplyComponentObject * _Nullable)addLabels:(NSArray * _Nonnull)labels desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addLabels:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddLabels(NSObject[] labels, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addVectors:(NSArray * _Nonnull)vectors desc:(NSDictionary * _Nullable)desc;
        [Export("addVectors:desc:")]

        [return: NullAllowed]
        MaplyComponentObject AddVectors(NSObject[] vectors, [NullAllowed] NSDictionary desc);

        // -(MaplyComponentObject * _Nullable)addVectors:(NSArray * _Nonnull)vectors desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addVectors:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddVectors(NSObject[] vectors, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)instanceVectors:(MaplyComponentObject * _Nonnull)baseObj desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("instanceVectors:desc:mode:")]
        [return: NullAllowed]
        MaplyComponentObject InstanceVectors(MaplyComponentObject baseObj, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addWideVectors:(NSArray * _Nonnull)vectors desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addWideVectors:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddWideVectors(NSObject[] vectors, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addWideVectors:(NSArray * _Nonnull)vectors desc:(NSDictionary * _Nullable)desc;
        [Export("addWideVectors:desc:")]

        [return: NullAllowed]
        MaplyComponentObject AddWideVectors(NSObject[] vectors, [NullAllowed] NSDictionary desc);

        // -(MaplyComponentObject * _Nullable)addShapes:(NSArray * _Nonnull)shapes desc:(NSDictionary * _Nullable)desc;
        [Export("addShapes:desc:")]

        [return: NullAllowed]
        MaplyComponentObject AddShapes(NSObject[] shapes, [NullAllowed] NSDictionary desc);

        // -(MaplyComponentObject * _Nullable)addModelInstances:(NSArray * _Nonnull)modelInstances desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addModelInstances:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddModelInstances(NSObject[] modelInstances, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addGeometry:(NSArray * _Nonnull)geom desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addGeometry:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddGeometry(NSObject[] geom, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addShapes:(NSArray * _Nonnull)shapes desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addShapes:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddShapes(NSObject[] shapes, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addStickers:(NSArray * _Nonnull)stickers desc:(NSDictionary * _Nullable)desc;
        [Export("addStickers:desc:")]

        [return: NullAllowed]
        MaplyComponentObject AddStickers(NSObject[] stickers, [NullAllowed] NSDictionary desc);

        // -(MaplyComponentObject * _Nullable)addStickers:(NSArray * _Nonnull)stickers desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addStickers:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddStickers(NSObject[] stickers, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(void)changeSticker:(MaplyComponentObject * _Nonnull)compObj desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("changeSticker:desc:mode:")]
        void ChangeSticker(MaplyComponentObject compObj, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addBillboards:(NSArray * _Nonnull)billboards desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addBillboards:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddBillboards(NSObject[] billboards, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addParticleSystem:(MaplyParticleSystem * _Nonnull)partSys desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addParticleSystem:desc:mode:")]
        [return: NullAllowed]
        MaplyComponentObject AddParticleSystem(MaplyParticleSystem partSys, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(void)addParticleBatch:(MaplyParticleBatch * _Nonnull)batch mode:(MaplyThreadMode)threadMode;
        [Export("addParticleBatch:mode:")]
        void AddParticleBatch(MaplyParticleBatch batch, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addSelectionVectors:(NSArray * _Nonnull)vectors;
        [Export("addSelectionVectors:")]

        [return: NullAllowed]
        MaplyComponentObject AddSelectionVectors(NSObject[] vectors);

        // -(void)changeVector:(MaplyComponentObject * _Nonnull)compObj desc:(NSDictionary * _Nullable)desc;
        [Export("changeVector:desc:")]
        void ChangeVector(MaplyComponentObject compObj, [NullAllowed] NSDictionary desc);

        // -(void)changeVector:(MaplyComponentObject * _Nonnull)compObj desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("changeVector:desc:mode:")]
        void ChangeVector(MaplyComponentObject compObj, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addLoftedPolys:(NSArray * _Nonnull)polys key:(NSString * _Nullable)key cache:(MaplyVectorDatabase * _Nullable)cacheDb desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addLoftedPolys:key:cache:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddLoftedPolys(NSObject[] polys, [NullAllowed] string key, [NullAllowed] MaplyVectorDatabase cacheDb, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyComponentObject * _Nullable)addPoints:(NSArray * _Nonnull)points desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addPoints:desc:mode:")]

        [return: NullAllowed]
        MaplyComponentObject AddPoints(NSObject[] points, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(void)addViewTracker:(MaplyViewTracker * _Nonnull)viewTrack;
        [Export("addViewTracker:")]
        void AddViewTracker(MaplyViewTracker viewTrack);

        // -(void)moveViewTracker:(MaplyViewTracker * _Nonnull)viewTrack moveTo:(MaplyCoordinate)newPos;
        [Export("moveViewTracker:moveTo:")]
        void MoveViewTracker(MaplyViewTracker viewTrack, MaplyCoordinate newPos);

        // -(void)addAnnotation:(MaplyAnnotation * _Nonnull)annotate forPoint:(MaplyCoordinate)coord offset:(CGPoint)offset;
        [Export("addAnnotation:forPoint:offset:")]
        void AddAnnotation(MaplyAnnotation annotate, MaplyCoordinate coord, CGPoint offset);

        // -(void)removeAnnotation:(MaplyAnnotation * _Nonnull)annotate;
        [Export("removeAnnotation:")]
        void RemoveAnnotation(MaplyAnnotation annotate);

        // -(void)freezeAnnotation:(MaplyAnnotation * _Nonnull)annotate;
        [Export("freezeAnnotation:")]
        void FreezeAnnotation(MaplyAnnotation annotate);

        // -(void)unfreezeAnnotation:(MaplyAnnotation * _Nonnull)annotate;
        [Export("unfreezeAnnotation:")]
        void UnfreezeAnnotation(MaplyAnnotation annotate);

        // -(void)clearAnnotations;
        [Export("clearAnnotations")]
        void ClearAnnotations();

        // -(NSArray * _Nullable)annotations;
        [NullAllowed, Export("annotations")]

        NSObject[] Annotations { get; }

        // -(void)removeViewTrackForView:(UIView * _Nonnull)view;
        [Export("removeViewTrackForView:")]
        void RemoveViewTrackForView(UIView view);

        // -(CGPoint)screenPointFromGeo:(MaplyCoordinate)geoCoord;
        [Export("screenPointFromGeo:")]
        CGPoint ScreenPointFromGeo(MaplyCoordinate geoCoord);

        // -(_Bool)animateToPosition:(MaplyCoordinate)newPos onScreen:(CGPoint)loc time:(NSTimeInterval)howLong;
        [Export("animateToPosition:onScreen:time:")]
        bool AnimateToPosition(MaplyCoordinate newPos, CGPoint loc, double howLong);

        // -(MaplyTexture * _Nullable)addTexture:(UIImage * _Nonnull)image imageFormat:(MaplyQuadImageFormat)imageFormat wrapFlags:(int)wrapFlags mode:(MaplyThreadMode)threadMode;
        [Export("addTexture:imageFormat:wrapFlags:mode:")]
        [return: NullAllowed]
        MaplyTexture AddTexture(UIImage image, MaplyQuadImageFormat imageFormat, int wrapFlags, MaplyThreadMode threadMode);

        // -(MaplyTexture * _Nullable)addTexture:(UIImage * _Nonnull)image desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)threadMode;
        [Export("addTexture:desc:mode:")]
        [return: NullAllowed]
        MaplyTexture AddTexture(UIImage image, [NullAllowed] NSDictionary desc, MaplyThreadMode threadMode);

        // -(MaplyTexture * _Nullable)addTextureToAtlas:(UIImage * _Nonnull)image mode:(MaplyThreadMode)threadMode;
        [Export("addTextureToAtlas:mode:")]
        [return: NullAllowed]
        MaplyTexture AddTextureToAtlas(UIImage image, MaplyThreadMode threadMode);

        // -(MaplyTexture * _Nullable)addTextureToAtlas:(UIImage * _Nonnull)image imageFormat:(MaplyQuadImageFormat)imageFormat wrapFlags:(int)wrapFlags mode:(MaplyThreadMode)threadMode;
        [Export("addTextureToAtlas:imageFormat:wrapFlags:mode:")]
        [return: NullAllowed]
        MaplyTexture AddTextureToAtlas(UIImage image, MaplyQuadImageFormat imageFormat, int wrapFlags, MaplyThreadMode threadMode);

        // -(void)removeTexture:(MaplyTexture * _Nonnull)image mode:(MaplyThreadMode)threadMode;
        [Export("removeTexture:mode:")]
        void RemoveTexture(MaplyTexture image, MaplyThreadMode threadMode);

        // -(void)removeTextures:(NSArray * _Nonnull)texture mode:(MaplyThreadMode)threadMode;
        [Export("removeTextures:mode:")]

        void RemoveTextures(NSObject[] texture, MaplyThreadMode threadMode);

        // -(void)setMaxLayoutObjects:(int)maxLayoutObjects;
        [Export("setMaxLayoutObjects:")]
        void SetMaxLayoutObjects(int maxLayoutObjects);

        // -(void)removeObject:(MaplyComponentObject * _Nonnull)theObj;
        [Export("removeObject:")]
        void RemoveObject(MaplyComponentObject theObj);

        // -(void)removeObjects:(NSArray * _Nonnull)theObjs;
        [Export("removeObjects:")]

        void RemoveObjects(NSObject[] theObjs);

        // -(void)removeObjects:(NSArray * _Nonnull)theObjs mode:(MaplyThreadMode)threadMode;
        [Export("removeObjects:mode:")]

        void RemoveObjects(NSObject[] theObjs, MaplyThreadMode threadMode);

        // -(void)disableObjects:(NSArray * _Nonnull)theObjs mode:(MaplyThreadMode)threadMode;
        [Export("disableObjects:mode:")]

        void DisableObjects(NSObject[] theObjs, MaplyThreadMode threadMode);

        // -(void)enableObjects:(NSArray * _Nonnull)theObjs mode:(MaplyThreadMode)threadMode;
        [Export("enableObjects:mode:")]

        void EnableObjects(NSObject[] theObjs, MaplyThreadMode threadMode);

        // -(void)startChanges;
        [Export("startChanges")]
        void StartChanges();

        // -(void)endChanges;
        [Export("endChanges")]
        void EndChanges();

        // -(void)addActiveObject:(MaplyActiveObject * _Nonnull)theObj;
        [Export("addActiveObject:")]
        void AddActiveObject(MaplyActiveObject theObj);

        // -(void)removeActiveObject:(MaplyActiveObject * _Nonnull)theObj;
        [Export("removeActiveObject:")]
        void RemoveActiveObject(MaplyActiveObject theObj);

        // -(void)removeActiveObjects:(NSArray * _Nonnull)theObjs;
        [Export("removeActiveObjects:")]

        void RemoveActiveObjects(NSObject[] theObjs);

        // -(_Bool)addLayer:(MaplyViewControllerLayer * _Nonnull)layer;
        [Export("addLayer:")]
        bool AddLayer(MaplyViewControllerLayer layer);

        // -(void)removeLayer:(MaplyViewControllerLayer * _Nonnull)layer;
        [Export("removeLayer:")]
        void RemoveLayer(MaplyViewControllerLayer layer);

        // -(void)removeLayers:(NSArray * _Nonnull)layers;
        [Export("removeLayers:")]

        void RemoveLayers(NSObject[] layers);

        // -(void)removeAllLayers;
        [Export("removeAllLayers")]
        void RemoveAllLayers();

        // -(MaplyCoordinate3d)displayPointFromGeo:(MaplyCoordinate)geoCoord;
        [Export("displayPointFromGeo:")]
        MaplyCoordinate3d DisplayPointFromGeo(MaplyCoordinate geoCoord);

        // -(void)startAnimation;
        [Export("startAnimation")]
        void StartAnimation();

        // -(void)stopAnimation;
        [Export("stopAnimation")]
        void StopAnimation();

        // -(void)teardown;
        [Export("teardown")]
        void Teardown();

        // -(void)addShaderProgram:(MaplyShader * _Nonnull)shader sceneName:(NSString * _Nonnull)sceneName;
        [Export("addShaderProgram:sceneName:")]
        void AddShaderProgram(MaplyShader shader, string sceneName);

        // -(MaplyShader * _Nullable)getShaderByName:(NSString * _Nonnull)name;
        [Export("getShaderByName:")]
        [return: NullAllowed]
        MaplyShader GetShaderByName(string name);

        // -(float)currentMapScale;
        [Export("currentMapScale")]

        float CurrentMapScale { get; }

        // -(float)heightForMapScale:(float)scale;
        [Export("heightForMapScale:")]
        float HeightForMapScale(float scale);

        // -(UIImage * _Nullable)snapshot;
        [NullAllowed, Export("snapshot")]

        UIImage Snapshot { get; }

        // -(float)currentMapZoom:(MaplyCoordinate)coordinate;
        [Export("currentMapZoom:")]
        float CurrentMapZoom(MaplyCoordinate coordinate);

        // -(MaplyCoordinateSystem * _Nullable)coordSystem;
        [NullAllowed, Export("coordSystem")]

        MaplyCoordinateSystem CoordSystem { get; }

        // -(MaplyCoordinate3d)displayCoordFromLocal:(MaplyCoordinate3d)localCoord;
        [Export("displayCoordFromLocal:")]
        MaplyCoordinate3d DisplayCoordFromLocal(MaplyCoordinate3d localCoord);

        // -(MaplyCoordinate3d)displayCoord:(MaplyCoordinate3d)localCoord fromSystem:(MaplyCoordinateSystem * _Nonnull)coordSys;
        [Export("displayCoord:fromSystem:")]
        MaplyCoordinate3d DisplayCoord(MaplyCoordinate3d localCoord, MaplyCoordinateSystem coordSys);

        // -(MaplyCoordinate3dD)displayCoordD:(MaplyCoordinate3dD)localCoord fromSystem:(MaplyCoordinateSystem * _Nonnull)coordSys;
        [Export("displayCoordD:fromSystem:")]
        MaplyCoordinate3dD DisplayCoordD(MaplyCoordinate3dD localCoord, MaplyCoordinateSystem coordSys);

        // -(BOOL)enable3dTouchSelection:(NSObject<Maply3dTouchPreviewDatasource> * _Nonnull)previewDataSource;
        [Export("enable3dTouchSelection:")]
        bool Enable3dTouchSelection(IMaply3dTouchPreviewDatasource previewDataSource);

        // -(void)disable3dTouchSelection;
        [Export("disable3dTouchSelection")]
        void Disable3dTouchSelection();

        // @property (assign, nonatomic) _Bool performanceOutput;
        [Export("performanceOutput")]
        bool PerformanceOutput { get; set; }

        // -(void)requirePanGestureRecognizerToFailForGesture:(UIGestureRecognizer * _Nullable)other;
        [Export("requirePanGestureRecognizerToFailForGesture:")]
        void RequirePanGestureRecognizerToFailForGesture([NullAllowed] UIGestureRecognizer other);

        // -(void)startLocationTrackingWithDelegate:(NSObject<MaplyLocationTrackerDelegate> * _Nullable)delegate useHeading:(_Bool)useHeading useCourse:(_Bool)useCourse simulate:(_Bool)simulate;
        [Export("startLocationTrackingWithDelegate:useHeading:useCourse:simulate:")]
        void StartLocationTrackingWithDelegate([NullAllowed] IMaplyLocationTrackerDelegate @delegate, bool useHeading, bool useCourse, bool simulate);

        // -(void)changeLocationTrackingLockType:(MaplyLocationLockType)lockType;
        [Export("changeLocationTrackingLockType:")]
        void ChangeLocationTrackingLockType(MaplyLocationLockType lockType);

        // -(void)changeLocationTrackingLockType:(MaplyLocationLockType)lockType forwardTrackOffset:(int)forwardTrackOffset;
        [Export("changeLocationTrackingLockType:forwardTrackOffset:")]
        void ChangeLocationTrackingLockType(MaplyLocationLockType lockType, int forwardTrackOffset);

        // -(void)stopLocationTracking;
        [Export("stopLocationTracking")]
        void StopLocationTracking();
    }

    // @interface MaplyScreenObject : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyScreenObject
    {
        // -(void)addString:(NSString *)str font:(UIFont *)font color:(UIColor *)color;
        [Export("addString:font:color:")]
        void AddString(string str, UIFont font, UIColor color);

        // -(void)addAttributedString:(NSAttributedString *)str;
        [Export("addAttributedString:")]
        void AddAttributedString(NSAttributedString str);

        // -(void)addImage:(id)image color:(UIColor *)color size:(CGSize)size;
        [Export("addImage:color:size:")]
        void AddImage(NSObject image, UIColor color, CGSize size);

        // -(MaplyBoundingBox)getSize;
        [Export("getSize")]

        MaplyBoundingBox Size { get; }

        // -(void)scaleX:(double)x y:(double)y;
        [Export("scaleX:y:")]
        void ScaleX(double x, double y);

        // -(void)translateX:(double)x y:(double)y;
        [Export("translateX:y:")]
        void TranslateX(double x, double y);

        // -(void)addScreenObject:(MaplyScreenObject *)screenObj;
        [Export("addScreenObject:")]
        void AddScreenObject(MaplyScreenObject screenObj);
    }

    // @interface MaplyBillboard : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyBillboard
    {
        // @property (nonatomic) MaplyCoordinate3d center;
        [Export("center", ArgumentSemantic.Assign)]
        MaplyCoordinate3d Center { get; set; }

        // @property (nonatomic) _Bool selectable;
        [Export("selectable")]
        bool Selectable { get; set; }

        // @property (nonatomic, strong) MaplyScreenObject * _Nullable screenObj;
        [NullAllowed, Export("screenObj", ArgumentSemantic.Strong)]
        MaplyScreenObject ScreenObj { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable vertexAttributes;
        [NullAllowed, Export("vertexAttributes", ArgumentSemantic.Strong)]

        NSObject[] VertexAttributes { get; set; }

        // @property (nonatomic, strong) id _Nullable userObject;
        [NullAllowed, Export("userObject", ArgumentSemantic.Strong)]
        NSObject UserObject { get; set; }

        // -(instancetype _Nullable)initWithImage:(id _Nonnull)texture color:(UIColor * _Nonnull)color size:(CGSize)size;
        [Export("initWithImage:color:size:")]
        IntPtr Constructor(NSObject texture, UIColor color, CGSize size);
    }

    // @interface MaplyViewControllerAnimationState : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyViewControllerAnimationState
    {
        // @property (nonatomic) double heading;
        [Export("heading")]
        double Heading { get; set; }

        // @property (nonatomic) double height;
        [Export("height")]
        double Height { get; set; }

        // @property (nonatomic) MaplyCoordinateD pos;
        [Export("pos", ArgumentSemantic.Assign)]
        MaplyCoordinateD Pos { get; set; }

        // @property (nonatomic) CGPoint screenPos;
        [Export("screenPos", ArgumentSemantic.Assign)]
        CGPoint ScreenPos { get; set; }

        // +(MaplyViewControllerAnimationState * _Nonnull)Interpolate:(double)t from:(MaplyViewControllerAnimationState * _Nonnull)stateA to:(MaplyViewControllerAnimationState * _Nonnull)stateB;
        [Static]
        [Export("Interpolate:from:to:")]
        MaplyViewControllerAnimationState Interpolate(double t, MaplyViewControllerAnimationState stateA, MaplyViewControllerAnimationState stateB);
    }

    // @protocol MaplyViewControllerAnimationDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface MaplyViewControllerAnimationDelegate
    {
        // @required -(void)mapViewController:(MaplyViewController * _Nonnull)viewC startState:(MaplyViewControllerAnimationState * _Nonnull)startState startTime:(NSTimeInterval)startTime endTime:(NSTimeInterval)endTime;
        [Abstract]
        [Export("mapViewController:startState:startTime:endTime:")]
        void MapViewController(MaplyViewController viewC, MaplyViewControllerAnimationState startState, double startTime, double endTime);

        // @required -(MaplyViewControllerAnimationState * _Nonnull)mapViewController:(MaplyViewController * _Nonnull)viewC stateForTime:(NSTimeInterval)currentTime;
        [Abstract]
        [Export("mapViewController:stateForTime:")]
        MaplyViewControllerAnimationState MapViewController(MaplyViewController viewC, double currentTime);

        // @optional -(void)mapViewControllerDidFinishAnimation:(MaplyViewController * _Nonnull)viewC;
        [Export("mapViewControllerDidFinishAnimation:")]
        void MapViewControllerDidFinishAnimation(MaplyViewController viewC);
    }

    // @interface MaplyViewControllerSimpleAnimationDelegate : NSObject <MaplyViewControllerAnimationDelegate>
    [BaseType(typeof(NSObject))]
    interface MaplyViewControllerSimpleAnimationDelegate : MaplyViewControllerAnimationDelegate
    {
        // -(instancetype _Nonnull)initWithState:(MaplyViewControllerAnimationState * _Nonnull)endState;
        [Export("initWithState:")]
        IntPtr Constructor(MaplyViewControllerAnimationState endState);

        // @property (nonatomic) MaplyCoordinateD loc;
        [Export("loc", ArgumentSemantic.Assign)]
        MaplyCoordinateD Loc { get; set; }

        // @property (nonatomic) double heading;
        [Export("heading")]
        double Heading { get; set; }

        // @property (nonatomic) double height;
        [Export("height")]
        double Height { get; set; }
    }

    // @protocol MaplyViewControllerDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface MaplyViewControllerDelegate
    {
        // @optional -(void)maplyViewController:(MaplyViewController * _Nonnull)viewC didSelect:(NSObject * _Nonnull)selectedObj;
        [Export("maplyViewController:didSelect:")]
        void DidSelect(MaplyViewController viewC, NSObject selectedObj);

        // @optional -(void)maplyViewController:(MaplyViewController * _Nonnull)viewC didSelect:(NSObject * _Nonnull)selectedObj atLoc:(MaplyCoordinate)coord onScreen:(CGPoint)screenPt;
        [Export("maplyViewController:didSelect:atLoc:onScreen:")]
        void DidSelect(MaplyViewController viewC, NSObject selectedObj, MaplyCoordinate coord, CGPoint screenPt);

        // @optional -(void)maplyViewController:(MaplyViewController * _Nonnull)viewC didTapAt:(MaplyCoordinate)coord;
        [Export("maplyViewController:didTapAt:")]
        void DidTapAt(MaplyViewController viewC, MaplyCoordinate coord);

        // @optional -(void)maplyViewControllerDidStartMoving:(MaplyViewController * _Nonnull)viewC userMotion:(_Bool)userMotion;
        [Export("maplyViewControllerDidStartMoving:userMotion:")]
        void DidStartMoving(MaplyViewController viewC, bool userMotion);

        // @optional -(void)maplyViewController:(MaplyViewController * _Nonnull)viewC didStopMoving:(MaplyCoordinate * _Nonnull)corners userMotion:(_Bool)userMotion;
        [Export("maplyViewController:didStopMoving:userMotion:")]
        unsafe void DidStopMoving(MaplyViewController viewC, NSNumber[] corners, bool userMotion);

        // @optional -(void)maplyViewController:(MaplyViewController * _Nonnull)viewC didTapAnnotation:(MaplyAnnotation * _Nonnull)annotation;
        [Export("maplyViewController:didTapAnnotation:")]
        void DidTapAnnotation(MaplyViewController viewC, MaplyAnnotation annotation);

        // @optional -(void)maplyViewController:(MaplyViewController * _Nonnull)viewC didClickAnnotation:(MaplyAnnotation * _Nonnull)annotation __attribute__((deprecated("")));
        [Export("maplyViewController:didClickAnnotation:")]
        void DidClickAnnotation(MaplyViewController viewC, MaplyAnnotation annotation);
    }

    partial interface IMaplyViewControllerDelegate { }

    // @interface MaplyViewController : MaplyBaseViewController
    [BaseType(typeof(MaplyBaseViewController))]
    interface MaplyViewController
    {
        // -(instancetype _Nonnull)initWithMapType:(MaplyMapType)mapType;
        [Export("initWithMapType:")]
        IntPtr Constructor(MaplyMapType mapType);

        // -(instancetype _Nonnull)initAsTetheredFlatMap:(UIScrollView * _Nonnull)scrollView tetherView:(UIView * _Nullable)tetherView;
        [Export("initAsTetheredFlatMap:tetherView:")]
        IntPtr Constructor(UIScrollView scrollView, [NullAllowed] UIView tetherView);

        // -(void)resetTetheredFlatMap:(UIScrollView * _Nonnull)scrollView tetherView:(UIView * _Nullable)tetherView;
        [Export("resetTetheredFlatMap:tetherView:")]
        void ResetTetheredFlatMap(UIScrollView scrollView, [NullAllowed] UIView tetherView);

        // @property (readonly, nonatomic) _Bool flatMode;
        [Export("flatMode")]
        bool FlatMode { get; }

        // @property (nonatomic, weak) UIView * _Nullable tetherView;
        [NullAllowed, Export("tetherView", ArgumentSemantic.Weak)]
        UIView TetherView { get; set; }

        // @property (assign, nonatomic) _Bool tetheredMode;
        [Export("tetheredMode")]
        bool TetheredMode { get; set; }

        // @property (nonatomic, strong) MaplyCoordinateSystem * _Nullable coordSys;
        [NullAllowed, Export("coordSys", ArgumentSemantic.Strong)]
        MaplyCoordinateSystem CoordSys { get; set; }

        // @property (nonatomic) MaplyCoordinate3d displayCenter;
        [Export("displayCenter", ArgumentSemantic.Assign)]
        MaplyCoordinate3d DisplayCenter { get; set; }

        // @property (assign, nonatomic) _Bool pinchGesture;
        [Export("pinchGesture")]
        bool PinchGesture { get; set; }

        // @property (assign, nonatomic) _Bool rotateGesture;
        [Export("rotateGesture")]
        bool RotateGesture { get; set; }

        // @property (assign, nonatomic) _Bool panGesture;
        [Export("panGesture")]
        bool PanGesture { get; set; }

        // @property (assign, nonatomic) _Bool doubleTapZoomGesture;
        [Export("doubleTapZoomGesture")]
        bool DoubleTapZoomGesture { get; set; }

        // @property (assign, nonatomic) _Bool twoFingerTapGesture;
        [Export("twoFingerTapGesture")]
        bool TwoFingerTapGesture { get; set; }

        // @property (assign, nonatomic) _Bool doubleTapDragGesture;
        [Export("doubleTapDragGesture")]
        bool DoubleTapDragGesture { get; set; }

        // @property (assign, nonatomic) _Bool inScrollView;
        [Export("inScrollView")]
        bool InScrollView { get; set; }

        // @property (assign, nonatomic) _Bool cancelAnimationOnTouch;
        [Export("cancelAnimationOnTouch")]
        bool CancelAnimationOnTouch { get; set; }

        // @property (assign, nonatomic) float heading;
        [Export("heading")]
        float Heading { get; set; }

        // @property (assign, nonatomic) _Bool autoMoveToTap;
        [Export("autoMoveToTap")]
        bool AutoMoveToTap { get; set; }

        [Wrap("WeakDelegate")]
        [NullAllowed]
        MaplyViewControllerDelegate Delegate { get; set; }

        // @property (nonatomic, weak) NSObject<MaplyViewControllerDelegate> * _Nullable delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
        NSObject WeakDelegate { get; set; }

        // @property (assign, nonatomic) float height;
        [Export("height")]
        float Height { get; set; }

        // @property (assign, nonatomic) _Bool viewWrap;
        [Export("viewWrap")]
        bool ViewWrap { get; set; }

        // -(MaplyBoundingBox)getViewExtents;
        [Export("getViewExtents")]

        MaplyBoundingBox ViewExtents { get; }

        // -(void)getViewExtentsLL:(MaplyCoordinate * _Nonnull)ll ur:(MaplyCoordinate * _Nonnull)ur;
        [Export("getViewExtentsLL:ur:")]
        unsafe void GetViewExtentsLL(NSNumber[] ll, NSNumber[] ur);

        // -(void)setViewExtents:(MaplyBoundingBox)box;
        [Export("setViewExtents:")]
        void SetViewExtents(MaplyBoundingBox box);

        // -(void)setViewExtentsLL:(MaplyCoordinate)ll ur:(MaplyCoordinate)ur;
        [Export("setViewExtentsLL:ur:")]
        void SetViewExtentsLL(MaplyCoordinate ll, MaplyCoordinate ur);

        // -(void)animateToPosition:(MaplyCoordinate)newPos time:(NSTimeInterval)howLong;
        [Export("animateToPosition:time:")]
        void AnimateToPosition(MaplyCoordinate newPos, double howLong);

        // -(void)animateToExtentsWindowSize:(CGSize)windowSize contentOffset:(CGPoint)contentOffset time:(NSTimeInterval)howLong;
        [Export("animateToExtentsWindowSize:contentOffset:time:")]
        void AnimateToExtentsWindowSize(CGSize windowSize, CGPoint contentOffset, double howLong);

        // -(_Bool)animateToPosition:(MaplyCoordinate)newPos onScreen:(CGPoint)loc time:(NSTimeInterval)howLong;
        [Export("animateToPosition:onScreen:time:")]
        bool AnimateToPosition(MaplyCoordinate newPos, CGPoint loc, double howLong);

        // -(void)animateToPosition:(MaplyCoordinate)newPos height:(float)newHeight time:(NSTimeInterval)howLong;
        [Export("animateToPosition:height:time:")]
        void AnimateToPosition(MaplyCoordinate newPos, float newHeight, double howLong);

        // -(_Bool)animateToPosition:(MaplyCoordinate)newPos height:(float)newHeight heading:(float)newHeading time:(NSTimeInterval)howLong;
        [Export("animateToPosition:height:heading:time:")]
        bool AnimateToPosition(MaplyCoordinate newPos, float newHeight, float newHeading, double howLong);

        // -(_Bool)animateToPositionD:(MaplyCoordinateD)newPos height:(double)newHeight heading:(double)newHeading time:(NSTimeInterval)howLong;
        [Export("animateToPositionD:height:heading:time:")]
        bool AnimateToPositionD(MaplyCoordinateD newPos, double newHeight, double newHeading, double howLong);

        // -(_Bool)animateToPosition:(MaplyCoordinate)newPos onScreen:(CGPoint)loc height:(float)newHeight heading:(float)newHeading time:(NSTimeInterval)howLong;
        [Export("animateToPosition:onScreen:height:heading:time:")]
        bool AnimateToPosition(MaplyCoordinate newPos, CGPoint loc, float newHeight, float newHeading, double howLong);

        // -(void)setPosition:(MaplyCoordinate)newPos;
        [Export("setPosition:")]
        void SetPosition(MaplyCoordinate newPos);

        // -(void)setPosition:(MaplyCoordinate)newPos height:(float)height;
        [Export("setPosition:height:")]
        void SetPosition(MaplyCoordinate newPos, float height);

        // -(MaplyCoordinate)getPosition;
        [Export("getPosition")]

        MaplyCoordinate Position { get; }

        // -(float)getHeight;
        [Export("getHeight")]

        float GetHeight();

        // -(void)getPosition:(MaplyCoordinate * _Nonnull)pos height:(float * _Nonnull)height;
        [Export("getPosition:height:")]
        unsafe void GetPosition(ref MaplyCoordinate pos, ref float height);

        // -(void)setViewState:(MaplyViewControllerAnimationState * _Nonnull)viewState;
        [Export("setViewState:")]
        void SetViewState(MaplyViewControllerAnimationState viewState);

        // -(MaplyViewControllerAnimationState * _Nullable)getViewState;
        [NullAllowed, Export("getViewState")]

        MaplyViewControllerAnimationState ViewState { get; }

        // -(float)getMinZoom;
        [Export("getMinZoom")]

        float MinZoom { get; }

        // -(float)getMaxZoom;
        [Export("getMaxZoom")]

        float MaxZoom { get; }

        // -(void)getZoomLimitsMin:(float * _Nonnull)minHeight max:(float * _Nonnull)maxHeight;
        [Export("getZoomLimitsMin:max:")]
        unsafe void GetZoomLimitsMin(ref float minHeight, ref float maxHeight);

        // -(void)setZoomLimitsMin:(float)minHeight max:(float)maxHeight;
        [Export("setZoomLimitsMin:max:")]
        void SetZoomLimitsMin(float minHeight, float maxHeight);

        // -(MaplyCoordinate)geoFromScreenPoint:(CGPoint)point;
        [Export("geoFromScreenPoint:")]
        MaplyCoordinate GeoFromScreenPoint(CGPoint point);

        // -(float)findHeightToViewBounds:(MaplyBoundingBox)bbox pos:(MaplyCoordinate)pos;
        [Export("findHeightToViewBounds:pos:")]
        float FindHeightToViewBounds(MaplyBoundingBox bbox, MaplyCoordinate pos);

        // -(MaplyBoundingBox)getCurrentExtents;
        [Export("getCurrentExtents")]

        MaplyBoundingBox CurrentExtents { get; }

        // -(void)requirePanGestureRecognizerToFailForGesture:(UIGestureRecognizer * _Nullable)other;
        [Export("requirePanGestureRecognizerToFailForGesture:")]
        void RequirePanGestureRecognizerToFailForGesture([NullAllowed] UIGestureRecognizer other);
    }

    // @interface WhirlyGlobeViewControllerAnimationState : NSObject
    [BaseType(typeof(NSObject))]
    interface WhirlyGlobeViewControllerAnimationState
    {
        // @property (nonatomic) double heading;
        [Export("heading")]
        double Heading { get; set; }

        // @property (nonatomic) double height;
        [Export("height")]
        double Height { get; set; }

        // @property (nonatomic) double tilt;
        [Export("tilt")]
        double Tilt { get; set; }

        // @property (nonatomic) MaplyCoordinateD pos;
        [Export("pos", ArgumentSemantic.Assign)]
        MaplyCoordinateD Pos { get; set; }

        // @property (nonatomic) CGPoint screenPos;
        [Export("screenPos", ArgumentSemantic.Assign)]
        CGPoint ScreenPos { get; set; }

        // +(WhirlyGlobeViewControllerAnimationState * _Nonnull)Interpolate:(double)t from:(WhirlyGlobeViewControllerAnimationState * _Nonnull)stateA to:(WhirlyGlobeViewControllerAnimationState * _Nonnull)stateB;
        [Static]
        [Export("Interpolate:from:to:")]
        WhirlyGlobeViewControllerAnimationState Interpolate(double t, WhirlyGlobeViewControllerAnimationState stateA, WhirlyGlobeViewControllerAnimationState stateB);
    }

    // @protocol WhirlyGlobeViewControllerAnimationDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface WhirlyGlobeViewControllerAnimationDelegate
    {
        // @required -(void)globeViewController:(WhirlyGlobeViewController * _Nonnull)viewC startState:(WhirlyGlobeViewControllerAnimationState * _Nonnull)startState startTime:(NSTimeInterval)startTime endTime:(NSTimeInterval)endTime;
        [Abstract]
        [Export("globeViewController:startState:startTime:endTime:")]
        void GlobeViewController(WhirlyGlobeViewController viewC, WhirlyGlobeViewControllerAnimationState startState, double startTime, double endTime);

        // @required -(WhirlyGlobeViewControllerAnimationState * _Nonnull)globeViewController:(WhirlyGlobeViewController * _Nonnull)viewC stateForTime:(NSTimeInterval)currentTime;
        [Abstract]
        [Export("globeViewController:stateForTime:")]
        WhirlyGlobeViewControllerAnimationState GlobeViewController(WhirlyGlobeViewController viewC, double currentTime);

        // @optional -(void)globeViewControllerDidFinishAnimation:(WhirlyGlobeViewController * _Nonnull)viewC;
        [Export("globeViewControllerDidFinishAnimation:")]
        void GlobeViewControllerDidFinishAnimation(WhirlyGlobeViewController viewC);
    }

    // @interface WhirlyGlobeViewControllerSimpleAnimationDelegate : NSObject <WhirlyGlobeViewControllerAnimationDelegate>
    [BaseType(typeof(NSObject))]
    interface WhirlyGlobeViewControllerSimpleAnimationDelegate : WhirlyGlobeViewControllerAnimationDelegate
    {
        // -(instancetype _Nonnull)initWithState:(WhirlyGlobeViewControllerAnimationState * _Nonnull)endState;
        [Export("initWithState:")]
        IntPtr Constructor(WhirlyGlobeViewControllerAnimationState endState);

        // @property (nonatomic) MaplyCoordinateD loc;
        [Export("loc", ArgumentSemantic.Assign)]
        MaplyCoordinateD Loc { get; set; }

        // @property (nonatomic) double heading;
        [Export("heading")]
        double Heading { get; set; }

        // @property (nonatomic) double height;
        [Export("height")]
        double Height { get; set; }

        // @property (nonatomic) double tilt;
        [Export("tilt")]
        double Tilt { get; set; }
    }

    // @protocol WhirlyGlobeViewControllerDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface WhirlyGlobeViewControllerDelegate
    {
        // @optional -(void)globeViewController:(WhirlyGlobeViewController * _Nonnull)viewC didSelect:(NSObject * _Nonnull)selectedObj;
        [Export("globeViewController:didSelect:")]
        void DidSelect(WhirlyGlobeViewController viewC, NSObject selectedObj);

        // @optional -(void)globeViewController:(WhirlyGlobeViewController * _Nonnull)viewC didSelect:(NSObject * _Nonnull)selectedObj atLoc:(MaplyCoordinate)coord onScreen:(CGPoint)screenPt;
        [Export("globeViewController:didSelect:atLoc:onScreen:")]
        void DidSelect(WhirlyGlobeViewController viewC, NSObject selectedObj, MaplyCoordinate coord, CGPoint screenPt);

        // @optional -(void)globeViewController:(WhirlyGlobeViewController * _Nonnull)viewC allSelect:(NSArray * _Nonnull)selectedObjs atLoc:(MaplyCoordinate)coord onScreen:(CGPoint)screenPt;
        [Export("globeViewController:allSelect:atLoc:onScreen:")]

        void AllSelect(WhirlyGlobeViewController viewC, NSObject[] selectedObjs, MaplyCoordinate coord, CGPoint screenPt);

        // @optional -(void)globeViewControllerDidTapOutside:(WhirlyGlobeViewController * _Nonnull)viewC;
        [Export("globeViewControllerDidTapOutside:")]
        void DidTapOutside(WhirlyGlobeViewController viewC);

        // @optional -(void)globeViewController:(WhirlyGlobeViewController * _Nonnull)viewC didTapAt:(MaplyCoordinate)coord;
        [Export("globeViewController:didTapAt:")]
        void DidTapAt(WhirlyGlobeViewController viewC, MaplyCoordinate coord);

        // @optional -(void)globeViewController:(WhirlyGlobeViewController * _Nonnull)viewC layerDidLoad:(MaplyViewControllerLayer * _Nonnull)layer;
        [Export("globeViewController:layerDidLoad:")]
        void LayerDidLoad(WhirlyGlobeViewController viewC, MaplyViewControllerLayer layer);

        // @optional -(void)globeViewControllerDidStartMoving:(WhirlyGlobeViewController * _Nonnull)viewC userMotion:(_Bool)userMotion;
        [Export("globeViewControllerDidStartMoving:userMotion:")]
        void DidStartMoving(WhirlyGlobeViewController viewC, bool userMotion);

        // @optional -(void)globeViewController:(WhirlyGlobeViewController * _Nonnull)viewC didStopMoving:(MaplyCoordinate * _Nonnull)corners userMotion:(_Bool)userMotion;
        [Export("globeViewController:didStopMoving:userMotion:")]
        unsafe void DidStopMoving(WhirlyGlobeViewController viewC, NSNumber[] corners, bool userMotion);

        // @optional -(void)globeViewController:(WhirlyGlobeViewController * _Nonnull)viewC willStopMoving:(MaplyCoordinate * _Nonnull)corners userMotion:(_Bool)userMotion;
        [Export("globeViewController:willStopMoving:userMotion:")]
        unsafe void WillStopMoving(WhirlyGlobeViewController viewC, NSNumber[] corners, bool userMotion);

        // @optional -(void)globeViewController:(WhirlyGlobeViewController * _Nonnull)viewC didMove:(MaplyCoordinate * _Nonnull)corners;
        [Export("globeViewController:didMove:")]
        unsafe void DidMove(WhirlyGlobeViewController viewC, NSNumber[] corners);
    }

    partial interface IWhirlyGlobeViewControllerDelegate { }

    // @interface WhirlyGlobeViewController : MaplyBaseViewController
    [BaseType(typeof(MaplyBaseViewController))]
    interface WhirlyGlobeViewController
    {
        // @property (assign, nonatomic) _Bool keepNorthUp;
        [Export("keepNorthUp")]
        bool KeepNorthUp { get; set; }

        // @property (assign, nonatomic) _Bool panGesture;
        [Export("panGesture")]
        bool PanGesture { get; set; }

        // @property (assign, nonatomic) _Bool pinchGesture;
        [Export("pinchGesture")]
        bool PinchGesture { get; set; }

        // @property (assign, nonatomic) _Bool rotateGesture;
        [Export("rotateGesture")]
        bool RotateGesture { get; set; }

        // @property (assign, nonatomic) _Bool tiltGesture;
        [Export("tiltGesture")]
        bool TiltGesture { get; set; }

        // @property (assign, nonatomic) _Bool doubleTapZoomGesture;
        [Export("doubleTapZoomGesture")]
        bool DoubleTapZoomGesture { get; set; }

        // @property (assign, nonatomic) _Bool twoFingerTapGesture;
        [Export("twoFingerTapGesture")]
        bool TwoFingerTapGesture { get; set; }

        // @property (assign, nonatomic) _Bool doubleTapDragGesture;
        [Export("doubleTapDragGesture")]
        bool DoubleTapDragGesture { get; set; }

        // @property (assign, nonatomic) _Bool inScrollView;
        [Export("inScrollView")]
        bool InScrollView { get; set; }

        // @property (assign, nonatomic) _Bool autoMoveToTap;
        [Export("autoMoveToTap")]
        bool AutoMoveToTap { get; set; }

        [Wrap("WeakDelegate")]
        [NullAllowed]
        WhirlyGlobeViewControllerDelegate Delegate { get; set; }

        // @property (nonatomic, weak) NSObject<WhirlyGlobeViewControllerDelegate> * _Nullable delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
        NSObject WeakDelegate { get; set; }

        // @property (assign, nonatomic) float height;
        [Export("height")]
        float Height { get; set; }

        // @property (assign, nonatomic) float tilt;
        [Export("tilt")]
        float Tilt { get; set; }

        // @property (assign, nonatomic) float heading;
        [Export("heading")]
        float Heading { get; set; }

        // -(float)getZoomLimitsMin;
        [Export("getZoomLimitsMin")]

        float ZoomLimitsMin { get; }

        // -(float)getZoomLimitsMax;
        [Export("getZoomLimitsMax")]

        float ZoomLimitsMax { get; }

        // -(void)getZoomLimitsMin:(float * _Nonnull)minHeight max:(float * _Nonnull)maxHeight;
        [Export("getZoomLimitsMin:max:")]
        unsafe void GetZoomLimitsMin(ref float minHeight, ref float maxHeight);

        // -(void)setZoomLimitsMin:(float)minHeight max:(float)maxHeight;
        [Export("setZoomLimitsMin:max:")]
        void SetZoomLimitsMin(float minHeight, float maxHeight);

        // @property (nonatomic) float zoomTapFactor;
        [Export("zoomTapFactor")]
        float ZoomTapFactor { get; set; }

        // @property (nonatomic) float zoomTapAnimationDuration;
        [Export("zoomTapAnimationDuration")]
        float ZoomTapAnimationDuration { get; set; }

        // -(void)setFarClipPlane:(double)farClipPlane;
        [Export("setFarClipPlane:")]
        void SetFarClipPlane(double farClipPlane);

        // -(void)setTiltMinHeight:(float)minHeight maxHeight:(float)maxHeight minTilt:(float)minTilt maxTilt:(float)maxTilt;
        [Export("setTiltMinHeight:maxHeight:minTilt:maxTilt:")]
        void SetTiltMinHeight(float minHeight, float maxHeight, float minTilt, float maxTilt);

        // -(void)clearTiltHeight;
        [Export("clearTiltHeight")]
        void ClearTiltHeight();

        // -(void)setAutoRotateInterval:(float)autoRotateInterval degrees:(float)autoRotateDegrees;
        [Export("setAutoRotateInterval:degrees:")]
        void SetAutoRotateInterval(float autoRotateInterval, float autoRotateDegrees);

        // -(void)animateToPosition:(MaplyCoordinate)newPos time:(NSTimeInterval)howLong;
        [Export("animateToPosition:time:")]
        void AnimateToPosition(MaplyCoordinate newPos, double howLong);

        // -(_Bool)animateToPosition:(MaplyCoordinate)newPos onScreen:(CGPoint)loc time:(NSTimeInterval)howLong;
        [Export("animateToPosition:onScreen:time:")]
        bool AnimateToPosition(MaplyCoordinate newPos, CGPoint loc, double howLong);

        // -(_Bool)animateToPosition:(MaplyCoordinate)newPos height:(float)newHeight heading:(float)newHeading time:(NSTimeInterval)howLong;
        [Export("animateToPosition:height:heading:time:")]
        bool AnimateToPosition(MaplyCoordinate newPos, float newHeight, float newHeading, double howLong);

        // -(_Bool)animateToPositionD:(MaplyCoordinateD)newPos height:(double)newHeight heading:(double)newHeading time:(NSTimeInterval)howLong;
        [Export("animateToPositionD:height:heading:time:")]
        bool AnimateToPositionD(MaplyCoordinateD newPos, double newHeight, double newHeading, double howLong);

        // -(_Bool)animateToPosition:(MaplyCoordinate)newPos onScreen:(CGPoint)loc height:(float)newHeight heading:(float)newHeading time:(NSTimeInterval)howLong;
        [Export("animateToPosition:onScreen:height:heading:time:")]
        bool AnimateToPosition(MaplyCoordinate newPos, CGPoint loc, float newHeight, float newHeading, double howLong);

        // -(void)animateWithDelegate:(NSObject<WhirlyGlobeViewControllerAnimationDelegate> * _Nonnull)animationDelegate time:(NSTimeInterval)howLong;
        [Export("animateWithDelegate:time:")]
        void AnimateWithDelegate(IWhirlyGlobeViewControllerAnimationDelegate animationDelegate, double howLong);

        // -(void)setPosition:(MaplyCoordinate)newPos;
        [Export("setPosition:")]
        void SetPosition(MaplyCoordinate newPos);

        // -(void)setPosition:(MaplyCoordinate)newPos height:(float)height;
        [Export("setPosition:height:")]
        void SetPosition(MaplyCoordinate newPos, float height);

        // -(MaplyCoordinate)getPosition;
        [Export("getPosition")]

        MaplyCoordinate Position { get; }

        // -(MaplyCoordinateD)getPositionD;
        [Export("getPositionD")]

        MaplyCoordinateD PositionD { get; }

        // -(double)getHeight;
        [Export("getHeight")]

        double GetHeight();

        // -(void)setPositionD:(MaplyCoordinateD)newPos height:(double)height;
        [Export("setPositionD:height:")]
        void SetPositionD(MaplyCoordinateD newPos, double height);

        // -(void)getPosition:(MaplyCoordinate * _Nonnull)pos height:(float * _Nonnull)height;
        [Export("getPosition:height:")]
        unsafe void GetPosition(ref MaplyCoordinate pos, ref float height);

        // -(void)getPositionD:(MaplyCoordinateD * _Nonnull)pos height:(double * _Nonnull)height;
        [Export("getPositionD:height:")]
        unsafe void GetPositionD(ref MaplyCoordinateD pos, ref double height);

        // -(void)setViewState:(WhirlyGlobeViewControllerAnimationState * _Nonnull)viewState;
        [Export("setViewState:")]
        void SetViewState(WhirlyGlobeViewControllerAnimationState viewState);

        // -(WhirlyGlobeViewControllerAnimationState * _Nullable)getViewState;
        [NullAllowed, Export("getViewState")]

        WhirlyGlobeViewControllerAnimationState ViewState { get; }

        // -(WhirlyGlobeViewControllerAnimationState * _Nullable)viewStateForLookAt:(MaplyCoordinate)coord tilt:(float)tilt heading:(float)heading altitude:(float)alt range:(float)range;
        [Export("viewStateForLookAt:tilt:heading:altitude:range:")]
        [return: NullAllowed]
        WhirlyGlobeViewControllerAnimationState ViewStateForLookAt(MaplyCoordinate coord, float tilt, float heading, float alt, float range);

        // -(void)applyConstraintsToViewState:(WhirlyGlobeViewControllerAnimationState * _Nonnull)viewState;
        [Export("applyConstraintsToViewState:")]
        void ApplyConstraintsToViewState(WhirlyGlobeViewControllerAnimationState viewState);

        // -(id _Nullable)findObjectAtLocation:(CGPoint)screenPt;
        [Export("findObjectAtLocation:")]
        [return: NullAllowed]
        NSObject FindObjectAtLocation(CGPoint screenPt);

        // -(MaplyViewControllerLayer * _Nullable)addSphericalEarthLayerWithImageSet:(NSString * _Nonnull)name;
        [Export("addSphericalEarthLayerWithImageSet:")]
        [return: NullAllowed]
        MaplyViewControllerLayer AddSphericalEarthLayerWithImageSet(string name);

        // -(CGPoint)screenPointFromGeo:(MaplyCoordinate)geoCoord;
        [Export("screenPointFromGeo:")]
        CGPoint ScreenPointFromGeo(MaplyCoordinate geoCoord);

        // -(_Bool)screenPointFromGeo:(MaplyCoordinate)geoCoord screenPt:(CGPoint * _Nonnull)screenPt;
        [Export("screenPointFromGeo:screenPt:")]
        unsafe bool ScreenPointFromGeo(MaplyCoordinate geoCoord, ref CGPoint screenPt);

        // -(_Bool)geoPointFromScreen:(CGPoint)screenPt geoCoord:(MaplyCoordinate * _Nonnull)geoCoord;
        [Export("geoPointFromScreen:geoCoord:")]
        unsafe bool GeoPointFromScreen(CGPoint screenPt, NSNumber[] geoCoord);

        // -(NSArray * _Nullable)geocPointFromScreen:(CGPoint)screenPt;
        [Export("geocPointFromScreen:")]

        [return: NullAllowed]
        NSObject[] GeocPointFromScreen(CGPoint screenPt);

        // -(_Bool)geocPointFromScreen:(CGPoint)screenPt geocCoord:(double * _Nonnull)retCoords;
        [Export("geocPointFromScreen:geocCoord:")]
        unsafe bool GeocPointFromScreen(CGPoint screenPt, NSNumber[] retCoords);

        // -(float)findHeightToViewBounds:(MaplyBoundingBox)bbox pos:(MaplyCoordinate)pos;
        [Export("findHeightToViewBounds:pos:")]
        float FindHeightToViewBounds(MaplyBoundingBox bbox, MaplyCoordinate pos);

        // -(MaplyBoundingBox)getCurrentExtents;
        [Export("getCurrentExtents")]

        MaplyBoundingBox CurrentExtents { get; }

        // -(_Bool)getCurrentExtents:(MaplyBoundingBox * _Nonnull)bbox;
        [Export("getCurrentExtents:")]
        unsafe bool GetCurrentExtents(ref MaplyBoundingBox bbox);

        // -(int)getUsableGeoBoundsForView:(MaplyBoundingBox * _Nonnull)bboxes visual:(_Bool)visualBoxes;
        [Export("getUsableGeoBoundsForView:visual:")]
        unsafe int GetUsableGeoBoundsForView(ref MaplyBoundingBox bboxes, bool visualBoxes);

        // -(void)requirePanGestureRecognizerToFailForGesture:(UIGestureRecognizer * _Nullable)other;
        [Export("requirePanGestureRecognizerToFailForGesture:")]
        void RequirePanGestureRecognizerToFailForGesture([NullAllowed] UIGestureRecognizer other);
    }

    partial interface IWhirlyGlobeViewControllerAnimationDelegate { }

    // @protocol MaplyPagingDelegate
    [Protocol, Model]
    interface MaplyPagingDelegate
    {
        // @required -(int)minZoom;
        [Abstract]
        [Export("minZoom")]

        int MinZoom { get; }

        // @required -(int)maxZoom;
        [Abstract]
        [Export("maxZoom")]

        int MaxZoom { get; }

        // @required -(void)startFetchForTile:(MaplyTileID)tileID forLayer:(MaplyQuadPagingLayer * _Nonnull)layer;
        [Abstract]
        [Export("startFetchForTile:forLayer:")]
        void StartFetchForTile(MaplyTileID tileID, MaplyQuadPagingLayer layer);

        // @optional -(void)getBoundingBox:(MaplyTileID)tileID ll:(MaplyCoordinate3dD * _Nonnull)ll ur:(MaplyCoordinate3dD * _Nonnull)ur;
        [Export("getBoundingBox:ll:ur:")]
        unsafe void GetBoundingBox(MaplyTileID tileID, ref MaplyCoordinate3dD ll, ref MaplyCoordinate3dD ur);

        // @optional -(void)tileDidUnload:(MaplyTileID)tileID;
        [Export("tileDidUnload:")]
        void TileDidUnload(MaplyTileID tileID);
    }

    // @interface MaplyQuadPagingLayer : MaplyViewControllerLayer
    [BaseType(typeof(MaplyViewControllerLayer))]
    interface MaplyQuadPagingLayer
    {
        // @property (assign, nonatomic) int numSimultaneousFetches;
        [Export("numSimultaneousFetches")]
        int NumSimultaneousFetches { get; set; }

        // @property (assign, nonatomic) float importance;
        [Export("importance")]
        float Importance { get; set; }

        // @property (nonatomic) _Bool flipY;
        [Export("flipY")]
        bool FlipY { get; set; }

        // @property (readonly, nonatomic, weak) MaplyBaseViewController * _Nullable viewC;
        [NullAllowed, Export("viewC", ArgumentSemantic.Weak)]
        MaplyBaseViewController ViewC { get; }

        // -(instancetype _Nullable)initWithCoordSystem:(MaplyCoordinateSystem * _Nonnull)coordSys delegate:(NSObject<MaplyPagingDelegate> * _Nonnull)tileSource;
        [Export("initWithCoordSystem:delegate:")]
        IntPtr Constructor(MaplyCoordinateSystem coordSys, IMaplyPagingDelegate tileSource);

        // @property (nonatomic) _Bool useTargetZoomLevel;
        [Export("useTargetZoomLevel")]
        bool UseTargetZoomLevel { get; set; }

        // @property (nonatomic) _Bool singleLevelLoading;
        [Export("singleLevelLoading")]
        bool SingleLevelLoading { get; set; }

        // -(int)targetZoomLevel;
        [Export("targetZoomLevel")]

        int TargetZoomLevel { get; }

        // @property (nonatomic) _Bool useParentTileBounds;
        [Export("useParentTileBounds")]
        bool UseParentTileBounds { get; set; }

        // @property (nonatomic) _Bool groupChildrenWithParent;
        [Export("groupChildrenWithParent")]
        bool GroupChildrenWithParent { get; set; }

        // -(void)addData:(NSArray * _Nonnull)dataObjects forTile:(MaplyTileID)tileID;
        [Export("addData:forTile:")]

        void AddData(NSObject[] dataObjects, MaplyTileID tileID);

        // -(void)addData:(NSArray * _Nonnull)dataObjects forTile:(MaplyTileID)tileID style:(MaplyQuadPagingDataStyle)dataStyle;
        [Export("addData:forTile:style:")]

        void AddData(NSObject[] dataObjects, MaplyTileID tileID, MaplyQuadPagingDataStyle dataStyle);

        // -(void)tileFailedToLoad:(MaplyTileID)tileID;
        [Export("tileFailedToLoad:")]
        void TileFailedToLoad(MaplyTileID tileID);

        // -(void)tileDidLoad:(MaplyTileID)tileID;
        [Export("tileDidLoad:")]
        void TileDidLoad(MaplyTileID tileID);

        // -(void)tile:(MaplyTileID)tileID hasNumParts:(int)numParts;
        [Export("tile:hasNumParts:")]
        void Tile(MaplyTileID tileID, int numParts);

        // -(void)tileDidLoad:(MaplyTileID)tileID part:(int)whichPart;
        [Export("tileDidLoad:part:")]
        void TileDidLoad(MaplyTileID tileID, int whichPart);

        // -(MaplyBoundingBox)geoBoundsForTile:(MaplyTileID)tileID;
        [Export("geoBoundsForTile:")]
        MaplyBoundingBox GeoBoundsForTile(MaplyTileID tileID);

        // -(void)geoBoundsforTile:(MaplyTileID)tileID ll:(MaplyCoordinate * _Nonnull)ll ur:(MaplyCoordinate * _Nonnull)ur;
        [Export("geoBoundsforTile:ll:ur:")]
        unsafe void GeoBoundsforTile(MaplyTileID tileID, ref MaplyCoordinate ll, ref MaplyCoordinate ur);

        // -(MaplyBoundingBoxD)geoBoundsForTileD:(MaplyTileID)tileID;
        [Export("geoBoundsForTileD:")]
        MaplyBoundingBoxD GeoBoundsForTileD(MaplyTileID tileID);

        // -(void)geoBoundsForTileD:(MaplyTileID)tileID ll:(MaplyCoordinateD * _Nonnull)ll ur:(MaplyCoordinateD * _Nonnull)ur;
        [Export("geoBoundsForTileD:ll:ur:")]
        unsafe void GeoBoundsForTileD(MaplyTileID tileID, ref MaplyCoordinateD ll, ref MaplyCoordinateD ur);

        // -(MaplyBoundingBox)boundsForTile:(MaplyTileID)tileID;
        [Export("boundsForTile:")]
        MaplyBoundingBox BoundsForTile(MaplyTileID tileID);

        // -(void)boundsforTile:(MaplyTileID)tileID ll:(MaplyCoordinate * _Nonnull)ll ur:(MaplyCoordinate * _Nonnull)ur;
        [Export("boundsforTile:ll:ur:")]
        unsafe void BoundsforTile(MaplyTileID tileID, ref MaplyCoordinate ll, ref MaplyCoordinate ur);

        // -(MaplyCoordinate3d)displayCenterForTile:(MaplyTileID)tileID;
        [Export("displayCenterForTile:")]
        MaplyCoordinate3d DisplayCenterForTile(MaplyTileID tileID);

        // @property (nonatomic) int maxTiles;
        [Export("maxTiles")]
        int MaxTiles { get; set; }

        // @property (nonatomic) float minTileHeight;
        [Export("minTileHeight")]
        float MinTileHeight { get; set; }

        // @property (nonatomic) float maxTileHeight;
        [Export("maxTileHeight")]
        float MaxTileHeight { get; set; }

        // -(void)reload;
        [Export("reload")]
        void Reload();

        // -(void)reload:(MaplyBoundingBox)bounds;
        [Export("reload:")]
        void Reload(MaplyBoundingBox bounds);

        // @property (readonly) _Bool valid;
        [Export("valid")]
        bool Valid { get; }

        // -(NSObject<MaplyPagingDelegate> * _Nullable)pagingDelegate;
        [NullAllowed, Export("pagingDelegate")]

        IMaplyPagingDelegate PagingDelegate { get; }
    }

    partial interface IMaplyPagingDelegate { }

    // @interface MaplyViewerState : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyViewerState
    {
        // -(MaplyCoordinate3d)eyePos;
        [Export("eyePos")]

        MaplyCoordinate3d EyePos { get; }
    }

    // @protocol MaplyUpdateDelegate
    [Protocol, Model]
    interface MaplyUpdateDelegate
    {
        // @required -(void)start:(MaplyUpdateLayer * _Nonnull)layer;
        [Abstract]
        [Export("start:")]
        void Start(MaplyUpdateLayer layer);

        // @required -(void)viewerMovedTo:(MaplyViewerState * _Nonnull)viewState layer:(MaplyUpdateLayer * _Nonnull)layer;
        [Abstract]
        [Export("viewerMovedTo:layer:")]
        void ViewerMovedTo(MaplyViewerState viewState, MaplyUpdateLayer layer);

        // @required -(void)teardown:(MaplyUpdateLayer * _Nonnull)layer;
        [Abstract]
        [Export("teardown:")]
        void Teardown(MaplyUpdateLayer layer);
    }

    // @interface MaplyUpdateLayer : MaplyViewControllerLayer
    [BaseType(typeof(MaplyViewControllerLayer))]
    interface MaplyUpdateLayer
    {
        // @property (readonly, nonatomic) double moveDist;
        [Export("moveDist")]
        double MoveDist { get; }

        // @property (readonly, nonatomic) double minTime;
        [Export("minTime")]
        double MinTime { get; }

        // -(instancetype _Nonnull)initWithDelegate:(NSObject<MaplyUpdateDelegate> * _Nullable)delegate moveDist:(double)moveDist minTime:(double)minTime;
        [Export("initWithDelegate:moveDist:minTime:")]
        IntPtr Constructor([NullAllowed] IMaplyUpdateDelegate @delegate, double moveDist, double minTime);
    }

    partial interface IMaplyUpdateDelegate { }

    // @interface MaplyOfflineImage : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyOfflineImage
    {
        // @property (nonatomic) MaplyBoundingBox bbox;
        [Export("bbox", ArgumentSemantic.Assign)]
        MaplyBoundingBox Bbox { get; set; }

        // @property (assign, nonatomic) int frame;
        [Export("frame")]
        int Frame { get; set; }

        // @property (nonatomic, strong) MaplyTexture * _Nullable tex;
        [NullAllowed, Export("tex", ArgumentSemantic.Strong)]
        MaplyTexture Tex { get; set; }

        // @property (nonatomic) CGSize centerSize;
        [Export("centerSize", ArgumentSemantic.Assign)]
        CGSize CenterSize { get; set; }

        // @property (nonatomic) CGSize texSize;
        [Export("texSize", ArgumentSemantic.Assign)]
        CGSize TexSize { get; set; }
    }

    // @protocol MaplyQuadImageOfflineDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface MaplyQuadImageOfflineDelegate
    {
        // @required -(void)offlineLayer:(MaplyQuadImageOfflineLayer * _Nonnull)layer image:(MaplyOfflineImage * _Nonnull)offlineImage;
        [Abstract]
        [Export("offlineLayer:image:")]
        void Image(MaplyQuadImageOfflineLayer layer, MaplyOfflineImage offlineImage);
    }

    partial interface IMaplyTileSource { }

    // @interface MaplyQuadImageOfflineLayer : MaplyViewControllerLayer
    [BaseType(typeof(MaplyViewControllerLayer))]
    interface MaplyQuadImageOfflineLayer
    {
        // -(instancetype _Nullable)initWithCoordSystem:(MaplyCoordinateSystem * _Nonnull)coordSys tileSource:(NSObject<MaplyTileSource> * _Nonnull)tileSource;
        [Export("initWithCoordSystem:tileSource:")]
        IntPtr Constructor(MaplyCoordinateSystem coordSys, IMaplyTileSource tileSource);

        // @property (nonatomic, strong) NSObject<MaplyTileSource> * _Nonnull tileSource;
        [Export("tileSource", ArgumentSemantic.Strong)]
        IMaplyTileSource TileSource { get; set; }

        // @property (assign, nonatomic) _Bool on;
        [Export("on")]
        bool On { get; set; }

        // @property (assign, nonatomic) int numSimultaneousFetches;
        [Export("numSimultaneousFetches")]
        int NumSimultaneousFetches { get; set; }

        // @property (assign, nonatomic) unsigned int imageDepth;
        [Export("imageDepth")]
        uint ImageDepth { get; set; }

        // @property (nonatomic) _Bool flipY;
        [Export("flipY")]
        bool FlipY { get; set; }

        // @property (nonatomic) int maxTiles;
        [Export("maxTiles")]
        int MaxTiles { get; set; }

        // @property (nonatomic) float importanceScale;
        [Export("importanceScale")]
        float ImportanceScale { get; set; }

        // @property (nonatomic) int previewLevels;
        [Export("previewLevels")]
        int PreviewLevels { get; set; }

        // @property (nonatomic) CGSize textureSize;
        [Export("textureSize", ArgumentSemantic.Assign)]
        CGSize TextureSize { get; set; }

        // @property (nonatomic) _Bool autoRes;
        [Export("autoRes")]
        bool AutoRes { get; set; }

        // @property (assign, nonatomic) _Bool asyncFetching;
        [Export("asyncFetching")]
        bool AsyncFetching { get; set; }

        // @property (nonatomic) float period;
        [Export("period")]
        float Period { get; set; }

        // @property (nonatomic) _Bool singleLevelLoading;
        [Export("singleLevelLoading")]
        bool SingleLevelLoading { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable multiLevelLoads;
        [NullAllowed, Export("multiLevelLoads", ArgumentSemantic.Strong)]

        NSObject[] MultiLevelLoads { get; set; }

        // @property (nonatomic) MaplyBoundingBox bbox;
        [Export("bbox", ArgumentSemantic.Assign)]
        MaplyBoundingBox Bbox { get; set; }

        // -(void)setFrameLoadingPriority:(NSArray * _Nullable)priorities;
        [Export("setFrameLoadingPriority:")]

        void SetFrameLoadingPriority([NullAllowed] NSObject[] priorities);

        [Wrap("WeakDelegate")]
        [NullAllowed]
        MaplyQuadImageOfflineDelegate Delegate { get; set; }

        // @property (nonatomic, weak) NSObject<MaplyQuadImageOfflineDelegate> * _Nullable delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
        NSObject WeakDelegate { get; set; }

        // -(NSArray * _Nullable)loadedFrames;
        [NullAllowed, Export("loadedFrames")]

        NSObject[] LoadedFrames { get; }

        // -(void)reload;
        [Export("reload")]
        void Reload();
    }

    partial interface IMaplyQuadImageOfflineDelegate { }

    // @interface MaplyBlankTileSource : NSObject <MaplyTileSource>
    [BaseType(typeof(NSObject))]
    interface MaplyBlankTileSource : MaplyTileSource
    {
        // -(instancetype _Nonnull)initWithCoordSys:(MaplyCoordinateSystem * _Nonnull)coordSys minZoom:(int)minZoom maxZoom:(int)maxZoom;
        [Export("initWithCoordSys:minZoom:maxZoom:")]
        IntPtr Constructor(MaplyCoordinateSystem coordSys, int minZoom, int maxZoom);

        //// @property (readonly, nonatomic) MaplyCoordinateSystem * _Nonnull coordSys;
        //[Export("coordSys")]
        //MaplyCoordinateSystem CoordSys { get; }

        // @property (nonatomic) int pixelsPerSide;
        [Export("pixelsPerSide")]
        int PixelsPerSide { get; set; }

        // @property (nonatomic, strong) UIColor * _Nullable color;
        [NullAllowed, Export("color", ArgumentSemantic.Strong)]
        UIColor Color { get; set; }
    }

    // @interface MaplySphericalQuadEarthWithTexGroup : MaplyViewControllerLayer
    [BaseType(typeof(MaplyViewControllerLayer))]
    interface MaplySphericalQuadEarthWithTexGroup
    {
        // -(instancetype _Nonnull)initWithWithTexGroup:(NSString * _Nonnull)texGroupName;
        [Export("initWithWithTexGroup:")]
        IntPtr Constructor(string texGroupName);
    }

    // @interface MaplyWMSLayerBoundingBox : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyWMSLayerBoundingBox
    {
        // @property (nonatomic, strong) NSString * _Nullable crs;
        [NullAllowed, Export("crs", ArgumentSemantic.Strong)]
        string Crs { get; set; }

        // @property (nonatomic) double minx;
        [Export("minx")]
        double Minx { get; set; }

        // @property (nonatomic) double miny;
        [Export("miny")]
        double Miny { get; set; }

        // @property (nonatomic) double maxx;
        [Export("maxx")]
        double Maxx { get; set; }

        // @property (nonatomic) double maxy;
        [Export("maxy")]
        double Maxy { get; set; }

        // -(MaplyCoordinateSystem * _Nullable)buildCoordinateSystem;
        [NullAllowed, Export("buildCoordinateSystem")]

        MaplyCoordinateSystem BuildCoordinateSystem { get; }
    }

    // @interface MaplyWMSStyle : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyWMSStyle
    {
        // @property (nonatomic, strong) NSString * _Nullable name;
        [NullAllowed, Export("name", ArgumentSemantic.Strong)]
        string Name { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable title;
        [NullAllowed, Export("title", ArgumentSemantic.Strong)]
        string Title { get; set; }
    }

    // @interface MaplyWMSLayer : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyWMSLayer
    {
        // @property (nonatomic, strong) NSString * _Nullable name;
        [NullAllowed, Export("name", ArgumentSemantic.Strong)]
        string Name { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable title;
        [NullAllowed, Export("title", ArgumentSemantic.Strong)]
        string Title { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable abstract;
        [NullAllowed, Export("abstract", ArgumentSemantic.Strong)]
        string Abstract { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable coordRefSystems;
        [NullAllowed, Export("coordRefSystems", ArgumentSemantic.Strong)]

        NSObject[] CoordRefSystems { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable styles;
        [NullAllowed, Export("styles", ArgumentSemantic.Strong)]

        NSObject[] Styles { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable boundingBoxes;
        [NullAllowed, Export("boundingBoxes", ArgumentSemantic.Strong)]

        NSObject[] BoundingBoxes { get; set; }

        // @property (nonatomic) MaplyCoordinate ll;
        [Export("ll", ArgumentSemantic.Assign)]
        MaplyCoordinate Ll { get; set; }

        // @property (nonatomic) MaplyCoordinate ur;
        [Export("ur", ArgumentSemantic.Assign)]
        MaplyCoordinate Ur { get; set; }

        // -(MaplyCoordinateSystem * _Nullable)buildCoordSystem;
        [NullAllowed, Export("buildCoordSystem")]

        MaplyCoordinateSystem BuildCoordSystem { get; }

        // -(MaplyWMSStyle * _Nullable)findStyle:(NSString * _Nonnull)styleName;
        [Export("findStyle:")]
        [return: NullAllowed]
        MaplyWMSStyle FindStyle(string styleName);
    }

    // @interface MaplyWMSCapabilities : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyWMSCapabilities
    {
        // +(NSString * _Nonnull)CapabilitiesURLFor:(NSString * _Nonnull)baseURL;
        [Static]
        [Export("CapabilitiesURLFor:")]
        string CapabilitiesURLFor(string baseURL);

        // @property (nonatomic, strong) NSString * _Nullable name;
        [NullAllowed, Export("name", ArgumentSemantic.Strong)]
        string Name { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable title;
        [NullAllowed, Export("title", ArgumentSemantic.Strong)]
        string Title { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable formats;
        [NullAllowed, Export("formats", ArgumentSemantic.Strong)]

        NSObject[] Formats { get; set; }

        // @property (nonatomic, strong) NSArray * _Nullable layers;
        [NullAllowed, Export("layers", ArgumentSemantic.Strong)]

        NSObject[] Layers { get; set; }

        //// -(instancetype _Nullable)initWithXML:(DDXMLDocument * _Nonnull)xmlDoc;
        //[Export ("initWithXML:")] TODO
        //IntPtr Constructor (DDXMLDocument xmlDoc);

        // -(MaplyWMSLayer * _Nullable)findLayer:(NSString * _Nonnull)name;
        [Export("findLayer:")]
        [return: NullAllowed]
        MaplyWMSLayer FindLayer(string name);
    }

    // @interface MaplyWMSTileSource : NSObject <MaplyTileSource>
    [BaseType(typeof(NSObject))]
    interface MaplyWMSTileSource : MaplyTileSource
    {
        // @property (nonatomic, strong) NSString * _Nullable baseURL;
        [NullAllowed, Export("baseURL", ArgumentSemantic.Strong)]
        string BaseURL { get; set; }

        // @property (nonatomic, strong) MaplyWMSCapabilities * _Nullable capabilities;
        [NullAllowed, Export("capabilities", ArgumentSemantic.Strong)]
        MaplyWMSCapabilities Capabilities { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable imageType;
        [NullAllowed, Export("imageType", ArgumentSemantic.Strong)]
        string ImageType { get; set; }

        // @property (nonatomic, strong) MaplyWMSLayer * _Nonnull layer;
        [Export("layer", ArgumentSemantic.Strong)]
        MaplyWMSLayer Layer { get; set; }

        // @property (nonatomic, strong) MaplyWMSStyle * _Nonnull style;
        [Export("style", ArgumentSemantic.Strong)]
        MaplyWMSStyle Style { get; set; }

        //// @property (readonly, nonatomic) int minZoom;
        //[Export("minZoom")]
        //int MinZoom { get; }

        //// @property (readonly, nonatomic) int maxZoom;
        //[Export("maxZoom")]
        //int MaxZoom { get; }

        //// @property (readonly, nonatomic) int tileSize;
        //[Export("tileSize")]
        //int TileSize { get; }

        // @property (nonatomic) _Bool transparent;
        [Export("transparent")]
        bool Transparent { get; set; }

        //// @property (readonly, nonatomic) MaplyCoordinateSystem * _Nonnull coordSys;
        //[Export("coordSys")]
        //MaplyCoordinateSystem CoordSys { get; }

        // @property (nonatomic, strong) NSString * _Nullable cacheDir;
        [NullAllowed, Export("cacheDir", ArgumentSemantic.Strong)]
        string CacheDir { get; set; }

        // -(instancetype _Nullable)initWithBaseURL:(NSString * _Nonnull)baseURL capabilities:(MaplyWMSCapabilities * _Nullable)cap layer:(MaplyWMSLayer * _Nonnull)layer style:(MaplyWMSStyle * _Nonnull)style coordSys:(MaplyCoordinateSystem * _Nonnull)coordSys minZoom:(int)minZoom maxZoom:(int)maxZoom tileSize:(int)tileSize;
        [Export("initWithBaseURL:capabilities:layer:style:coordSys:minZoom:maxZoom:tileSize:")]
        IntPtr Constructor(string baseURL, [NullAllowed] MaplyWMSCapabilities cap, MaplyWMSLayer layer, MaplyWMSStyle style, MaplyCoordinateSystem coordSys, int minZoom, int maxZoom, int tileSize);
    }

    // @interface MaplyMBTileSource : NSObject <MaplyTileSource>
    [BaseType(typeof(NSObject))]
    interface MaplyMBTileSource : MaplyTileSource
    {
        // -(instancetype _Nullable)initWithMBTiles:(NSString * _Nonnull)fileName;
        [Export("initWithMBTiles:")]
        IntPtr Constructor(string fileName);

        //// @property (nonatomic) int maxZoom;
        //[Export("maxZoom")]
        //int MaxZoom { get; set; }

        //// @property (nonatomic) int minZoom;
        //[Export("minZoom")]
        //int MinZoom { get; set; }

        // -(MaplyBoundingBox)getBounds;
        [Export("getBounds")]

        MaplyBoundingBox Bounds { get; }

        //// @property (readonly, nonatomic) MaplyCoordinateSystem * _Nonnull coordSys;
        //[Export("coordSys")]
        //MaplyCoordinateSystem CoordSys { get; }
    }

    // @interface MaplyGDALRetileSource : NSObject <MaplyTileSource>
    [BaseType(typeof(NSObject))]
    interface MaplyGDALRetileSource : MaplyTileSource
    {
        // -(instancetype _Nonnull)initWithURL:(NSString * _Nonnull)baseURL baseName:(NSString * _Nonnull)baseName ext:(NSString * _Nonnull)ext coordSys:(MaplyCoordinateSystem * _Nonnull)coordSys levels:(int)numLevels;
        [Export("initWithURL:baseName:ext:coordSys:levels:")]
        IntPtr Constructor(string baseURL, string baseName, string ext, MaplyCoordinateSystem coordSys, int numLevels);

        // @property (nonatomic, strong) NSString * _Nullable cacheDir;
        [NullAllowed, Export("cacheDir", ArgumentSemantic.Strong)]
        string CacheDir { get; set; }
    }

    // @protocol MaplyRemoteTileInfoProtocol
    [Protocol, Model]
    interface MaplyRemoteTileInfoProtocol
    {
        // @required -(MaplyCoordinateSystem * _Nullable)coordSys;
        [Abstract]
        [NullAllowed, Export("coordSys")]

        MaplyCoordinateSystem CoordSys { get; }

        // @required -(int)minZoom;
        [Abstract]
        [Export("minZoom")]

        int MinZoom { get; }

        // @required -(int)maxZoom;
        [Abstract]
        [Export("maxZoom")]

        int MaxZoom { get; }

        // @required -(int)pixelsPerSide;
        [Abstract]
        [Export("pixelsPerSide")]

        int PixelsPerSide { get; }

        // @required -(NSURLRequest * _Nullable)requestForTile:(MaplyTileID)tileID;
        [Abstract]
        [Export("requestForTile:")]
        [return: NullAllowed]
        NSUrlRequest RequestForTile(MaplyTileID tileID);

        // @required -(_Bool)tileIsLocal:(MaplyTileID)tileID frame:(int)frame;
        [Abstract]
        [Export("tileIsLocal:frame:")]
        bool TileIsLocal(MaplyTileID tileID, int frame);

        // @required -(_Bool)validTile:(MaplyTileID)tileID bbox:(MaplyBoundingBox)bbox;
        [Abstract]
        [Export("validTile:bbox:")]
        bool ValidTile(MaplyTileID tileID, MaplyBoundingBox bbox);

        // @required -(NSData * _Nullable)readFromCache:(MaplyTileID)tileID;
        [Abstract]
        [Export("readFromCache:")]
        [return: NullAllowed]
        NSData ReadFromCache(MaplyTileID tileID);

        // @required -(void)writeToCache:(MaplyTileID)tileID tileData:(NSData * _Nonnull)tileData;
        [Abstract]
        [Export("writeToCache:tileData:")]
        void WriteToCache(MaplyTileID tileID, NSData tileData);
    }

    // @interface MaplyRemoteTileInfo : NSObject <MaplyRemoteTileInfoProtocol>
    [BaseType(typeof(NSObject))]
    interface MaplyRemoteTileInfo : MaplyRemoteTileInfoProtocol
    {
        // -(instancetype _Nonnull)initWithBaseURL:(NSString * _Nonnull)baseURL ext:(NSString * _Nullable)ext minZoom:(int)minZoom maxZoom:(int)maxZoom;
        [Export("initWithBaseURL:ext:minZoom:maxZoom:")]
        IntPtr Constructor(string baseURL, [NullAllowed] string ext, int minZoom, int maxZoom);

        // -(instancetype _Nullable)initWithTilespec:(NSDictionary * _Nonnull)jsonSpec;
        [Export("initWithTilespec:")]
        IntPtr Constructor(NSDictionary jsonSpec);

        // @property (readonly, nonatomic) NSString * _Nonnull baseURL;
        [Export("baseURL")]
        string BaseURL { get; }

        // @property (nonatomic) int minZoom;
        [Export("minZoom")]
        int MinZoom { get; set; }

        // @property (nonatomic) int maxZoom;
        [Export("maxZoom")]
        int MaxZoom { get; set; }

        // @property (nonatomic, strong) NSString * _Nonnull ext;
        [Export("ext", ArgumentSemantic.Strong)]
        string Ext { get; set; }

        // @property (assign, nonatomic) float timeOut;
        [Export("timeOut")]
        float TimeOut { get; set; }

        // @property (nonatomic) int pixelsPerSide;
        [Export("pixelsPerSide")]
        int PixelsPerSide { get; set; }

        // @property (nonatomic, strong) MaplyCoordinateSystem * _Nullable coordSys;
        [NullAllowed, Export("coordSys", ArgumentSemantic.Strong)]
        MaplyCoordinateSystem CoordSys { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable cacheDir;
        [NullAllowed, Export("cacheDir", ArgumentSemantic.Strong)]
        string CacheDir { get; set; }

        // @property (nonatomic) int cachedFileLifetime;
        [Export("cachedFileLifetime")]
        int CachedFileLifetime { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable queryStr;
        [NullAllowed, Export("queryStr", ArgumentSemantic.Strong)]
        string QueryStr { get; set; }

        // @property (nonatomic) _Bool replaceURL;
        [Export("replaceURL")]
        bool ReplaceURL { get; set; }

        // -(void)addBoundingBox:(MaplyBoundingBox * _Nonnull)bbox;
        [Export("addBoundingBox:")]
        unsafe void AddBoundingBox(ref MaplyBoundingBox bbox);

        // -(void)addGeoBoundingBox:(MaplyBoundingBox * _Nonnull)bbox;
        [Export("addGeoBoundingBox:")]
        unsafe void AddGeoBoundingBox(ref MaplyBoundingBox bbox);

        // -(NSURLRequest * _Nullable)requestForTile:(MaplyTileID)tileID;
        [Export("requestForTile:")]
        [return: NullAllowed]
        NSUrlRequest RequestForTile(MaplyTileID tileID);

        // -(NSString * _Nullable)fileNameForTile:(MaplyTileID)tileID;
        [Export("fileNameForTile:")]
        [return: NullAllowed]
        string FileNameForTile(MaplyTileID tileID);

        // -(_Bool)tileIsLocal:(MaplyTileID)tileID frame:(int)frame;
        [Export("tileIsLocal:frame:")]
        bool TileIsLocal(MaplyTileID tileID, int frame);

        // -(_Bool)validTile:(MaplyTileID)tileID bbox:(MaplyBoundingBox)bbox;
        [Export("validTile:bbox:")]
        bool ValidTile(MaplyTileID tileID, MaplyBoundingBox bbox);

        // @property (nonatomic, strong) id _Nullable userObject;
        [NullAllowed, Export("userObject", ArgumentSemantic.Strong)]
        NSObject UserObject { get; set; }
    }

    // @protocol MaplyRemoteTileSourceDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface MaplyRemoteTileSourceDelegate
    {
        // @optional -(void)remoteTileSource:(id _Nonnull)tileSource tileDidLoad:(MaplyTileID)tileID;
        [Export("remoteTileSource:tileDidLoad:")]
        void TileDidLoad(NSObject tileSource, MaplyTileID tileID);

        // @optional -(NSData * _Nonnull)remoteTileSource:(id _Nonnull)tileSource modifyTileReturn:(NSData * _Nonnull)tileData forTile:(MaplyTileID)tileID;
        [Export("remoteTileSource:modifyTileReturn:forTile:")]
        NSData ModifyTileReturn(NSObject tileSource, NSData tileData, MaplyTileID tileID);

        // @optional -(void)remoteTileSource:(id _Nonnull)tileSource tileDidNotLoad:(MaplyTileID)tileID error:(NSError * _Nonnull)error;
        [Export("remoteTileSource:tileDidNotLoad:error:")]
        void TileDidNotLoad(NSObject tileSource, MaplyTileID tileID, NSError error);

        // @optional -(void)remoteTileSource:(id _Nonnull)tileSource tileDisabled:(MaplyTileID)tileID;
        [Export("remoteTileSource:tileDisabled:")]
        void TileDisabled(NSObject tileSource, MaplyTileID tileID);

        // @optional -(void)remoteTileSource:(id _Nonnull)tileSource tileEnabled:(MaplyTileID)tileID;
        [Export("remoteTileSource:tileEnabled:")]
        void TileEnabled(NSObject tileSource, MaplyTileID tileID);

        // @optional -(void)remoteTileSource:(id _Nonnull)tileSource tileUnloaded:(MaplyTileID)tileID;
        [Export("remoteTileSource:tileUnloaded:")]
        void TileUnloaded(NSObject tileSource, MaplyTileID tileID);
    }

    // @interface MaplyRemoteTileSource : NSObject <MaplyTileSource>
    [BaseType(typeof(NSObject))]
    interface MaplyRemoteTileSource : MaplyTileSource
    {
        // -(instancetype _Nullable)initWithBaseURL:(NSString * _Nonnull)baseURL ext:(NSString * _Nullable)ext minZoom:(int)minZoom maxZoom:(int)maxZoom;
        [Export("initWithBaseURL:ext:minZoom:maxZoom:")]
        IntPtr Constructor(string baseURL, [NullAllowed] string ext, int minZoom, int maxZoom);

        // -(instancetype _Nullable)initWithTilespec:(NSDictionary * _Nonnull)jsonSpec;
        [Export("initWithTilespec:")]
        IntPtr Constructor(NSDictionary jsonSpec);

        // -(id _Nullable)imageForTile:(MaplyTileID)tileID;
        [Export("imageForTile:")]
        [return: NullAllowed]
        NSObject ImageForTile(MaplyTileID tileID);

        // -(instancetype _Nullable)initWithInfo:(NSObject<MaplyRemoteTileInfoProtocol> * _Nonnull)info;
        [Export("initWithInfo:")]
        IntPtr Constructor(IMaplyRemoteTileInfoProtocol info);

        // @property (readonly, nonatomic) NSObject<MaplyRemoteTileInfoProtocol> * _Nonnull tileInfo;
        [Export("tileInfo")]
        IMaplyRemoteTileInfoProtocol TileInfo { get; }

        [Wrap("WeakDelegate")]
        [NullAllowed]
        MaplyRemoteTileSourceDelegate Delegate { get; set; }

        // @property (nonatomic, weak) NSObject<MaplyRemoteTileSourceDelegate> * _Nullable delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
        NSObject WeakDelegate { get; set; }

        // @property (nonatomic, strong) MaplyCoordinateSystem * _Nonnull coordSys;
        [Export("coordSys", ArgumentSemantic.Strong)]
        MaplyCoordinateSystem CoordSys { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable cacheDir;
        [NullAllowed, Export("cacheDir", ArgumentSemantic.Strong)]
        string CacheDir { get; set; }

        // +(void)setTrackConnections:(_Bool)track;
        [Static]
        [Export("setTrackConnections:")]
        void SetTrackConnections(bool track);

        // +(int)numOutstandingConnections;
        [Static]
        [Export("numOutstandingConnections")]

        int NumOutstandingConnections { get; }
    }

    partial interface IMaplyRemoteTileInfoProtocol { }
    partial interface IMaplyRemoteTileSourceDelegate { }

    // @protocol MaplyMultiplexTileSourceDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface MaplyMultiplexTileSourceDelegate
    {
        // @optional -(void)remoteTileInfo:(id _Nonnull)tileInfo tileDidLoad:(MaplyTileID)tileID frame:(int)frame;
        [Export("remoteTileInfo:tileDidLoad:frame:")]
        void TileDidLoad(NSObject tileInfo, MaplyTileID tileID, int frame);

        // @optional -(NSData * _Nonnull)remoteTileInfo:(id _Nonnull)tileInfo modifyTileReturn:(NSData * _Nonnull)tileData forTile:(MaplyTileID)tileID frame:(int)frame;
        [Export("remoteTileInfo:modifyTileReturn:forTile:frame:")]
        NSData ModifyTileReturn(NSObject tileInfo, NSData tileData, MaplyTileID tileID, int frame);

        // @optional -(void)remoteTileInfo:(id _Nonnull)tileInfo tileDidNotLoad:(MaplyTileID)tileID frame:(int)frame error:(NSError * _Nonnull)error;
        [Export("remoteTileInfo:tileDidNotLoad:frame:error:")]
        void TileDidNotLoad(NSObject tileInfo, MaplyTileID tileID, int frame, NSError error);

        // @optional -(void)remoteTileInfo:(id _Nonnull)tileInfo tileUnloaded:(MaplyTileID)tileID frame:(int)frame;
        [Export("remoteTileInfo:tileUnloaded:frame:")]
        void TileUnloaded(NSObject tileInfo, MaplyTileID tileID, int frame);
    }

    // @interface MaplyMultiplexTileSource : NSObject <MaplyTileSource>
    [BaseType(typeof(NSObject))]
    interface MaplyMultiplexTileSource : MaplyTileSource
    {
        // -(instancetype _Nullable)initWithSources:(NSArray * _Nonnull)tileSources;
        [Export("initWithSources:")]

        IntPtr Constructor(NSObject[] tileSources);

        // @property (readonly, nonatomic) MaplyCoordinateSystem * _Nonnull coordSys;
        [Export("coordSys")]
        MaplyCoordinateSystem CoordSys { get; }

        [Wrap("WeakDelegate")]
        [NullAllowed]
        MaplyMultiplexTileSourceDelegate Delegate { get; set; }

        // @property (nonatomic, weak) NSObject<MaplyMultiplexTileSourceDelegate> * _Nullable delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
        NSObject WeakDelegate { get; set; }

        // @property (nonatomic) _Bool acceptFailures;
        [Export("acceptFailures")]
        bool AcceptFailures { get; set; }

        // +(void)setTrackConnections:(_Bool)track;
        [Static]
        [Export("setTrackConnections:")]
        void SetTrackConnections(bool track);

        // +(int)numOutstandingConnections;
        [Static]
        [Export("numOutstandingConnections")]

        int NumOutstandingConnections { get; }
    }

    partial interface IMaplyMultiplexTileSourceDelegate { }

    // @interface MaplyAnimationTestTileSource : NSObject <MaplyTileSource>
    [BaseType(typeof(NSObject))]
    interface MaplyAnimationTestTileSource : MaplyTileSource
    {
        // -(instancetype _Nonnull)initWithCoordSys:(MaplyCoordinateSystem * _Nonnull)coordSys minZoom:(int)minZoom maxZoom:(int)maxZoom depth:(int)depth;
        [Export("initWithCoordSys:minZoom:maxZoom:depth:")]
        IntPtr Constructor(MaplyCoordinateSystem coordSys, int minZoom, int maxZoom, int depth);

        // @property (readonly, nonatomic) MaplyCoordinateSystem * _Nonnull coordSys;
        [Export("coordSys")]
        MaplyCoordinateSystem CoordSys { get; }

        // @property (nonatomic) int pixelsPerSide;
        [Export("pixelsPerSide")]
        int PixelsPerSide { get; set; }

        // @property (nonatomic) _Bool useDelay;
        [Export("useDelay")]
        bool UseDelay { get; set; }

        // @property (nonatomic) _Bool transparentMode;
        [Export("transparentMode")]
        bool TransparentMode { get; set; }
    }

    // @interface MaplyPagingVectorTestTileSource : NSObject <MaplyPagingDelegate>
    [BaseType(typeof(NSObject))]
    interface MaplyPagingVectorTestTileSource : MaplyPagingDelegate
    {
        // -(instancetype _Nonnull)initWithCoordSys:(MaplyCoordinateSystem * _Nonnull)coordSys minZoom:(int)minZoom maxZoom:(int)maxZoom;
        [Export("initWithCoordSys:minZoom:maxZoom:")]
        IntPtr Constructor(MaplyCoordinateSystem coordSys, int minZoom, int maxZoom);

        // @property (readonly, nonatomic) MaplyCoordinateSystem * _Nonnull coordSys;
        [Export("coordSys")]
        MaplyCoordinateSystem CoordSys { get; }

        // @property (nonatomic) int minZoom;
        [Export("minZoom")]
        int MinZoom { get; set; }

        // @property (nonatomic) int maxZoom;
        [Export("maxZoom")]
        int MaxZoom { get; set; }

        // @property (nonatomic) _Bool useDelay;
        [Export("useDelay")]
        bool UseDelay { get; set; }
    }

    // @interface MaplyPagingElevationTestTileSource : NSObject <MaplyPagingDelegate>
    [BaseType(typeof(NSObject))]
    interface MaplyPagingElevationTestTileSource : MaplyPagingDelegate
    {
        // -(instancetype _Nonnull)initWithCoordSys:(MaplyCoordinateSystem * _Nonnull)coordSys minZoom:(int)minZoom maxZoom:(int)maxZoom elevSource:(id<MaplyElevationSourceDelegate> _Nullable)elevSource;
        [Export("initWithCoordSys:minZoom:maxZoom:elevSource:")]
        IntPtr Constructor(MaplyCoordinateSystem coordSys, int minZoom, int maxZoom, [NullAllowed] MaplyElevationSourceDelegate elevSource);

        // @property (readonly, nonatomic) MaplyCoordinateSystem * _Nonnull coordSys;
        [Export("coordSys")]
        MaplyCoordinateSystem CoordSys { get; }

        // @property (nonatomic) int minZoom;
        [Export("minZoom")]
        int MinZoom { get; set; }

        // @property (nonatomic) int maxZoom;
        [Export("maxZoom")]
        int MaxZoom { get; set; }

        // @property (nonatomic) id<MaplyElevationSourceDelegate> _Nullable elevSource;
        [NullAllowed, Export("elevSource", ArgumentSemantic.Assign)]
        IMaplyElevationSourceDelegate ElevSource { get; set; }
    }

    partial interface IMaplyElevationSourceDelegate { }

    // @interface MaplyElevationDatabase : NSObject <MaplyElevationSourceDelegate>
    [BaseType(typeof(NSObject))]
    interface MaplyElevationDatabase : MaplyElevationSourceDelegate
    {
        // @property (nonatomic) double minX;
        [Export("minX")]
        double MinX { get; set; }

        // @property (nonatomic) double minY;
        [Export("minY")]
        double MinY { get; set; }

        // @property (nonatomic) double maxX;
        [Export("maxX")]
        double MaxX { get; set; }

        // @property (nonatomic) double maxY;
        [Export("maxY")]
        double MaxY { get; set; }

        // @property (nonatomic) unsigned int tileSizeX;
        [Export("tileSizeX")]
        uint TileSizeX { get; set; }

        // @property (nonatomic) unsigned int tileSizeY;
        [Export("tileSizeY")]
        uint TileSizeY { get; set; }

        // -(instancetype _Nullable)initWithName:(NSString * _Nonnull)name;
        [Export("initWithName:")]
        IntPtr Constructor(string name);

        // -(int)minZoom;
        [Export("minZoom")]

        int MinZoom { get; }

        // -(int)maxZoom;
        [Export("maxZoom")]

        int MaxZoom { get; }

        // -(MaplyElevationChunk * _Nullable)elevForTile:(MaplyTileID)tileID;
        [Export("elevForTile:")]
        [return: NullAllowed]
        MaplyElevationChunk ElevForTile(MaplyTileID tileID);
    }

    // @interface MaplyIconManager : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyIconManager
    {
        // +(UIImage * _Nullable)iconForName:(NSString * _Nonnull)name size:(CGSize)size;
        [Static]
        [Export("iconForName:size:")]
        [return: NullAllowed]
        UIImage IconForName(string name, CGSize size);

        // +(UIImage * _Nullable)iconForName:(NSString * _Nullable)name size:(CGSize)size color:(UIColor * _Nullable)color circleColor:(UIColor * _Nullable)circleColor strokeSize:(float)strokeSize strokeColor:(UIColor * _Nullable)strokeColor;
        [Static]
        [Export("iconForName:size:color:circleColor:strokeSize:strokeColor:")]
        [return: NullAllowed]
        UIImage IconForName([NullAllowed] string name, CGSize size, [NullAllowed] UIColor color, [NullAllowed] UIColor circleColor, float strokeSize, [NullAllowed] UIColor strokeColor);
    }

    // @interface MaplyGeomModel : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyGeomModel
    {
        // -(instancetype _Nullable)initWithObj:(NSString * _Nonnull)fullPath;
        [Export("initWithObj:")]
        IntPtr Constructor(string fullPath);

        // -(instancetype _Nonnull)initWithShape:(MaplyShape * _Nonnull)shape;
        [Export("initWithShape:")]
        IntPtr Constructor(MaplyShape shape);
    }

    // @interface MaplyGeomModelInstance : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyGeomModelInstance
    {
        // @property (nonatomic, strong) id _Nullable userObject;
        [NullAllowed, Export("userObject", ArgumentSemantic.Strong)]
        NSObject UserObject { get; set; }

        // @property (nonatomic, strong) MaplyGeomModel * _Nullable model;
        [NullAllowed, Export("model", ArgumentSemantic.Strong)]
        MaplyGeomModel Model { get; set; }

        // @property (nonatomic) MaplyCoordinate3d center;
        [Export("center", ArgumentSemantic.Assign)]
        MaplyCoordinate3d Center { get; set; }

        // @property (nonatomic, strong) MaplyMatrix * _Nullable transform;
        [NullAllowed, Export("transform", ArgumentSemantic.Strong)]
        MaplyMatrix Transform { get; set; }

        // @property (nonatomic, strong) UIColor * _Nullable colorOverride;
        [NullAllowed, Export("colorOverride", ArgumentSemantic.Strong)]
        UIColor ColorOverride { get; set; }

        // @property (nonatomic) _Bool selectable;
        [Export("selectable")]
        bool Selectable { get; set; }
    }

    // @interface MaplyMovingGeomModelInstance : MaplyGeomModelInstance
    [BaseType(typeof(MaplyGeomModelInstance))]
    interface MaplyMovingGeomModelInstance
    {
        // @property (assign, nonatomic) MaplyCoordinate3d endCenter;
        [Export("endCenter", ArgumentSemantic.Assign)]
        MaplyCoordinate3d EndCenter { get; set; }

        // @property (assign, nonatomic) NSTimeInterval duration;
        [Export("duration")]
        double Duration { get; set; }
    }

    // @interface MaplyQuadTracker : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyQuadTracker
    {
        // @property (nonatomic, strong) MaplyCoordinateSystem * _Nullable coordSys;
        [NullAllowed, Export("coordSys", ArgumentSemantic.Strong)]
        MaplyCoordinateSystem CoordSys { get; set; }

        // -(instancetype _Nonnull)initWithViewC:(WhirlyGlobeViewController * _Nonnull)viewC;
        [Export("initWithViewC:")]
        IntPtr Constructor(WhirlyGlobeViewController viewC);

        // @property (assign, nonatomic) int minLevel;
        [Export("minLevel")]
        int MinLevel { get; set; }

        // -(void)tiles:(MaplyQuadTrackerPointReturn * _Nonnull)tilesInfo forPoints:(int)numPts;
        [Export("tiles:forPoints:")]
        unsafe void Tiles(ref MaplyQuadTrackerPointReturn tilesInfo, int numPts);

        // -(void)addTile:(MaplyTileID)tileID;
        [Export("addTile:")]
        void AddTile(MaplyTileID tileID);

        // -(void)removeTile:(MaplyTileID)tileID;
        [Export("removeTile:")]
        void RemoveTile(MaplyTileID tileID);

        // -(int)numTiles;
        [Export("numTiles")]

        int NumTiles { get; }
    }

    // @interface MaplyStarsModel : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyStarsModel
    {
        // -(instancetype _Nullable)initWithFileName:(NSString * _Nonnull)fileName;
        [Export("initWithFileName:")]
        IntPtr Constructor(string fileName);

        // -(void)setImage:(UIImage * _Nonnull)image;
        [Export("setImage:")]
        void SetImage(UIImage image);

        // -(void)addToViewC:(WhirlyGlobeViewController * _Nonnull)viewC date:(NSDate * _Nonnull)date desc:(NSDictionary * _Nullable)desc mode:(MaplyThreadMode)mode;
        [Export("addToViewC:date:desc:mode:")]
        void AddToViewC(WhirlyGlobeViewController viewC, NSDate date, [NullAllowed] NSDictionary desc, MaplyThreadMode mode);

        // -(void)removeFromViewC;
        [Export("removeFromViewC")]
        void RemoveFromViewC();
    }

    // @interface MaplySun : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplySun
    {
        // -(instancetype _Nonnull)initWithDate:(NSDate * _Nonnull)date;
        [Export("initWithDate:")]
        IntPtr Constructor(NSDate date);

        // -(MaplyCoordinate3d)getDirection;
        [Export("getDirection")]

        MaplyCoordinate3d Direction { get; }

        // -(MaplyLight * _Nonnull)makeLight;
        [Export("makeLight")]

        MaplyLight MakeLight { get; }

        // -(MaplyCoordinate)asPosition;
        [Export("asPosition")]

        MaplyCoordinate AsPosition { get; }
    }

    // @interface MaplyAtmosphere : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyAtmosphere
    {
        // -(instancetype _Nullable)initWithViewC:(WhirlyGlobeViewController * _Nonnull)viewC;
        [Export("initWithViewC:")]
        IntPtr Constructor(WhirlyGlobeViewController viewC);

        // @property (nonatomic) float Kr;
        [Export("Kr")]
        float Kr { get; set; }

        // @property (nonatomic) float Km;
        [Export("Km")]
        float Km { get; set; }

        // @property (nonatomic) float ESun;
        [Export("ESun")]
        float ESun { get; set; }

        // @property (nonatomic) int numSamples;
        [Export("numSamples")]
        int NumSamples { get; set; }

        // @property (nonatomic) float outerRadius;
        [Export("outerRadius")]
        float OuterRadius { get; set; }

        // @property (nonatomic) float g;
        [Export("g")]
        float G { get; set; }

        // @property (nonatomic) float exposure;
        [Export("exposure")]
        float Exposure { get; set; }

        // @property (nonatomic) MaplyShader * _Nullable groundShader;
        [NullAllowed, Export("groundShader", ArgumentSemantic.Assign)]
        MaplyShader GroundShader { get; set; }

        // @property (nonatomic) _Bool lockToCamera;
        [Export("lockToCamera")]
        bool LockToCamera { get; set; }

        // -(void)setWavelength:(float * _Nonnull)wavelength;
        [Export("setWavelength:")]
        unsafe void SetWavelength(ref float wavelength);

        // -(void)setWavelengthRed:(float)redWavelength green:(float)greenWavelength blue:(float)blueWavelength;
        [Export("setWavelengthRed:green:blue:")]
        void SetWavelengthRed(float redWavelength, float greenWavelength, float blueWavelength);

        // -(void)getWavelength:(float * _Nonnull)wavelength;
        [Export("getWavelength:")]
        unsafe void GetWavelength(ref float wavelength);

        // -(float)getWavelengthForComponent:(short)component;
        [Export("getWavelengthForComponent:")]
        float GetWavelengthForComponent(short component);

        // -(void)setSunPosition:(MaplyCoordinate3d)sunDir;
        [Export("setSunPosition:")]
        void SetSunPosition(MaplyCoordinate3d sunDir);

        // -(void)removeFromViewC;
        [Export("removeFromViewC")]
        void RemoveFromViewC();
    }

    // @interface MaplyMoon : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyMoon
    {
        // -(instancetype _Nonnull)initWithDate:(NSDate * _Nonnull)date;
        [Export("initWithDate:")]
        IntPtr Constructor(NSDate date);

        // -(MaplyCoordinate)asCoordinate;
        [Export("asCoordinate")]

        MaplyCoordinate AsCoordinate { get; }

        // -(MaplyCoordinate3d)asPosition;
        [Export("asPosition")]

        MaplyCoordinate3d AsPosition { get; }

        // @property (readonly) double illuminatedFraction;
        [Export("illuminatedFraction")]
        double IlluminatedFraction { get; }

        // @property (readonly) double phase;
        [Export("phase")]
        double Phase { get; }
    }

    // @interface MaplyRemoteTileElevationInfo : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyRemoteTileElevationInfo
    {
        // -(instancetype _Nonnull)initWithBaseURL:(NSString * _Nonnull)baseURL ext:(NSString * _Nonnull)ext minZoom:(int)minZoom maxZoom:(int)maxZoom;
        [Export("initWithBaseURL:ext:minZoom:maxZoom:")]
        IntPtr Constructor(string baseURL, string ext, int minZoom, int maxZoom);

        // @property (readonly, nonatomic) NSString * _Nonnull baseURL;
        [Export("baseURL")]
        string BaseURL { get; }

        // @property (nonatomic, strong) NSString * _Nonnull ext;
        [Export("ext", ArgumentSemantic.Strong)]
        string Ext { get; set; }

        // @property (nonatomic) int minZoom;
        [Export("minZoom")]
        int MinZoom { get; set; }

        // @property (nonatomic) int maxZoom;
        [Export("maxZoom")]
        int MaxZoom { get; set; }

        // @property (assign, nonatomic) float timeOut;
        [Export("timeOut")]
        float TimeOut { get; set; }

        // @property (nonatomic) int pixelsPerSide;
        [Export("pixelsPerSide")]
        int PixelsPerSide { get; set; }

        // @property (nonatomic, strong) MaplyCoordinateSystem * _Nullable coordSys;
        [NullAllowed, Export("coordSys", ArgumentSemantic.Strong)]
        MaplyCoordinateSystem CoordSys { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable cacheDir;
        [NullAllowed, Export("cacheDir", ArgumentSemantic.Strong)]
        string CacheDir { get; set; }

        // @property (nonatomic) int cachedFileLifetime;
        [Export("cachedFileLifetime")]
        int CachedFileLifetime { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable queryStr;
        [NullAllowed, Export("queryStr", ArgumentSemantic.Strong)]
        string QueryStr { get; set; }

        // -(NSURLRequest * _Nullable)requestForTile:(MaplyTileID)tileID;
        [Export("requestForTile:")]
        [return: NullAllowed]
        NSUrlRequest RequestForTile(MaplyTileID tileID);

        // -(NSString * _Nullable)fileNameForTile:(MaplyTileID)tileID;
        [Export("fileNameForTile:")]
        [return: NullAllowed]
        string FileNameForTile(MaplyTileID tileID);

        // -(_Bool)tileIsLocal:(MaplyTileID)tileID frame:(int)frame;
        [Export("tileIsLocal:frame:")]
        bool TileIsLocal(MaplyTileID tileID, int frame);
    }

    // @protocol MaplyRemoteTileElevationSourceDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface MaplyRemoteTileElevationSourceDelegate
    {
        // @optional -(void)remoteTileElevationSource:(id _Nonnull)tileSource tileDidLoad:(MaplyTileID)tileID;
        [Export("remoteTileElevationSource:tileDidLoad:")]
        void TileDidLoad(NSObject tileSource, MaplyTileID tileID);

        // @optional -(MaplyElevationChunk * _Nullable)remoteTileElevationSource:(id _Nonnull)tileSource modifyElevReturn:(MaplyElevationChunk * _Nullable)elevChunk forTile:(MaplyTileID)tileID;
        [Export("remoteTileElevationSource:modifyElevReturn:forTile:")]
        [return: NullAllowed]
        MaplyElevationChunk ModifyElevReturn(NSObject tileSource, [NullAllowed] MaplyElevationChunk elevChunk, MaplyTileID tileID);

        // @optional -(void)remoteTileElevationSource:(id _Nonnull)tileSource tileDidNotLoad:(MaplyTileID)tileID error:(NSError * _Nonnull)error;
        [Export("remoteTileElevationSource:tileDidNotLoad:error:")]
        void TileDidNotLoad(NSObject tileSource, MaplyTileID tileID, NSError error);

        // @optional -(void)remoteTileElevationSource:(id _Nonnull)tileSource tileUnloaded:(MaplyTileID)tileID;
        [Export("remoteTileElevationSource:tileUnloaded:")]
        void TileUnloaded(NSObject tileSource, MaplyTileID tileID);
    }

    // @interface MaplyRemoteTileElevationSource : NSObject <MaplyElevationSourceDelegate>
    [BaseType(typeof(NSObject))]
    interface MaplyRemoteTileElevationSource : MaplyElevationSourceDelegate
    {
        // -(instancetype _Nullable)initWithBaseURL:(NSString * _Nonnull)baseURL ext:(NSString * _Nonnull)ext minZoom:(int)minZoom maxZoom:(int)maxZoom;
        [Export("initWithBaseURL:ext:minZoom:maxZoom:")]
        IntPtr Constructor(string baseURL, string ext, int minZoom, int maxZoom);

        // -(instancetype _Nullable)initWithInfo:(MaplyRemoteTileElevationInfo * _Nonnull)info;
        [Export("initWithInfo:")]
        IntPtr Constructor(MaplyRemoteTileElevationInfo info);

        // -(MaplyCoordinateSystem * _Nullable)getCoordSystem;
        [NullAllowed, Export("getCoordSystem")]

        MaplyCoordinateSystem CoordSystem { get; }

        // -(int)minZoom;
        [Export("minZoom")]

        int MinZoom { get; }

        // -(int)maxZoom;
        [Export("maxZoom")]

        int MaxZoom { get; }

        // -(MaplyElevationChunk * _Nullable)elevForTile:(MaplyTileID)tileID;
        [Export("elevForTile:")]
        [return: NullAllowed]
        MaplyElevationChunk ElevForTile(MaplyTileID tileID);

        // -(_Bool)tileIsLocal:(MaplyTileID)tileID frame:(int)frame;
        [Export("tileIsLocal:frame:")]
        bool TileIsLocal(MaplyTileID tileID, int frame);

        // -(void)startFetchLayer:(id _Nonnull)layer tile:(MaplyTileID)tileID;
        [Export("startFetchLayer:tile:")]
        void StartFetchLayer(NSObject layer, MaplyTileID tileID);

        // -(MaplyElevationChunk * _Nonnull)decodeElevationData:(NSData * _Nonnull)data;
        [Export("decodeElevationData:")]
        MaplyElevationChunk DecodeElevationData(NSData data);

        // @property (nonatomic, strong) MaplyRemoteTileElevationInfo * _Nullable tileInfo;
        [NullAllowed, Export("tileInfo", ArgumentSemantic.Strong)]
        MaplyRemoteTileElevationInfo TileInfo { get; set; }

        [Wrap("WeakDelegate")]
        [NullAllowed]
        MaplyRemoteTileElevationSourceDelegate Delegate { get; set; }

        // @property (nonatomic, weak) NSObject<MaplyRemoteTileElevationSourceDelegate> * _Nullable delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
        NSObject WeakDelegate { get; set; }

        // @property (nonatomic, strong) MaplyCoordinateSystem * _Nullable coordSys;
        [NullAllowed, Export("coordSys", ArgumentSemantic.Strong)]
        MaplyCoordinateSystem CoordSys { get; set; }

        // @property (nonatomic, strong) NSString * _Nullable cacheDir;
        [NullAllowed, Export("cacheDir", ArgumentSemantic.Strong)]
        string CacheDir { get; set; }
    }

    partial interface IMaplyRemoteTileElevationSourceDelegate { }

    // @interface MaplyRemoteTileElevationCesiumSource : MaplyRemoteTileElevationSource
    [BaseType(typeof(MaplyRemoteTileElevationSource))]
    interface MaplyRemoteTileElevationCesiumSource
    {
        // @property (nonatomic) float scale;
        [Export("scale")]
        float Scale { get; set; }
    }

    // @interface MaplyRemoteTileElevationCesiumInfo : MaplyRemoteTileElevationInfo
    [BaseType(typeof(MaplyRemoteTileElevationInfo))]
    interface MaplyRemoteTileElevationCesiumInfo
    {
    }

    // @interface MaplyCesiumCoordSystem : MaplyPlateCarree
    [BaseType(typeof(MaplyPlateCarree))]
    interface MaplyCesiumCoordSystem
    {
    }

    // @interface MaplyGeomState : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyGeomState
    {
        // @property (nonatomic, strong) UIColor * color;
        [Export("color", ArgumentSemantic.Strong)]
        UIColor Color { get; set; }

        // @property (nonatomic, strong) UIColor * backColor;
        [Export("backColor", ArgumentSemantic.Strong)]
        UIColor BackColor { get; set; }

        // @property (nonatomic, strong) id texture;
        [Export("texture", ArgumentSemantic.Strong)]
        NSObject Texture { get; set; }
    }

    // @interface MaplyGeomBuilder : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyGeomBuilder
    {
        // -(id)initWithViewC:(MaplyBaseViewController *)viewC;
        [Export("initWithViewC:")]
        IntPtr Constructor(MaplyBaseViewController viewC);

        // -(void)addRectangleAroundOrigin:(MaplyCoordinateD)size state:(MaplyGeomState *)state;
        [Export("addRectangleAroundOrigin:state:")]
        void AddRectangleAroundOrigin(MaplyCoordinateD size, MaplyGeomState state);

        // -(void)addRectangleAroundOriginX:(double)x y:(double)y state:(MaplyGeomState *)state;
        [Export("addRectangleAroundOriginX:y:state:")]
        void AddRectangleAroundOriginX(double x, double y, MaplyGeomState state);

        // -(void)addRectangleAroundX:(double)x y:(double)y width:(double)width height:(double)height state:(MaplyGeomState *)state;
        [Export("addRectangleAroundX:y:width:height:state:")]
        void AddRectangleAroundX(double x, double y, double width, double height, MaplyGeomState state);

        // -(void)addAttributedString:(NSAttributedString *)str state:(MaplyGeomState *)state;
        [Export("addAttributedString:state:")]
        void AddAttributedString(NSAttributedString str, MaplyGeomState state);

        // -(void)addString:(NSString *)str font:(UIFont *)font state:(MaplyGeomState *)state;
        [Export("addString:font:state:")]
        void AddString(string str, UIFont font, MaplyGeomState state);

        // -(void)addString:(NSString *)str width:(double)width height:(double)height font:(UIFont *)font state:(MaplyGeomState *)state;
        [Export("addString:width:height:font:state:")]
        void AddString(string str, double width, double height, UIFont font, MaplyGeomState state);

        // -(void)addPolygonWithPts:(MaplyCoordinate3dD *)pts numPts:(int)numPts state:(MaplyGeomState *)state;
        [Export("addPolygonWithPts:numPts:state:")]
        unsafe void AddPolygonWithPts(NSNumber[] pts, int numPts, MaplyGeomState state);

        // -(void)addPolygonWithPts:(MaplyCoordinate3dD *)pts tex:(MaplyCoordinateD *)tex norms:(MaplyCoordinate3dD *)norms numPts:(int)numPts state:(MaplyGeomState *)state;
        [Export("addPolygonWithPts:tex:norms:numPts:state:")]
        unsafe void AddPolygonWithPts(NSNumber[] pts, NSNumber[] tex, NSNumber[] norms, int numPts, MaplyGeomState state);

        // -(void)addCurPointX:(double)x y:(double)y z:(double)z;
        [Export("addCurPointX:y:z:")]
        void AddCurPointX(double x, double y, double z);

        // -(void)addCurPointX:(double)x y:(double)y;
        [Export("addCurPointX:y:")]
        void AddCurPointX(double x, double y);

        // -(void)addCurPoly:(MaplyGeomState *)state;
        [Export("addCurPoly:")]
        void AddCurPoly(MaplyGeomState state);

        // -(void)scale:(MaplyCoordinate3dD)scale;
        [Export("scale:")]
        void Scale(MaplyCoordinate3dD scale);

        // -(void)scaleX:(double)x y:(double)y z:(double)z;
        [Export("scaleX:y:z:")]
        void ScaleX(double x, double y, double z);

        // -(void)translate:(MaplyCoordinate3dD)trans;
        [Export("translate:")]
        void Translate(MaplyCoordinate3dD trans);

        // -(void)translateX:(double)x y:(double)y z:(double)z;
        [Export("translateX:y:z:")]
        void TranslateX(double x, double y, double z);

        // -(void)rotate:(double)angle around:(MaplyCoordinate3dD)axis;
        [Export("rotate:around:")]
        void Rotate(double angle, MaplyCoordinate3dD axis);

        // -(void)rotate:(double)angle aroundX:(double)x y:(double)y z:(double)z;
        [Export("rotate:aroundX:y:z:")]
        void Rotate(double angle, double x, double y, double z);

        // -(void)transform:(MaplyMatrix *)matrix;
        [Export("transform:")]
        void Transform(MaplyMatrix matrix);

        // -(void)addGeomFromBuilder:(MaplyGeomBuilder *)modelBuilder;
        [Export("addGeomFromBuilder:")]
        void AddGeomFromBuilder(MaplyGeomBuilder modelBuilder);

        // -(void)addGeomFromBuilder:(MaplyGeomBuilder *)modelBuilder transform:(MaplyMatrix *)matrix;
        [Export("addGeomFromBuilder:transform:")]
        void AddGeomFromBuilder(MaplyGeomBuilder modelBuilder, MaplyMatrix matrix);

        // -(_Bool)getSizeLL:(MaplyCoordinate3dD *)ll ur:(MaplyCoordinate3dD *)ur;
        [Export("getSizeLL:ur:")]
        unsafe bool GetSizeLL(NSNumber[] ll, NSNumber[] ur);

        // -(MaplyCoordinate3dD)getSize;
        [Export("getSize")]

        MaplyCoordinate3dD Size { get; }

        // -(MaplyGeomModel *)makeGeomModel:(MaplyThreadMode)threadMode;
        [Export("makeGeomModel:")]
        MaplyGeomModel MakeGeomModel(MaplyThreadMode threadMode);
    }

    // @interface MaplyColorRampGenerator : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyColorRampGenerator
    {
        // @property (assign, nonatomic) _Bool stretch;
        [Export("stretch")]
        bool Stretch { get; set; }

        // -(void)addHexColor:(int)hexColor;
        [Export("addHexColor:")]
        void AddHexColor(int hexColor);

        // -(void)addHexColorWithAlpha:(int)hexColor;
        [Export("addHexColorWithAlpha:")]
        void AddHexColorWithAlpha(int hexColor);

        // -(void)addColor:(UIColor *)color;
        [Export("addColor:")]
        void AddColor(UIColor color);

        // -(UIImage *)makeImage:(CGSize)size;
        [Export("makeImage:")]
        UIImage MakeImage(CGSize size);
    }

    // @interface MaplyAerisLayerInfo : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyAerisLayerInfo
    {
        // -(instancetype _Nullable)initWithCode:(NSString * _Nonnull)code name:(NSString * _Nonnull)name minZoom:(unsigned int)minZoom maxZoom:(unsigned int)maxZoom updatePeriod:(unsigned int)updatePeriod;
        [Export("initWithCode:name:minZoom:maxZoom:updatePeriod:")]
        IntPtr Constructor(string code, string name, uint minZoom, uint maxZoom, uint updatePeriod);

        // @property (nonatomic, strong) NSString * _Nonnull layerCode;
        [Export("layerCode", ArgumentSemantic.Strong)]
        string LayerCode { get; set; }

        // @property (nonatomic, strong) NSString * _Nonnull layerName;
        [Export("layerName", ArgumentSemantic.Strong)]
        string LayerName { get; set; }

        // @property (assign, nonatomic) unsigned int minZoom;
        [Export("minZoom")]
        uint MinZoom { get; set; }

        // @property (assign, nonatomic) unsigned int maxZoom;
        [Export("maxZoom")]
        uint MaxZoom { get; set; }

        // @property (assign, nonatomic) unsigned int updatePeriod;
        [Export("updatePeriod")]
        uint UpdatePeriod { get; set; }
    }

    // @interface MaplyAerisTiles : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyAerisTiles
    {
        // -(instancetype _Nullable)initWithAerisID:(NSString * _Nonnull)aerisID secretKey:(NSString * _Nonnull)secretKey;
        [Export("initWithAerisID:secretKey:")]
        IntPtr Constructor(string aerisID, string secretKey);

        // -(NSDictionary * _Nonnull)layerInfo;
        [Export("layerInfo")]

        NSDictionary LayerInfo { get; }
    }

    // @interface MaplyAerisTileSet : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyAerisTileSet
    {
        // -(instancetype _Nullable)initWithAerisID:(NSString * _Nonnull)aerisID secretKey:(NSString * _Nonnull)secretKey layerInfo:(MaplyAerisLayerInfo * _Nonnull)layerInfo tileSetCount:(unsigned int)tileSetCount;
        [Export("initWithAerisID:secretKey:layerInfo:tileSetCount:")]
        IntPtr Constructor(string aerisID, string secretKey, MaplyAerisLayerInfo layerInfo, uint tileSetCount);

        // -(void)startFetchWithSuccess:(void (^ _Nonnull)(NSArray * _Nullable))successBlock failure:(void (^ _Nullable)(NSError * _Nonnull))failureBlock;
        [Export("startFetchWithSuccess:failure:")]
        void StartFetchWithSuccess(Action<NSArray> successBlock, [NullAllowed] Action<NSError> failureBlock);
    }

    [Static]
    partial interface Constants
    {
        // extern NSString *const kMaplyLAZShaderPointSize;
        [Field("kMaplyLAZShaderPointSize", "__Internal")]
        NSString kMaplyLAZShaderPointSize { get; }

        // extern NSString *const kMaplyLAZShaderZMin;
        [Field("kMaplyLAZShaderZMin", "__Internal")]
        NSString kMaplyLAZShaderZMin { get; }

        // extern NSString *const kMaplyLAZShaderZMax;
        [Field("kMaplyLAZShaderZMax", "__Internal")]
        NSString kMaplyLAZShaderZMax { get; }

        // extern NSString *const kMaplyLAZReaderCoordSys;
        [Field("kMaplyLAZReaderCoordSys", "__Internal")]
        NSString kMaplyLAZReaderCoordSys { get; }

        // extern NSString *const kMaplyLAZReaderZOffset;
        [Field("kMaplyLAZReaderZOffset", "__Internal")]
        NSString kMaplyLAZReaderZOffset { get; }

        // extern NSString *const kMaplyLAZReaderColorScale;
        [Field("kMaplyLAZReaderColorScale", "__Internal")]
        NSString kMaplyLAZReaderColorScale { get; }
    }

    // @interface MaplyLAZQuadReader : NSObject <MaplyPagingDelegate>
    [BaseType(typeof(NSObject))]
    interface MaplyLAZQuadReader : MaplyPagingDelegate
    {
        // @property (readonly, strong) MaplyCoordinateSystem * coordSys;
        [Export("coordSys", ArgumentSemantic.Strong)]
        MaplyCoordinateSystem CoordSys { get; }

        // @property (readonly, nonatomic) NSString * name;
        [Export("name")]
        string Name { get; }

        // @property (readonly) double minX;
        [Export("minX")]
        double MinX { get; }

        // @property (readonly) double minY;
        [Export("minY")]
        double MinY { get; }

        // @property (readonly) double minZ;
        [Export("minZ")]
        double MinZ { get; }

        // @property (readonly) double maxX;
        [Export("maxX")]
        double MaxX { get; }

        // @property (readonly) double maxY;
        [Export("maxY")]
        double MaxY { get; }

        // @property (readonly) double maxZ;
        [Export("maxZ")]
        double MaxZ { get; }

        // @property (nonatomic) double zOffset;
        [Export("zOffset")]
        double ZOffset { get; set; }

        // @property (nonatomic) int minZoom;
        [Export("minZoom")]
        int MinZoom { get; set; }

        // @property (nonatomic) int maxZoom;
        [Export("maxZoom")]
        int MaxZoom { get; set; }

        // @property (readonly, nonatomic) int minTilePoints;
        [Export("minTilePoints")]
        int MinTilePoints { get; }

        // @property (readonly, nonatomic) int maxTilePoints;
        [Export("maxTilePoints")]
        int MaxTilePoints { get; }

        // @property (nonatomic, strong) MaplyShader * shader;
        [Export("shader", ArgumentSemantic.Strong)]
        MaplyShader Shader { get; set; }

        // @property (nonatomic) float pointSize;
        [Export("pointSize")]
        float PointSize { get; set; }

        // -(id)initWithDB:(NSString *)fileName desc:(NSDictionary *)desc viewC:(WhirlyGlobeViewController *)viewC;
        [Export("initWithDB:desc:viewC:")]
        IntPtr Constructor(string fileName, NSDictionary desc, WhirlyGlobeViewController viewC);

        // -(int)getNumTilesFromMaxPoints:(int)maxPoints;
        [Export("getNumTilesFromMaxPoints:")]
        int GetNumTilesFromMaxPoints(int maxPoints);

        // -(MaplyCoordinate)getCenter;
        [Export("getCenter")]

        MaplyCoordinate Center { get; }

        // -(void)getBoundsLL:(MaplyCoordinate3dD *)ll ur:(MaplyCoordinate3dD *)ur;
        [Export("getBoundsLL:ur:")]
        unsafe void GetBoundsLL(NSNumber[] ll, NSNumber[] ur);

        // -(_Bool)hasColor;
        [Export("hasColor")]

        bool HasColor { get; }
    }


    partial interface IMaply3dTouchPreviewDatasource { }

    // @interface Maply3dTouchPreviewDelegate : NSObject <UIViewControllerPreviewingDelegate>
    [BaseType(typeof(NSObject))]
    interface Maply3dTouchPreviewDelegate : IUIViewControllerPreviewingDelegate
    {
        // @property (nonatomic, strong) NSObject<Maply3dTouchPreviewDatasource> * _Nonnull datasource;
        [Export("datasource", ArgumentSemantic.Strong)]
        IMaply3dTouchPreviewDatasource Datasource { get; set; }

        // +(Maply3dTouchPreviewDelegate * _Nonnull)touchDelegate:(MaplyBaseViewController * _Nonnull)maplyViewC interactLayer:(MaplyBaseInteractionLayer * _Nonnull)interactLayer datasource:(NSObject<Maply3dTouchPreviewDatasource> * _Nonnull)datasource;
        [Static]
        [Export("touchDelegate:interactLayer:datasource:")]
        Maply3dTouchPreviewDelegate TouchDelegate(MaplyBaseViewController maplyViewC, NSObject interactLayer, Maply3dTouchPreviewDatasource datasource);
    }

    // @interface MaplyLinearTextureBuilder : NSObject
    [BaseType(typeof(NSObject))]
    interface MaplyLinearTextureBuilder
    {
        // -(void)setPattern:(NSArray *)elements;
        [Export("setPattern:")]

        void SetPattern(NSObject[] elements);

        // -(UIImage *)makeImage;
        [Export("makeImage")]

        UIImage MakeImage { get; }
    }

    // @interface zlib (NSData)
    [Category]
    [BaseType(typeof(NSData))]
    interface NSData_zlib
    {
        // -(NSData *)compressData;
        [Export("compressData")]

        NSData CompressData();

        // -(NSData *)uncompressGZip;
        [Export("uncompressGZip")]

        NSData UncompressGZip();

        // -(BOOL)isCompressed;
        [Export("isCompressed")]

        bool IsCompressed();
    }

    // @interface StyleRules (NSMutableDictionary)
    [Category]
    [BaseType(typeof(NSMutableDictionary))]
    interface NSMutableDictionary_StyleRules
    {
        // -(NSMutableArray *)styles;
        [Export("styles")]

        NSMutableArray Styles();

        // -(NSMutableArray *)rules;
        [Export("rules")]

        NSMutableArray Rules();

        // -(NSMutableArray *)symbolizers;
        [Export("symbolizers")]

        NSMutableArray Symbolizers();

        // -(NSMutableArray *)layers;
        [Export("layers")]

        NSMutableArray Layers();

        // -(NSString *)filter;
        // -(void)setFilter:(NSString *)filter;
        [Export("filter")]

        string Filter();

        // -(NSString *)name;
        [Export("name")]
        string Name();

        // -(NSNumber *)minScaleDenom;
        [Export("minScaleDenom")]
        NSNumber MinScaleDenom();

        // -(void)setMinScaleDenom:(NSNumber *)num;
        [Export("setMinScaleDenom:")]
        void SetMinScaleDenom(NSNumber num);

        // -(NSNumber *)maxScaleDenom;
        [Export("maxScaleDenom")]
        NSNumber MaxScaleDenom();

        // -(void)setMaxScaleDenom:(NSNumber *)num;
        [Export("setMaxScaleDenom:")]
        void SetMaxScaleDenom(NSNumber num);

        // -(NSMutableDictionary *)parameters;
        [Export("parameters")]
        NSMutableDictionary Parameters();
    }
}
